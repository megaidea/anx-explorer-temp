##Usage
Custom or passthrough proxy apis added to make wec-api compatible with insight-api    


##Installation
custom-api depends on bitcoind, and oa-core.  Follow installation instructions for oa-core.

Then in the node_modules folder in the bitcore installation:

From the `node_modules` folder where you see bitcore-node folder:

`ln -s <your path>/anx-explorer/custom-api`

From the `custom-api` folder, run `npm install` to install the dependencies.

Add `"custom-api"` after `"oa-core"` in the `bitcore-node.json` config file.

custom-api passes through API calls, so it can act as the primary api to insight-ui instead of insight-api.
To configure (on bitcore 4.0) - Add this section under "servicesConfig":
    "insight-ui": {
      "apiPrefix": "customcoin"
    },

    "custom-api": {
      "routePrefix": "wec",
      "shortName" : "WEC",
      "assetId" : "AGXSTkTjYAZRh9Q3484vqxus355VzjYF5v",
      "divisibility": 5
    },

Setting apiPrefix will cause insight-ui to use /customcoin (using custom-api) instead of /api or /insight-api
Setting routePrefix will cause custom-api to put its custom API on /customcoin
Future: The api could be configured with a end-point URL.
