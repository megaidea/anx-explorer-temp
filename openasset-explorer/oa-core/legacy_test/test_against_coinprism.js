'use strict';

var async = require('async');
var request = require("request");
var coloringengine = require("../coloringengine");
var ce = new coloringengine.ColoringEngine(function(h,c) {});

var our_api = 'http://localhost:3001/oa';
var cpm_api = 'https://api.coinprism.com/v1';

function test_against_cointprism() {

	var hash = '';
	a_equals_b(our_api+'/tx/'+hash, 'qty', cpm_api + 'transactions/'+hash, 'asset_qty', function(err, rslt) {
		if (err)
		 	console.log(err);
		else if (rslt === true)
			console.log('PASSED');
		else
			console.log("FAIL");
	});



};

function a_equals_b(url_a, item_a, url_b, item_b, callback) {
	async.parallel({
		a: function(callback_a) { get_item(url_a, item_b, function(err, rslt_a) {
				callback_a(null, rslt_a);

	 		})
	 	},


		b: function(callback_b) { get_item(url_b, item_b, function(err, rslt_b) {
				callback_b(null, rslt_b);
	 		})
	 	},


		}, function(err, results) {
			if (err)
				return callback(err);

			if (results.a === undefined)
				return callback(null, false);

			if (results.b === undefined)
				return callback(null, false);

			console.log(results);
			if (results[0] == results[1])
				callback(null, true);

		}
	);
}

/* Pass in a URL and a top-level item to return from the JSON
 * Or leave item as undefined get the all the data back      */
function get_item(url, item, callback) {

	var data;
	if (url) {
		request(url, function(error, response, body) {
		  if (error) {
		  		console.log("Error getting info from " + url + error);
		  		return callback(error);
		  }
		  if (response.statusCode == 200) {
		  	data = JSON.parse(body);
		  	if (item)
		  	  callback(null, data[item]);
		  	else 
		  	  callback(null, data);
		  }
		  else
		  {
		  	console.error('url call failed: ' + url);
		  	callback('URL call failed:');
		  }
		});
	}
	else
	{
		//If not a url, just return json name
		callback('Invalid URL:' + url);	
	} 

}

function test_asset_holders(assetId) {

	async.parallel( { 
		our: function(callback) {
			//get_item(our_api + '/balance/' + assetId, 'owners', function (err, our_data) {
			get_item(our_api + '/owners/' + assetId, null, function (err, our_data) {
				// console.log(JSON.stringify(our_data));
				// console.log(our_data.length);
				callback(null, our_data);
				// our_data.forEach(function (one, i , our_data) {
				// 	console.log(one);
				// }); 

			});
		},

		cpm: function(callback) { 
			get_item(cpm_api + '/assets/' + assetId + '/owners', 'owners', function (err, cpm_data) {
				callback(null, cpm_data);

				});
			} 
		},
	    function(err, results) {
	    	var matches = 0;
	    	//Loop through the coin prism data and compare to data from our API call
	    	console.log(results.our);
	    	console.log(results.cpm);
			results.cpm.forEach(function(owner, i, owner_arr) {
				var qty = lookup_owner_balance(results.our, owner.address); //Check by looking in the balance index.
				//var qty = lookup_owner_qty(results.our, owner.address);   //Check by adding up all utxos
				if (owner.asset_quantity == qty) {
				   console.log(owner.address + ' has ' + owner.asset_quantity + ' == ' + qty + ' MATCH');
				   matches++;
				}
				else {
				   console.log(owner.address + ' has ' + owner.asset_quantity + ' NOT EQUAL ' + qty + ' FAIL');
				}
			});
			if (results.cpm.length === results.our.length)
				console.log("Same number of addresses.  PASS");
		 	else 
				console.log("Different number of addresses.  FAIL");

			if (results.cpm.length == matches)
				console.log("Same number of matches.  PASS");
		 	else 
				console.log("Different number of matches.  FAIL");
	 	});

} 


function lookup_owner_balance(arr, address) {

	address = ce.addressFromBitcoinAddress(address);

	for(var i=0;i<arr.length;i++) {
		var a = arr[i];

		//console.log(a.address);
		//console.log(address);
		if (a.address == address)
			return(a.balance);
	}
	return(-1);
}

function lookup_owner_qty(arr, address) {

	address = ce.addressFromBitcoinAddress(address);

	for(var i=0;i<arr.length;i++) {
		var a = arr[i];

		//console.log(a.address);
		//console.log(address);
		if (a.address == address)
			return(a.qty);
	}
	return(-1);
}


//Go through each of the asset holders (asset/address), and check against (address/asset)
function test_internal_balance_consistency() {
	get_item(our_api + '/balances', null, function (err, lst) {
		console.log('Count balances:' + lst.length);
		console.log(lst);
		console.log(lst[0]);
		console.log(lst[0].assetId);
		//lst = JSON.parse(lst);
		async.forEachOfSeries(lst, 
			function(one, i, cb) {
				console.log("Get:" + i + ' ' + our_api + '/balance/' + one.assetId+'/'+one.address);
				get_item(our_api + '/balance/'+one.assetId+'/'+one.address, null, function (err, lst2) {
					if (err) {
						console.log(err);
						cb(err);
					}
					else {
						if(lst2.balance === one.balance)
							console.log("Passed: " + ' Expected:' + one.balance);
						else
							console.log('Failed on assetId: ' + JSON.stringify(lst2) + ' Expected:' + one.balance);
						cb();
					}
				});

		},
			function(err) {
				console.log("Finished Checking");
			}

		);
	});
}

//console.log(coloringengine);
test_asset_holders('AGXSTkTjYAZRh9Q3484vqxus355VzjYF5v');

//test_internal_balance_consistency();

//test_against_cointprism();
