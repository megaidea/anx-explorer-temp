exports.ASSETPREFIXES = {
  ASSETTOTX: new Buffer('A0', 'hex'),  //AssetToTx index
  TXTOASSET: new Buffer('A1', 'hex'),  //TxToAsset index
  ADDRESSTOBALANCE: new Buffer('A2', 'hex'),  //AddressToBalance index
  ADDRESSTOTX: new Buffer('A3', 'hex'),  //AddressToTx index
  ASSETNAMETOASSET: new Buffer('A4', 'hex'),  //AssetNameToAsset index
  ASSETTOMETA: new Buffer('A5', 'hex'),       //AssetToMeta index
  ASSETTOBALANCE: new Buffer('A6', 'hex'),  //AssetToBalance
  ASSETTIP: new Buffer('04', 'hex')       //Tracks tip
};

// To save space, we're only storing the PubKeyHash or ScriptHash in our index.
// To avoid intentional unspendable collisions, which have been seen on the blockchain,
// we must store the hash type (PK or Script) as well.
exports.HASH_TYPES = {
  PUBKEY: new Buffer('01', 'hex'),
  REDEEMSCRIPT: new Buffer('02', 'hex')
};

// Translates from our enum type back into the hash types returned by
// bitcore-lib/address.
exports.HASH_TYPES_READABLE = {
  '01': 'pubkeyhash',
  '02': 'scripthash'
};

exports.HASH_TYPES_MAP = {
  'pubkeyhash': exports.HASH_TYPES.PUBKEY,
  'scripthash': exports.HASH_TYPES.REDEEMSCRIPT
};

exports.ISSPENT = {
  UNSPENT: new Buffer('00', 'hex'),
  SPENT: new Buffer('01', 'hex')
};

exports.SPACER_MIN = new Buffer('00', 'hex');
exports.SPACER_MAX = new Buffer('ff', 'hex');
exports.SPACER_HEIGHT_MIN = new Buffer('0000000000', 'hex');
exports.SPACER_HEIGHT_MAX = new Buffer('ffffffffff', 'hex');
exports.TIMESTAMP_MIN = new Buffer('0000000000000000', 'hex');
exports.TIMESTAMP_MAX = new Buffer('ffffffffffffffff', 'hex');
