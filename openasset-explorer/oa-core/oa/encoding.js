'use strict';

var bitcore = require('bitcore-lib');
var BufferReader = bitcore.encoding.BufferReader;
var Address = bitcore.Address;
var PublicKey = bitcore.PublicKey;
var constants = require('./constants');
var $ = bitcore.util.preconditions;

var exports = {};


exports.getAddressInfo = function(addressStr) {
  var addrObj = bitcore.Address(addressStr);
  var hashTypeBuffer = constants.HASH_TYPES_MAP[addrObj.type];
  
  return {
    hashBuffer: addrObj.hashBuffer,
    hashTypeBuffer: hashTypeBuffer,
    hashTypeReadable: addrObj.type
  };
};

/**
 * This function is optimized to return address information about an output script
 * without constructing a Bitcore Address instance.
 * @param {Script} - An instance of a Bitcore Script
 * @param {Network|String} - The network for the address
 */
exports.extractAddressInfoFromScript = function(script, network) {
  $.checkArgument(network, 'Second argument is expected to be a network');
  var hashBuffer;
  var addressType;
  var hashTypeBuffer;
  if (script.isPublicKeyHashOut()) {
    hashBuffer = script.chunks[2].buf;
    hashTypeBuffer = constants.HASH_TYPES.PUBKEY;
    addressType = Address.PayToPublicKeyHash;
  } else if (script.isScriptHashOut()) {
    hashBuffer = script.chunks[1].buf;
    hashTypeBuffer = constants.HASH_TYPES.REDEEMSCRIPT;
    addressType = Address.PayToScriptHash;
  } else if (script.isPublicKeyOut()) {
    var pubkey = script.chunks[0].buf;
    var address = Address.fromPublicKey(new PublicKey(pubkey), network);
    hashBuffer = address.hashBuffer;
    hashTypeBuffer = constants.HASH_TYPES.PUBKEY;
    // pay-to-publickey doesn't have an address, however for compatibility    console.log(JSON.stringify(cti.metadata_acquired));

    // purposes, we can create an address
    addressType = Address.PayToPublicKeyHash;
  } else {
    return false;
  }
  return {
    hashBuffer: hashBuffer,
    hashTypeBuffer: hashTypeBuffer,
    addressType: addressType
  };
};


exports.encodeAssetToTxKey = function(assetId, height, txnum, outputIdx) {
  $.checkArgument(assetId && assetId.length===20 || assetId.length===40, 'First argument is expected to be an assetIdBuffer');  
  $.checkArgument(height && isNumeric(height), 'Second argument is expected to be a numeric block height');  
  $.checkArgument(isNumeric(txnum), 'Third argument is expected to be a numeric tx number (in block)');  
  $.checkArgument(isNumeric(outputIdx), 'Fourth argument is expected to be a numeric output (in tx)');

  var assetIdBuffer = (assetId.length == 40) ? new Buffer(assetId, 'hex') : assetId;
  var heightBuffer = new Buffer(4);
  heightBuffer.writeUInt32BE(height);
  var txnumBuffer = new Buffer(4);
  txnumBuffer.writeUInt32BE(txnum);
  var outputIdxBuffer = new Buffer(4);
  outputIdxBuffer.writeUInt32BE(outputIdx);

  return Buffer.concat([
    constants.ASSETPREFIXES.ASSETTOTX,
    assetIdBuffer,
    heightBuffer,
    txnumBuffer,
    outputIdxBuffer
  ]);
};

exports.decodeAssetToTxKey = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  
  $.checkArgument(buffer.readUInt8() === constants.ASSETPREFIXES.ASSETTOTX.readUInt8(), 'LevelDB prefix does not match - wrong decode function?');

  var assetId = buffer.slice(1, 21).toString('hex');
  var height = buffer.readUInt32BE(21);
  var txnum  = buffer.readUInt32BE(25);
  var outputIdx = buffer.readUInt32BE(29);

  return{
    assetId:assetId,
    height: height,
    txnum: txnum,
    outputIdx: outputIdx
  };
};


exports.encodeAssetToTxValue = function(txId, outputIdx) {
  $.checkArgument(txId && txId.length==32 || txId.length==64, 'First argument is expected to be an txId in hex or Buffer (32 bytes)'); 
  $.checkArgument(isNumeric(outputIdx), 'Second argument is expected to be an outputIdx');  

  var txIdBuffer = (txId.length == 64) ? new Buffer(txId, 'hex') : txId;
  var outputIdxBuffer = new Buffer(4);
  outputIdxBuffer.writeUInt32BE(outputIdx);

  return Buffer.concat([
      txIdBuffer,
      outputIdxBuffer
    ]);
};

exports.decodeAssetToTxValue = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  

  var txId = buffer.slice(0, 32).toString('hex');
  var outputIdx = buffer.readUInt32BE(32);
 
  return { txId: txId, outputIdx: outputIdx };
};

exports.encodeTxToAssetKey = function(txId, outputIndex) {
  $.checkArgument(txId && txId.length===32 || txId.length===64, 'First argument is expected to be an txId Buffer or Hex (32 bytes)');  
  $.checkArgument(isNumeric(outputIndex), 'Second argument is expected to be an outputIndex');  

  var txIdBuffer = (txId.length == 64) ? new Buffer(txId, 'hex') : txId;
  var outputIndexBuffer = new Buffer(4);
  outputIndexBuffer.writeUInt32BE(outputIndex);

  return Buffer.concat([
    constants.ASSETPREFIXES.TXTOASSET,
    txIdBuffer,
    outputIndexBuffer
  ]);
};

exports.decodeTxToAssetKey = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  
  $.checkArgument(buffer.readUInt8() === constants.ASSETPREFIXES.TXTOASSET.readUInt8(), 'LevelDB prefix does not match - wrong decode function?');

  var txId = buffer.slice(1, 33).toString('hex');
  var outputIndex = buffer.readUInt32BE(33);

  return {
    txId : txId,
    outputIndex: outputIndex
  };
};


exports.encodeTxToAssetValue = function(assetId, assetAddress, height, txnum, outputIdx, qty, unspent_qty) {
  $.checkArgument(assetId && assetId.length==20 || assetId.length==40, 'First argument is expected to be an assetId Buffer or hex (20 bytes)');  
  $.checkArgument(assetId && assetId.length==20 || assetId.length==40, 'Second argument is expected to be an assetAddress Buffer or hex (20 bytes)');  
  $.checkArgument(isNumeric(height), 'Third argument is expected to be a numeric block height');  
  $.checkArgument(isNumeric(txnum), 'Fourth argument is expected to be a numeric tx number (in block)');  
  $.checkArgument(isNumeric(outputIdx), 'Fifth argument is expected to be a numeric output (in tx)');
  $.checkArgument(isNumeric(qty), 'Sixth argument is expected to be a numeric qty');
  $.checkArgument(isNumeric(unspent_qty), 'Seventh argument is expected to be a numeric UNSPENT qty');

  var assetIdBuffer = (assetId.length == 40) ? new Buffer(assetId, 'hex') : assetId;
  var assetAddressBuffer = (assetAddress.length == 40) ? new Buffer(assetAddress, 'hex') : assetAddress;

  var heightBuffer = new Buffer(4);
  heightBuffer.writeUInt32BE(height);
  var txnumBuffer = new Buffer(4);
  txnumBuffer.writeUInt32BE(txnum);
  var outputIdxBuffer = new Buffer(4);
  outputIdxBuffer.writeUInt32BE(outputIdx);
  var qtyBuffer = new Buffer(8);
  qtyBuffer.writeDoubleBE(qty);
  var unspentQtyBuffer = new Buffer(16);
  unspentQtyBuffer.writeDoubleBE(unspent_qty);
  
  return Buffer.concat([
    assetIdBuffer,
    assetAddressBuffer,
    heightBuffer,
    txnumBuffer,
    outputIdxBuffer,
    qtyBuffer,
    unspentQtyBuffer
  ]);
};

function isNumeric(n){
  return (typeof n == "number" && !isNaN(n));
}


exports.decodeTxToAssetValue = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  

  var assetId = buffer.slice(0, 20).toString('hex');
  var assetAddress = buffer.slice(20, 40).toString('hex');
  var height = buffer.readUInt32BE(40);
  var txnum = buffer.readUInt32BE(44);
  var outputIdx = buffer.readUInt32BE(48);
  var qty = 0;
  try {
    qty = buffer.readDoubleBE(52);

  } catch(e) {
    console.error('Exceeded range decoding asset value.  Might be old index.  Asset:' + assetId);
    console.error(e);
  }

  var unspentQty = 0;
  try {
    unspentQty = buffer.readDoubleBE(60);
  } 
  catch(err) {
    console.error('Exceeded range decoding asset value.  Might be old index.  Asset:' + assetId);
    console.error(err);
  }

  return {
    assetId: assetId,
    assetAddress: assetAddress,
    height: height,
    txnum: txnum,
    outputIdx: outputIdx,
    qty: qty,
    unspentQty: unspentQty
  };

};

exports.encodeAddressToBalanceKey = function(assetAddress, assetId) {
  $.checkArgument(assetAddress && assetAddress.length===20 || assetAddress.length===40, 'First argument is expected to be an assetAddress Buffer or Hex (20 bytes)');  
  $.checkArgument(assetId && assetId.length===20 || assetId.length===40, 'Second argument is expected to be an assetId Buffer or Hex (20 bytes)');  
  
  var assetAddressBuffer = (assetAddress.length === 40) ? new Buffer(assetAddress, 'hex') : assetAddress;
  var assetIdBuffer = (assetId.length === 40) ? new Buffer(assetId, 'hex') : assetId;

  return Buffer.concat([
    constants.ASSETPREFIXES.ADDRESSTOBALANCE,
    assetAddressBuffer,
    assetIdBuffer
  ]);
};


exports.decodeAddressToBalanceKey = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  
  $.checkArgument(buffer.readUInt8() === constants.ASSETPREFIXES.ADDRESSTOBALANCE.readUInt8(), 'LevelDB prefix does not match - wrong decode function?');

  var assetAddress = buffer.slice(1, 21).toString('hex');
  var assetId = buffer.slice(21, 41).toString('hex');

  return {
    assetAddress: assetAddress,
    assetId: assetId
  }; 
};


exports.encodeAddressToBalanceValue = function(balance) {
  $.checkArgument(isNumeric(balance), 'First argument is expected to be a balance');  

  var balanceBuffer = new Buffer(8);
  balanceBuffer.writeDoubleBE(balance);

  return(balanceBuffer);
};


exports.decodeAddressToBalanceValue = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  

  var balance = buffer.readDoubleBE(buffer);

  return {
    balance: balance
  };
};


exports.encodeAssetToBalanceKey = function(assetId, assetAddress) {
  $.checkArgument(assetId && assetId.length===20 || assetId.length===40, 'First argument is expected to be an assetId Buffer or Hex (20 bytes)');  
  $.checkArgument(assetAddress && assetAddress.length===20 || assetAddress.length===40, 'Second argument is expected to be an assetAddress Buffer or Hex (20 bytes)');  
  
  var assetIdBuffer = (assetId.length === 40) ? new Buffer(assetId, 'hex') : assetId;
  var assetAddressBuffer = (assetAddress.length === 40) ? new Buffer(assetAddress, 'hex') : assetAddress;

  return Buffer.concat([
    constants.ASSETPREFIXES.ASSETTOBALANCE,
    assetIdBuffer,
    assetAddressBuffer
  ]);
};


exports.decodeAssetToBalanceKey = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  
  $.checkArgument(buffer.readUInt8() === constants.ASSETPREFIXES.ASSETTOBALANCE.readUInt8(), 'LevelDB prefix does not match - wrong decode function?');

  var assetId = buffer.slice(1, 21).toString('hex');
  var assetAddress = buffer.slice(21, 41).toString('hex');

  return {
    assetId: assetId,
    assetAddress: assetAddress
  }; 
};

exports.encodeAssetToBalanceValue = function(balance) {
  $.checkArgument(isNumeric(balance), 'First argument is expected to be a balance');  

  var balanceBuffer = new Buffer(8);
  balanceBuffer.writeDoubleBE(balance);

  return(balanceBuffer);
};


exports.decodeAssetToBalanceValue = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  

  var balance = buffer.readDoubleBE(buffer);

  return {
    balance: balance
  };
};

exports.encodeAddressToTxKey = function(assetAddress, assetId, height, txnum, outputIdx) {
  $.checkArgument(assetAddress && assetAddress.length===20 || assetAddress.length===40, 'First argument is expected to be an addressBuffer (20 bytes)');  
  $.checkArgument(assetId && assetId.length===20 || assetId.length===40, 'Second argument is expected to be an assetIdBuffer (20 bytes)');  
  $.checkArgument(isNumeric(height), 'Third argument is expected to be a numeric block height');  
  $.checkArgument(isNumeric(txnum), 'Fourth argument is expected to be a numeric tx number (in block)');  
  $.checkArgument(isNumeric(outputIdx), 'Fifth argument is expected to be a numeric output (in tx)');


  var assetAddressBuffer = (assetAddress.length === 40) ? new Buffer(assetAddress, 'hex') : assetAddress;
  var assetIdBuffer = (assetId.length === 40) ? new Buffer(assetId, 'hex') : assetId;
  var heightBuffer = new Buffer(4);
  heightBuffer.writeUInt32BE(height);
  var txnumBuffer = new Buffer(4);
  txnumBuffer.writeUInt32BE(txnum);
  var outputIdxBuffer = new Buffer(4);
  outputIdxBuffer.writeUInt32BE(outputIdx);
  

  return Buffer.concat([
    constants.ASSETPREFIXES.ADDRESSTOTX,
    assetAddressBuffer,
    assetIdBuffer,
    heightBuffer,
    txnumBuffer,
    outputIdxBuffer
  ]);
};



exports.decodeAddressToTxKey = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  
  $.checkArgument(buffer.readUInt8() === constants.ASSETPREFIXES.ADDRESSTOTX.readUInt8(), 'LevelDB prefix does not match - wrong decode function?');

  var assetAddress = buffer.slice(1, 21).toString('hex');
  var assetId = buffer.slice(21, 41).toString('hex');
  var height = buffer.readUInt32BE(41);
  var txnum  = buffer.readUInt32BE(45);
  var outputIdx = buffer.readUInt32BE(49);

  return {
    assetAddress: assetAddress,
    assetId: assetId,
    height: height,
    txnum: txnum,
    outputIdx: outputIdx
  }; 
};


exports.encodeAddressToTxValue = function(txId, outputIdx) {
  $.checkArgument(txId && txId.length===32 || txId.length===64, 'First argument is expected to be an txId buffer or hex (32 bytes)');  
  $.checkArgument(isNumeric(outputIdx), 'Second argument is expected to be a numeric output (in tx)');

  var txIdBuffer = (txId.length === 64) ? new Buffer(txId, 'hex') : txId;
  var outputIdxBuffer = new Buffer(4);
  outputIdxBuffer.writeUInt32BE(outputIdx);
  
  return Buffer.concat([
    txIdBuffer,
    outputIdxBuffer
  ]);
};


exports.decodeAddressToTxValue = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  

  var txId = buffer.slice(0, 32).toString('hex');
  var outputIdx = buffer.readUInt32BE(32);

  return {
    txId : txId,
    outputIdx: outputIdx
  };  
};



exports.encodeAssetNameToAssetKey = function(assetName) {
  $.checkArgument(assetName && assetName.length > 0 && assetName.length <= 32, 'First argument is expected to be an assetName string');  

  var assetNameBuffer = new Buffer(assetName, 'utf-8');

  return Buffer.concat([
    constants.ASSETPREFIXES.ASSETNAMETOASSET,
    assetNameBuffer
  ]);
};

exports.decodeAssetNameToAssetKey = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  
  $.checkArgument(buffer.readUInt8() === constants.ASSETPREFIXES.ASSETNAMETOASSET.readUInt8(), 'LevelDB prefix does not match - wrong decode function?');

  var assetName = buffer.slice(1).toString('utf-8');

  return assetName;

};


exports.encodeAssetNameToAssetValue = function(assetId, txId, outputIdx) {
  $.checkArgument(assetId && assetId.length===20 || assetId.length===40, 'First argument is expected to be an assetId Buffer or Hex (20 bytes)');  
  $.checkArgument(txId && txId.length===32 || txId.length===64, 'Second argument is expected to be an txId Buffer or Hex (32 bytes)');  
  $.checkArgument(isNumeric(outputIdx), 'Third argument is expected to be a numeric output (in tx)');

  var assetIdBuffer = (assetId.length === 40) ? new Buffer(assetId, 'hex') : assetId;
  var txIdBuffer = (txId.length === 64) ? new Buffer(txId, 'hex') : txId;
  var outputIdxBuffer = new Buffer(4);
  outputIdxBuffer.writeUInt32BE(outputIdx);

  return Buffer.concat([
    assetIdBuffer,
    txIdBuffer,
    outputIdxBuffer
  ]);
};


exports.decodeAssetNameToAssetValue = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  

  var assetId = buffer.slice(0, 20).toString('hex');
  var txId = buffer.slice(20, 52).toString('hex');
  var outputIdx = buffer.readUInt32BE(52);

  return {
    assetId: assetId,
    txId: txId,
    outputIdx: outputIdx
  };
};



exports.encodeAssetToMetaKey = function(assetId) {
  $.checkArgument(assetId && assetId.length===20 || assetId.length===40, 'First argument is expected to be an assetId Buffer or hex');  

  var assetIdBuffer = (assetId.length == 40) ? new Buffer(assetId, 'hex') : assetId;

  return Buffer.concat([
    constants.ASSETPREFIXES.ASSETTOMETA,
    assetIdBuffer
  ]);
};



exports.decodeAssetToMetaKey = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  
  $.checkArgument(buffer.readUInt8() === constants.ASSETPREFIXES.ASSETTOMETA.readUInt8(), 'LevelDB prefix does not match - wrong decode function?');

  var assetId = buffer.slice(1, 21).toString('hex');

  return{
    assetId:assetId
  };
};


exports.encodeAssetToMetaValue = function(json) {
  json = json || '';

  var valBuffer = new Buffer(json, 'utf-8');

  return(valBuffer);
};

exports.decodeAssetToMetaValue = function(buffer) {
  $.checkArgument(Buffer.isBuffer(buffer), 'First argument is expected to be a Buffer');  

  var jsonString = buffer.toString('utf-8');
  return {
    metadata: jsonString
  };
};


module.exports = exports;


//This encodes and decodes buffers for use with LevelDB
function test_asset_encode_and_decode() {

  //Made up test data
  var TEST = {  
    assetId: '1011121314151617181920212223242526272829',
    assetName: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ012345',
    assetAddress: '3031323334353637383940414243444546474849',
    txId : '0001020304050607080900010203040506070809000102030405060708090001',
    height: 50,
    txnum: 40,
    outputIdx: 30,
    spent: false,
    counter: 25,
    balance: 2100000087654321,
    qty: 21000000876543216,
    unspentQty: 18000000876543216,
    outputIndex: 3,
    metadata: '{"asset_ids":["AXcMmzTXyTQtGTsFT5JAU4Df5o3PUHrNTa"],"contract_url":null,"name_short":"TEST"}'
  };

  var buffer;
  var retval;

  console.log("Testing Asset Encoding");


  console.log("Test encode/decode AssetToTxKey");
  buffer = exports.encodeAssetToTxKey(TEST.assetId, TEST.height, TEST.txnum, TEST.outputIdx);
  retval = exports.decodeAssetToTxKey(buffer);
  console.log(JSON.stringify(retval));
  console.log((retval.assetId === TEST.assetId && retval.height === TEST.height && retval.txnum === TEST.txnum && retval.outputIdx === TEST.outputIdx) ? 'PASSED' : 'FAIL');


  console.log("Test encode/decode AssetToTxValue");
  buffer = exports.encodeAssetToTxValue(TEST.txId, TEST.outputIdx);
  retval = exports.decodeAssetToTxValue(buffer);
  console.log((retval.txId === TEST.txId && retval.outputIdx === TEST.outputIdx) ? 'PASSED' : 'FAIL');


  console.log("Test encode/decode TxToAssetKey");
  buffer = exports.encodeTxToAssetKey(TEST.txId, TEST.outputIndex);
  retval = exports.decodeTxToAssetKey(buffer);
  console.log((retval.txId === TEST.txId && retval.outputIndex===TEST.outputIndex) ? 'PASSED' : 'FAIL');


  console.log("Test encode/decode TxToAssetValue");
  buffer = exports.encodeTxToAssetValue(TEST.assetId, TEST.assetAddress, TEST.height, TEST.txnum, TEST.outputIdx, TEST.qty, TEST.unspentQty);
  retval = exports.decodeTxToAssetValue(buffer);
  console.log(JSON.stringify(retval));
  if (retval.assetId===TEST.assetId && retval.assetAddress===TEST.assetAddress && retval.height===TEST.height && retval.txnum===TEST.txnum && retval.outputIdx===TEST.outputIdx  && retval.qty==TEST.qty && retval.unspentQty===TEST.unspentQty) 
    console.log('PASSED');
  else
    console.log('FAIL');


  console.log("Test encode/decode AddressToBalanceKey");
  buffer = exports.encodeAddressToBalanceKey(TEST.assetAddress, TEST.assetId);
  retval = exports.decodeAddressToBalanceKey(buffer);
  console.log(JSON.stringify(retval));
  if (retval.address===TEST.address && retval.assetId===TEST.assetId) 
    console.log('PASSED');
  else
    console.log('FAIL');


  console.log("Test encode/decode AddressToBalanceValue");
  buffer = exports.encodeAddressToBalanceValue(TEST.balance);
  retval = exports.decodeAddressToBalanceValue(buffer);
  console.log(JSON.stringify(retval));
  console.log( (retval.balance===TEST.balance) ? 'PASSED' : 'FAIL');



  console.log("Test encode/decode AssetToBalanceKey");
  buffer = exports.encodeAssetToBalanceKey(TEST.assetId, TEST.assetAddress);
  retval = exports.decodeAssetToBalanceKey(buffer);
  console.log(JSON.stringify(retval));
  if (retval.assetId===TEST.assetId && retval.address===TEST.address) 
    console.log('PASSED');
  else
    console.log('FAIL');


  console.log("Test encode/decode AssetToBalanceValue");
  buffer = exports.encodeAssetToBalanceValue(TEST.balance);
  retval = exports.decodeAssetToBalanceValue(buffer);
  console.log(JSON.stringify(retval));
  console.log( (retval.balance===TEST.balance) ? 'PASSED' : 'FAIL');


  console.log("Test encode/decode AddressToTxKey");
  buffer = exports.encodeAddressToTxKey(TEST.assetAddress, TEST.assetId, TEST.height, TEST.txnum, TEST.outputIdx);
  retval = exports.decodeAddressToTxKey(buffer);
  console.log(JSON.stringify(retval));
  if (retval.address===TEST.address && retval.assetId===TEST.assetId && retval.height===TEST.height && retval.txnum===TEST.txnum && retval.outputIdx===TEST.outputIdx) 
    console.log('PASSED');
  else
    console.log('FAIL');


  console.log("Test encode/decode AddressToTxValue");
  buffer = exports.encodeAddressToTxValue(TEST.txId, TEST.outputIdx);
  retval = exports.decodeAddressToTxValue(buffer);
  console.log(JSON.stringify(retval));
  if (retval.txId===TEST.txId && retval.outputIdx===TEST.outputIdx) 
    console.log('PASSED');
  else
    console.log('FAIL');

  console.log("Test encode/decode AssetNameToAssetKey");
  buffer = exports.encodeAssetNameToAssetKey(TEST.assetName);
  retval = exports.decodeAssetNameToAssetKey(buffer);
  console.log(JSON.stringify(retval));
  if (TEST.assetName===retval) 
    console.log('PASSED');
  else
    console.log('FAIL');


  console.log("Test encode/decode AssetNameToAssetValue");
  buffer = exports.encodeAssetNameToAssetValue(TEST.assetId, TEST.txId, TEST.outputIdx);
  retval = exports.decodeAssetNameToAssetValue(buffer);
  console.log(JSON.stringify(retval));
  if (retval.assetId===TEST.assetId && retval.txId===TEST.txId && retval.outputIdx===TEST.outputIdx) 
    console.log('PASSED');
  else
    console.log('FAIL');

  console.log("Test encode/decode AssetToMetaKey");
  buffer = exports.encodeAssetToMetaKey(TEST.assetId);
  retval = exports.decodeAssetToMetaKey(buffer);
  console.log(JSON.stringify(retval));
  if (retval.assetId === TEST.assetId)
    console.log('PASSED');
  else 
    console.log('FAIL');


  console.log("Test encode/decode AssetToMetaValue");
  buffer = exports.encodeAssetToMetaValue(TEST.metadata);
  retval = exports.decodeAssetToMetaValue(buffer);
  //console.log(JSON.stringify(retval));
  if (retval.metadata === TEST.metadata)
    console.log('PASSED');
  else 
    console.log('FAIL');


}

//test_asset_encode_and_decode();