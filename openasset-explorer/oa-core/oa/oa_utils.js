'use strict';

var request = require("request");

//var oa = function(options) {};

exports.getAssetMeta = function(metadata, callback) {
	var url;

	//console.log('MetaData is ' + metadata);
	metadata = metadata || '';
	//console.log('MetaData is now ' + metadata);

	if (Buffer.isBuffer(metadata))
	  metadata = metadata.toString('utf-8');

	if (metadata.substring(0,6) == 'u=http')
	  url = metadata.substring(2);
	else if (metadata.substring(0,4) == 'http')
	  url = metadata;


	if (url) {
		var options = {
			url : url,
			timeout : 2000 
		}
		request(options, function(error, response, body) {
		  if (error) {
		  		console.log("Error getting asset metadata from " + url + ' Error:' + error);
		  		return callback(error);
		  }
		  //console.log('response: ' + response);
		  //console.log('Metadata:'+ metadata);	 
		  if (response.statusCode == 200) {
		  	try {
		  	  metadata = JSON.parse(body);
		  	  callback(null, metadata);
		  	} catch(e) {
		  		return callback(null, {name: ''});
		  	}
		  }
		  else
		  {
		  	console.error('url call failed: ' + url);
		  	callback(null, {url: url, name:''});
		  }
		});
	}
	else
	{
		//If not a url, just return json name
		callback(null, {name: metadata} );	
	} 

}

//This should be used only for testing, and debugging
//We should not rely on Coinprism API
exports.getCoinprismTx = function(txId) {
  var url;
  if (Buffer.isBuffer(txId))
  	txId = txId.toString('hex');

  url = 'https://api.coinprism.com/v1/transactions/' + txId;

  request(url, function(error, response, body) {
  	if (error)
  		console.log('Could not query Coinprism API for tx: ' + txId)
    else {

        	console.log(txId +'--->' +  body);
        }

  });
}

//////////////
//  TEST    //
//////////////
// var mdata = 'https://chroma.fund/c/ALtZ3A';
// //var mdata = 'Blahcoin';

// oa.prototype.getAssetMeta(mdata, function (err, data) {
// 		if (err)
// 		  console.error(err);
// 		else
// 		  console.log(data);
// 	});

module.exports = exports;