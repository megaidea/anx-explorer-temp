//Use this sample service to create additional services that depend on oa-core

'use strict';

var inherits = require('util').inherits;
var EventEmitter = require('events').EventEmitter;

function Sample_Service(options) {
  EventEmitter.call(this);
  this.node = options.node;
}

inherits(Sample_Service, EventEmitter);

Sample_Service.dependencies = ['bitcoind', 'oa-core'];

Sample_Service.prototype.start = function(callback) {
  setImmediate(callback);
};

Sample_Service.prototype.stop = function(callback) {
  setImmediate(callback);
};

Sample_Service.prototype.getRoutePrefix = function() {
  return 'test';
};

Sample_Service.prototype.setupRoutes = function(app) {
  app.get('/test', this.testWeb.bind(this));
};

Sample_Service.prototype.getAPIMethods = function() {
  return [];
};

Sample_Service.prototype.getPublishEvents = function() {
  return [];
};

Sample_Service.prototype.getPublishEvents = function() {
  return [];
};

Sample_Service.prototype.testWeb = function(req, res, next) {
    res.status(200).send('OK');
}



module.exports = Sample_Service;