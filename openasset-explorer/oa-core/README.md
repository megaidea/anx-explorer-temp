##OA-Core
This Open Assets plug-in works with BitPay's bitcore to pre-scan the bitcoin blockchain and continually update indexes for Open Assets contained within the bitcoin blockchain.

It also provides an API to get information about Open Assets

It will work while bitcoind is catching up, but works best with a fully synchronized bitcoin blockchain.   As an optimization, it will only scan for Open Asset transactions after block 300,000 on livenet, and block 240000 on testnet.

###API (subject to change)
Returns the most recently processed block by openassets forward scanner.  May differ from bitcoind block height while catching up.  
http://localhost:3001/openassets/height

Get list of transactions for an assetId  
`txs/<assetId>`  
http://localhost:3001/openassets/txs/AGvARPLULWb2B1WYLjYmKMnNWeQfCx3wqS  
http://localhost:3001/openassets/txs/AGvARPLULWb2B1WYLjYmKMnNWeQfCx3wqS?startBlock=10000&endBlock=350000

getRecentTransactions(assetId, [count])  
Use ?count=X  to show the most recent X transactions for the asset (in recent->older order)    
http://localhost:3001/openassets/txs/AGvARPLULWb2B1WYLjYmKMnNWeQfCx3wqS?count=1

Get an assetId by short_name  
http://localhost:3001/openassets/asset/WEC

Get metadata about the asset that we've cached (note some is long gone if issuer did not maintain their url)  
http://localhost:3001/openassets/asset/meta/AYmhY8yijsktwc5PiKCQFHAve8kvbCpZjL

Get a list of all assets by name  
http://localhost:3001/openassets/assets

Get info on an openasset tranasaction:  
http://localhost:3001/openassets/tx/c9feb23d54925e71a21828da69f80bd050b88e3347414fa27c629138832e71a1

Get information about an addresss - What assets does it contain and what txId/Output  
http://localhost:3001/openassets/txs/address/anUFXQHWrrsvkK1L8Mo7TBptZcH2DmqxNQJ

Get all tx and outputIdx for an asset and address  (asset first)  
http://localhost:3001/openassets/txs/AchDKMsBeAfAQPBPzjHV8bB9WbBYJ1oEcp/anWX5cQFeeD9g69ZPfTHgkxcJHdAckfbrb4

Get all tx Outputs for an asset and address  (asset goes first)  
http://localhost:3001/openassets/txs/AchDKMsBeAfAQPBPzjHV8bB9WbBYJ1oEcp/anWX5cQFeeD9g69ZPfTHgkxcJHdAckfbrb4

Get all UNSPENT (UTXO) for an asset and address (asset goes first)  
http://localhost:3001/openassets/utxos/AchDKMsBeAfAQPBPzjHV8bB9WbBYJ1oEcp/anWX5cQFeeD9g69ZPfTHgkxcJHdAckfbrb4

Get ALL transaction outputs (txo) with optional count   
http://localhost:3001/openassets/txos?count=5

Get balance for an asset/address  
Balance for WEC (largest holder)  
http://localhost:3001/openassets/balance/AGXSTkTjYAZRh9Q3484vqxus355VzjYF5v/anYwPsucB74q5QKaVKjZtwqKKsKfxJNrDar

Get balances for all assets and all addresses  
http://localhost:3001/openassets/balances

Get owners addresses and balances for an asset  
http://localhost:3001/openassets/owners/akRNa83e6Cp7aswkuvbY8J69MwjTfQeLwBq


###Installation

Recommend using node version v4.2.6.  Use nvm if necessary.

Install bitcore.  Begin here: https://bitcore.io/start

Optionally sync the bitcoin blockchain, or jump-start with a pre-synchronized data folder.

From the `node_modules` folder where you see bitcore-node folder:
`ln -s <your path>/anx-explorer/oa-core`

From the oa-core folder, run `npm install` to install the dependencies.

Copy `bitcore-node.json` to the root of the bitcore installation.

Start normally with:
`bitcored`

To reset the index, stop bitcore, reset, and scan the blockchain again.
`./reset`

###TODO

 - Need to store an additional flag for whether it is multisig address, so address can be encoded with different first byte. (Example: issuing address of WEC)
 - Additional testing to ensure accuracy of the information.
 - Switch in-file testing to mocha/chai/sinon
 - Handle incoming mempool transactions.  Currently only handling confirmed blocks.
 - Add oa/tx to message bus to allow subscription to open asset transaction events.
 - Add oa/address to message bus to allow subscription to specific address events.
 - Add oa/asset to message bus to allow subscription to specific asset events.
 - Add additional API calls (as needed)
 - Look at anomalies and DO NOT INDEX so that they cannot be mistaken for valid UTXOs
 - Insufficient inputs - Check to find reason and fix or flag to not index the transaction.
 - Partial input assignment resulting in permanent loss of asset.  Make sure it is assigning inputs correctly.
 - Remove debugging information
 

###Limitations
Because of the way Open Assets works, the meta data is referenced by a URL supplied by the open asset issuer.  The meta-data is not stored in the bitcoin blockchain.  This meta-data may disappear at any time if the issuer stops supporting the site.  The open asset issuances and transfers will remain in the bitcoin blockchain, but the meta information (asset name, divisibility, etc) about the asset may be lost forever.   OA-Core does try to index the meta information on each initial scan.  If the information exists at the supplied URL at the time of the scan, it will be indexed.   All Open Asset explorers that did not capture the data while the meta-data URLs were live will have the same limitation.

The open asset data is not validated or checked by the bitcoin miners.  Anyone can shove bogus data into the bitcoin blockchain in the area that open assets uses.   Spot checks show that most of the data is valid.   Most of the anomalies are detected and logged to the console, but they have not yet been checked to determine the cause.

