'use strict';
var async = require("async");
/**
 * A list that holds all the colored input and output transactions for a particular transaction
 */
var computedAssetIdCache = [];
function TransactionList(coloringEngine) {
    this.list = new Array();
    this.coloringEngine = coloringEngine;
    this.checkedLisItem = [];
    this.inputTransactionInProgress = [];
}

TransactionList.prototype.ItemMetadata = function (transactionHash, coloredTransactionInfo, coloredOutput, outputIndex, isTransactionInList) {
    this.transactionHash = transactionHash;
    this.coloredTransactionInfo = coloredTransactionInfo;
    this.coloredOutput = coloredOutput;
    this.isTransactionInList = isTransactionInList;
    this.outputIndex = outputIndex;
}

TransactionList.prototype.addAll = function (transactions) {
    for (var index = 0; index < transactions.length; ++index) {
        var transaction = transactions[index];
        this.add(transaction);
    }
}

TransactionList.prototype.add = function (transaction) {

    var transactionHash = transaction.hash;
    if (this.getItem(transactionHash)) {
        return;
    }
    var coloredTransactionInfo = this.coloringEngine.getColoredTransactionInfo(transaction);
    var coloredOutput = coloredTransactionInfo ? null : this.coloringEngine.makeUncoloredResponse(transaction);
    var self = this;
    var inputTransactions = new Array();;

    for (var index = 0; index < transaction.inputs.length; ++index) {
        var inputTransaction = transaction.inputs[index];
        var inputTransactionHash = inputTransaction.prevTxId.toString('hex');
        var outputIndex = inputTransaction.outputIndex;
        //The input transactions are not added in the List yet, so isTransactionInList set to false
        var isTransactionInList = false;
        inputTransactions.push(
            {
                transaction: inputTransaction,
                metadata: new self.ItemMetadata(inputTransactionHash, null, null, outputIndex, isTransactionInList),
            }
            )
    }

    var newListItem = {
        transaction: transaction,
        metadata: new self.ItemMetadata(transactionHash, coloredTransactionInfo, coloredOutput, null, true),
        inputTransactions: inputTransactions
    };

    this.list.push(newListItem);
    this.updateAllInputTransactionMetadata(newListItem);
}

TransactionList.prototype.updateAllInputTransactionMetadata = function (newListItem) {
    for (var index = 0; index < this.list.length; ++index) {
        var listItem = this.list[index];
        for (var transactionIndex = 0; transactionIndex < listItem.inputTransactions.length; ++transactionIndex) {
            var inputTransaction = listItem.inputTransactions[transactionIndex];
            if (newListItem.metadata.transactionHash == inputTransaction.metadata.transactionHash) {
                inputTransaction.metadata.isTransactionInList = true;
                inputTransaction.metadata.coloredOutput = newListItem.metadata.coloredOutput;
                inputTransaction.metadata.coloredTransactionInfo = newListItem.metadata.coloredTransactionInfo;
            }

        }
    }
}

TransactionList.prototype.updateAllInputTransactionColoredOutput = function (computedOutputsListItems) {

    var self = this;
    for (var index = 0; index < computedOutputsListItems.length; ++index) {
        var computedListItem = computedOutputsListItems[index];
        for (var itemIndex = 0; itemIndex < self.list.length; ++itemIndex) {
            var listItem = self.list[itemIndex];
            for (var inputTransactionIndex = 0; inputTransactionIndex < listItem.inputTransactions.length; ++inputTransactionIndex) {
                var inputTransaction = listItem.inputTransactions[inputTransactionIndex];
                if (inputTransaction.metadata.transactionHash == computedListItem.metadata.transactionHash) {
                    inputTransaction.metadata.coloredOutput = computedListItem.metadata.coloredOutput;
                }
            }
        }
    }
}


TransactionList.prototype.getItem = function (transactionHash) {
    for (var listIndex = 0; listIndex < this.list.length; ++listIndex) {
        var listItem = this.list[listIndex];
        if (listItem.metadata.transactionHash == transactionHash) {
            return listItem;
        }
    }
    return null;
}

TransactionList.prototype.hasComputedAllColoredOutputs = function () {
    var notComputedCount = 0;
    for (var listIndex = 0; listIndex < this.list.length; ++listIndex) {
        var listItem = this.list[listIndex];
        if (listItem.metadata.coloredOutput == null) {
            ++notComputedCount;
        }
    }
    return notComputedCount <= 0;
}


TransactionList.prototype.hasMultipleAssetsInSingleTransaction = function (coloredOutputs) {
    var assetIds = [];
    for (var index = 0; index < coloredOutputs.length; ++index) {
        var coloredOutput = coloredOutputs[index];
        if (coloredOutput.assetId) {
            assetIds[coloredOutput.assetId.toString('hex')] = {};
        }
    }
    return Object.keys(assetIds).length > 1;
}

TransactionList.prototype.computeAllColoredOutputs = function (transaction, callback) {
    var self = this;

    var computeColoredOutputFunc = function () {
        if (!self.hasComputedAllColoredOutputs()) {
            var computedListItems = [];
            for (var index = 0; index < self.list.length; ++index) {
                var listItem = self.list[index];
                var coloredInputs = [];
                if (listItem.metadata.coloredOutput) {
                    //Skip the uncolored transaction or the coloredOutput is already calculated
                    continue;
                }

                var hasAllColoredInputTransactions = true;
                for (var inputTransactionIndex = 0; inputTransactionIndex < listItem.inputTransactions.length; ++inputTransactionIndex) {
                    var inputTransaction = listItem.inputTransactions[inputTransactionIndex];
                    if (!inputTransaction.metadata.coloredOutput) {
                        hasAllColoredInputTransactions = false;
                        break;
                    }
                    else {
                        coloredInputs.push(inputTransaction.metadata.coloredOutput[inputTransaction.metadata.outputIndex]);
                    }
                }

                if (hasAllColoredInputTransactions) {
                    if (self.hasMultipleAssetsInSingleTransaction(coloredInputs)) {
                        console.log("Not support multiple asset types");
                        throw { error: { code: 1, message: "Multiple assets in a single transaction is not supported" } };
                    }
                    var outputsWithAssetIds = computedAssetIdCache[listItem.metadata.transactionHash];
                    if (!outputsWithAssetIds) {
                        outputsWithAssetIds = self.coloringEngine._computeAssetIds(
                            coloredInputs,
                            listItem.metadata.coloredTransactionInfo.outputIndex,
                            listItem.transaction.outputs,
                            listItem.metadata.coloredTransactionInfo.markerOutput.assetQuantities);
                        computedAssetIdCache[listItem.metadata.transactionHash] = outputsWithAssetIds;
                    }
                    if (outputsWithAssetIds) {
                        listItem.metadata.coloredInput = coloredInputs;
                        listItem.metadata.coloredOutput = outputsWithAssetIds;
                    } else {
                        //set it to uncolored transaction for invalid open asset outputs
                        listItem.metadata.coloredInput = [];
                        listItem.metadata.coloredOutput = self.coloringEngine.makeUncoloredResponse(listItem.transaction);
                    }
                    computedListItems.push(listItem);
                }
            };
            self.updateAllInputTransactionColoredOutput(computedListItems);
            process.nextTick(computeColoredOutputFunc);
        } else {
            return callback(null,self.getItem(transaction.hash));
        }
    }
    process.nextTick(computeColoredOutputFunc);
}

/***
 * Get the input transaction that is not in the List
 */
TransactionList.prototype.backtrackPreviousInputTransactions = function (callback) {
    var self = this;
    var hasMoreItems = false;
    var getTransactionFuncs = [];
    for (var listIndex = 0; listIndex < this.list.length; ++listIndex) {
        var listItem = this.list[listIndex];
        if (self.checkedLisItem[listItem.metadata.transactionHash]) {
            continue;
        }
        if (!listItem.metadata.coloredTransactionInfo) {
            //Not a colored transaction, skip it
            self.checkedLisItem[listItem.metadata.transactionHash] = true;
            continue;
        }
        for (var transactionIndex = 0;
            transactionIndex < listItem.inputTransactions.length;
            ++transactionIndex) {

            var inputTransaction = listItem.inputTransactions[transactionIndex];
            if (!inputTransaction.metadata.isTransactionInList
                && self.getItem(inputTransaction.metadata.transactionHash) == null) {
                hasMoreItems = true;
                var inProgress = self.inputTransactionInProgress[inputTransaction.metadata.transactionHash];
                if (!inProgress) {
                    self.inputTransactionInProgress[inputTransaction.metadata.transactionHash] = true;
                    var getInputTransaction = function (currentInputTransaction, currentListItem, callback) {
                        self.coloringEngine.getTransactionByHash(currentInputTransaction.metadata.transactionHash,
                            function (err, nextTransaction) {
                                if (err) {
                                    console.log("error! failed to get transaction " + currentInputTransaction.hash);
                                    //Cannot get the input transaction by the hash, it is a rare case and should only
                                    //happen in the testnet. To avoid endless looping and recursion,
                                    //just treat the whole transaction is uncolored 
                                    currentInputTransaction.metadata.isTransactionInList = true;
                                    currentInputTransaction.metadata.coloredTransactionInfo = null;
                                    currentListItem.metadata.coloredTransactionInfo = null;
                                    return callback("cannot get transaction!", null);
                                }
                                return callback(null, nextTransaction);
                            });
                    }
                    getTransactionFuncs.push(getInputTransaction.bind(null, inputTransaction, listItem));
                }
            }
        }
        self.checkedLisItem[listItem.metadata.transactionHash] = true;
    }
    if (getTransactionFuncs.length > 0) {
        async.parallel(getTransactionFuncs, function (err, transactions) {
            if (err) {
                return callback(err, null);
            }
            callback(null, transactions);
        });
    } else {
        callback(null, null);
    }
}
module.exports = TransactionList;