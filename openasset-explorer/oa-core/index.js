/**************************************& 
 * Copyright 2016 ANX International    *
 * Author: Tron Black                  *
 * Purpose: Add Open Assets to bitcore *
 ***************************************/ 

"use strict";

var util = require("util");
var EventEmitter = require("events").EventEmitter;
var fs = require("fs");
var assert = require("assert");
var async = require("async");
var sync = require("synchronize");
var levelup = require("levelup");
var leveldown = require("leveldown");
var mkdirp = require("mkdirp");
delete global._bitcore;
var bitcore = require("bitcore-lib");
var BufferUtil = bitcore.util.buffer;
var Networks = bitcore.Networks;
var Block = bitcore.Block;
var $ = bitcore.util.preconditions;
var encoding = require("./oa/encoding");
var constants = require("./oa/constants");
var oa = require("./oa/oa_utils");
var coloringengine = require("./coloringengine");
var request = require('request');

function enableCors(response) {
  // A convenience function to ensure
  // the response object supports cross-origin requests for APIs
  response.setHeader('Content-Type', 'application/json');
  response.set('Access-Control-Allow-Origin','*');
  response.set('Access-Control-Allow-Methods','POST, GET, OPTIONS, PUT');
  response.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

}

function isNumeric(n){
  return (typeof n == "number" && !isNaN(n));
}

function OpenAssetService(options) {
  if (!(this instanceof OpenAssetService)) {
    return new OpenAssetService(options);
  }
  if (!options) {
    options = {};
  }

  this.version = '0.9.0';
  this.assets = options.assets;
  this.datadir = options.datadir;
  this.network = options.node.network;
  this.node = options.node;
  this.name = options.name;


  $.checkState(this.node.network, 'Node is expected to have a "network" property');
  this.network = this.node.network;

  //Optimiazation to start pre-indexing blocks at beginning of openasset (June 2014)
  if (this.network.name=='livenet') {
    this.genesis = {hash:'000000000000000082ccf8f1557c5d40b21edabb18d2d691cfbf87118bac7254', __height:300000};  //First livenet block with colored trans?
  } else if (this.network.name=='testnet') {
    this.genesis = {hash:'00000000d8c25f89da0a1ddee3f3fe9efd23e0068d8b6e4b1ec2b81da8757611', __height:240000};  //First one for testenet in block 258684  -- found one in 245730
  } else {
    this.genesis = null;
  }

  console.log('this.genesis: ' + JSON.stringify(this.genesis));

  EventEmitter.call(this);

  this.setDataPath();

  this.levelupStore = leveldown;
  if (options.openasset_store) {
    this.levelupStore = options.openassets_store;
  }
  this.retryInterval = 60000;
  this.log = this.node.log;

  this.node.services.bitcoind.on('tx', this.transactionHandler.bind(this));

  this.ce = new coloringengine.ColoringEngine(this.getTransactionProviderNative());

}
util.inherits(OpenAssetService, EventEmitter);

OpenAssetService.dependencies = ['bitcoind'];


OpenAssetService.prototype.getTransactionProviderNative = function () {
  var self = this;
  return function transactionProvider(hash, cb) {
    self.node.getTransaction(hash ,cb);
  };
};

/**
 * This function will set `this.dataPath` based on `this.node.network`.
 * @private
 */
OpenAssetService.prototype.setDataPath = function() {
  $.checkState(this.node.services.bitcoind.spawn.datadir, 'bitcoind is expected to have a "spawn.datadir" property');
  // console.log(this);
  // console.log(this.network);
  // console.log(Networks.livenet);
  var datadir = this.node.services.bitcoind.spawn.datadir;
  if (this.network.name === Networks.livenet.name) {
    this.dataPath = datadir + '/openasset-db';
  } else if (this.network.name === Networks.testnet.name) {
    if (this.network.regtestEnabled) {
      this.dataPath = datadir + '/regtest/openasset-db';
    } else {
      this.dataPath = datadir + '/testnet3/openasset-db';
    }
  } else {
    throw new Error('Unknown network: ' + this.network);
  }
};

OpenAssetService.prototype.loadTip = function(callback) {
  var self = this;
  var hash;

  var options = {
    keyEncoding: 'binary',
    valueEncoding: 'binary'
  };

  self.openassets_store.get(constants.ASSETPREFIXES.ASSETTIP, options, function(err, tipData) {
    if(err && err instanceof levelup.errors.NotFoundError) {
      self.tip = self.genesis;
      hash = self.tip.hash;
      self.connectBlock(self.genesis, function(err) {
        if(err) {
          return callback(err);
        }

        self.emit('addblock', self.genesis);
      });
    } else {
      hash = tipData.toString('hex');
    }

    //console.log('Hash:' + hash);    

    var total_times = 100;
    var times = 0;
    async.retry({times: total_times, interval: self.retryInterval}, function(done) {
      self.node.getBlock(hash, function(err, tip) {
        if(err) {
          times++;
          self.log.warn('Bitcoind does not have our tip (' + hash + '). Bitcoind may have crashed and needs to catch up.' + ' Looking for Block#: ' + self.genesis.__height);
          if(times < total_times) {
            self.log.warn('Retrying ' + times + ' of ' + total_times + ' in ' + (self.retryInterval / 1000) + ' seconds.');
          }
          return done(err);
        }

        done(null, tip);
      });
    }, function(err, tip) {
      if(err) {
        self.log.warn('Giving up after 100 tries. Please report this bug to https://github.com/bitpay/bitcore-node/issues');
        self.log.warn('Please reindex your database.');
        return callback(err);
      }

      self.tip = tip;
      self.node.getBlockHeader(self.tip.hash, function(err, blockHeader) {
        if (err) {
          return callback(err);
        }
        if(!blockHeader) {
          return callback(new Error('Could not get height for tip.'));
        }
        self.tip.__height = blockHeader.height;
        callback();
      });

    });
  });
};

/**
 * Connects a block to the database and add indexes
 * @param {Block} block - The bitcore block
 * @param {Function} callback
 */
OpenAssetService.prototype.connectBlock = function(block, callback) {
  //this.log.info('adding block', block.hash);
  this.blockHandler(block, true, callback);
};

/**
 * Disconnects a block from the database and removes indexes
 * @param {Block} block - The bitcore block
 * @param {Function} callback
 */
OpenAssetService.prototype.disconnectBlock = function(block, callback) {
  this.log.info('disconnecting block', block.hash);
  this.blockHandler(block, false, callback);
};



/**
 * This will handle data from the daemon "tx" event, go through each of the outputs
 * and send messages by calling `transactionEventHandler` to any subscribers for a
 * particular address.
 * @param {Object} txInfo - The data from the daemon.on('tx') event
 * @param {Buffer} txInfo.buffer - The transaction buffer
 * @param {Boolean} txInfo.mempool - If the transaction was accepted in the mempool
 * @param {String} txInfo.hash - The hash of the transaction
 * @param {Function} [callback] - Optional callback
 */
OpenAssetService.prototype.transactionHandler = function(txInfo, callback) {
  var self = this;

  //Disabled for now - use for mempool
  return;
  console.log('transactionHandler', txInfo);

  callback();

  if (!callback) {
    callback = function(err) {
      if (err) {
        return log.error(err);
      }
    };
  }

  if (this.node.stopping) {
    return callback();
  }

  //console.log("OpenAssets Transaction Handler");
  //console.log(txInfo);
  //console.log(JSON.stringify(txInfo));

  // Basic transaction format is handled by the daemon
  // and we can safely assume the buffer is properly formatted.
  var tx = bitcore.Transaction().fromBuffer(txInfo.buffer);
  //console.log(tx);
  //console.log(JSON.stringify(tx));

  self.coloringEngine.isColored(tx,function(err,result){
    if (err) {
      console.log(err);
    }
    if (result){
      //console.log("Found colored transaction - " + tx.hash);
      //console.log(result);
      self.coloringEngine.colorTransaction_v2(tx, function(err, data) {
        console.log("Colored Transaction Data");
        console.log(data);  
      } );
    }
    else
    {
      console.log("Not colored transaction.");
    }
  });


  var messages = {};

  var outputsLength = tx.outputs.length;
  for (var i = 0; i < outputsLength; i++) {
    this.transactionOutputHandler(messages, tx, i, !txInfo.mempool);
  }

  function finish(err) {
    if (err) {
      return callback(err);
    }
    for (var key in messages) {
      self.transactionEventHandler(messages[key]);
      self.balanceEventHandler(null, messages[key].addressInfo);
    }
    callback();
  }

  //console.log(txInfo);

  if (txInfo.mempool) {
    //self.updateOpenAssetIndex(tx, true, finish);
  } else {
    setImmediate(finish);
  }

};



OpenAssetService.prototype.blockHandler = function(block, add, callback) {
  if (this.node.stopping) {
    return callback();
  }

  //console.log("In oa-core blockHandler - Block# " + block.__height);
  
  var self = this;
  var txs = block.transactions;
  var operations = [];
  var txIdx = 0;

    // Update tip - keeps track of where we left off in indexing
  var tipHash = add ? new Buffer(block.hash, 'hex') : BufferUtil.reverse(block.header.prevHash);
  operations.push({
    type: 'put',
    key: constants.ASSETPREFIXES.ASSETTIP,
    value: tipHash
  });


  //Process the tx in series.  We may need info from previous ones
  async.eachSeries(txs, function(tx, next) {
    //console.log('Handling tx #' + txIdx)
    self.txHandler(block, tx, txIdx, add, function(err, ops) {
      //console.log('Returned ops len', ops.length);
      //console.log('ops value', ops);
      
      //operations = operations.concat(ops);
      //Batch these every tx because we may need them for the next tx
      self.openassets_store.batch(ops, function(err) {
        if (err)
          console.log('Storage error: ' + err)
	      //console.log('Total operations length', operations.length);
	      //console.log('operations value', operations);
	      //console.log('Handled tx #' + txIdx)
	      //operations = [];
	      txIdx++;
	      next();
      });

    });
  }, function done() {
      //Here to handle the operations for final transaction in the block
      self.openassets_store.batch(operations, function (err) {
        //console.log('Finished handling block#' + block.__height);
      	callback(null, []);
      });
  });
};


//Search for a coloredOutput in the transaction
//Only the first one is allowed/used, so stop searching after we find one.
OpenAssetService.prototype.txHandler = function(block, tx, txIdx, addOutput, callback) {
  if (height < 290000)  //Ignore blocks before the March 11, 2014 block.  Beta of open assets launched March 13, 2014
    return callback(null, []);
  
  var self = this;
  var height = block.__height;

  var coloredOutput = false;;

  var action = 'put';
  if (!addOutput) {
    action = 'del';
  }

  var ops = [];

  //If tx is Coinbase, no open assets (optimization)
  if (tx.isCoinbase())
    return callback(null, []);


  var txid = tx.id;
  var txidBuffer = new Buffer(txid, 'hex');
  var inputs = tx.inputs;
  var outputs = tx.outputs;

  //console.log('tx', JSON.stringify(tx));
  //console.log('Checking block#' + height + ' tx#' + txIdx);


  var outputLength = outputs.length;
  for (var outputIndex = 0; (!coloredOutput) && (outputIndex < outputLength); ++outputIndex) {
    var output = outputs[outputIndex];

    var script = output.script;

    if(!script) {
      console.error('Invalid script');
      continue;
    }

    //console.log('Checking block#' + height + ' tx#' + txIdx +  ' Output#' + outputIndex);


    //Check for colored output, with error logging to handle poorly formatted outputs

    try {
      coloredOutput = this.ce.isOutputColored(output);
    } catch(e) {
      console.error(e);
    }

    if (coloredOutput) {


      //console.log("Found colored transaction - " + tx.hash + ' tx#' + txIdx +  ' Output#' + outputIndex);

      this.getColoredTx(action, ops, tx, height, txIdx, outputIndex, function (err, cti) {
        var tx_ops = [];
        //console.log('cti: ' + JSON.stringify(cti));
        self.indexAll(cti, function(err, ops) {
            tx_ops = tx_ops.concat(ops);
            console.log('Finished indexing. ' +  tx_ops.length + ' ops to add to db');
            //console.log('Finished Checking block#' + height + ' tx#' + txIdx +  ' Output#' + outputIndex);
            return callback(null, tx_ops);
        });
        //console.log('cti.db.ops: ' + cti.db.ops);
        //
        //console.log('ops: ' + ops);

      });

      //break;   //Only one per transaction, so break out of the loop

    } else {
      //console.log("Not colored transaction.");
    }

  }


  if (!coloredOutput)  //If scanned all outputs and did not find one, then we're done
    callback(null, []);

}



//Gets information about a colored transaction from the tx itself, and
//from available db info to prevent recursive backtracking.
//Only needs to grab input hashes and look up in TxToAsset index.
OpenAssetService.prototype.getColoredTx = function(action, ops, tx, height, txIndex, outputMarkerIndex, cb) {
  var self = this;
  var coloredTxInfo = {
    tx: tx,
    height: height,
    txIndex: txIndex,
    outputMarkerIndex: outputMarkerIndex
  };
 
  coloredTxInfo.markerOutput = this.ce.getColoredOutputInfo(tx.outputs[outputMarkerIndex]);
  //console.log('coloredTxInfo.markerOutput:' + coloredTxInfo.markerOutput);
  coloredTxInfo.db = { action: action, ops: ops }; 

  //coloredTxInfo.assetAddresses = this.getAssetAddresses(tx, txIndex, outputMarkerIndex);

  //console.log('coloredTxInfo:' + coloredTxInfo);
  this.getAssetIssuances(coloredTxInfo, function(err, cti) {
  	self.getAssetTransfers(coloredTxInfo, function(err, cti) {
      self.getAssetMetaData(coloredTxInfo, function (err, cti) {
        cb(null, cti);
      });
  	});
  });
} 


OpenAssetService.prototype.getAssetMetaData = function (cti, cb) {
  var self = this;
  oa.getAssetMeta(cti.markerOutput.metadata, function (err, metadata) {
    cti.metadata_acquired = metadata;
    self.indexAssetToMeta(cti);
    self.indexNameToAsset(cti);
    cb(null, cti);
  });
};


//Return array of input tx hashes
var getInputsFromTx = function(tx) {
  var inputs = [];
  for(var i=0;i<tx.inputs.length;i++) {
    inputs.push({txId: tx.inputs[i].prevTxId, outputIndex: tx.inputs[i].outputIndex});
  }
  return(inputs);
};


// Expects a list of transactions (in Buffer format), and looks up the data
// for each one (AssetId and qty)
OpenAssetService.prototype.getAssetIdAndQtyFromIndex = function (cti, callback) {
  var self = this;
  //console.log('All of cti: ' +  JSON.stringify(cti));
  //console.log('Looking up Asset and Qty info for these TxIdBuffers: ' + cti.inputList);
  cti.inputs = [];

  //console.log('inputList: ' + cti.inputList);
  async.each(cti.inputList, function(txIdObj, each_callback) {
    //console.log('getAssetByTxId - Buffer (hex): ' + txIdObj.txId.toString('hex'));   
    self.getAssetByTxId(txIdObj.txId, txIdObj.outputIndex, function (err, dta) {

        if (dta.assetId)
        	dta.assetIdFriendly = self.toB58Asset(dta.assetId, self.network);

        dta.spentQty = 0; 

        console.log('getAssetByTxId:'+ JSON.stringify(dta));

        cti.inputs.push(dta);
        each_callback();      
      })
    },
    function(err) {
      callback(null, cti);
    }
  );

};


//Note: These assignments will be wrong unless all inputs can be reliably looked up in the index
OpenAssetService.prototype.assignInputsToTransfers = function(cti) {
  var self = this;
	var transfer_count = 0;
  var spentInputs = [];
  

  //Loop through our transfers and assign inputs to each one
  for(var j=0;j<cti.transfers.length;j++) {

		//Index to the the next input
		for(var i=0;i < cti.inputs.length;i++) {
			var input = cti.inputs[i];

      var transfer = cti.transfers[j]; 
      if (((transfer.qty - transfer.qtyAssigned) >= 0) && (transfer.qtyAssigned < transfer.qty) && input.unspentQty) {

			  if (input.unspentQty >= (transfer.qty - transfer.qtyAssigned)) {   //Input has exact amount or too much
         input.unspentQty -= (transfer.qty - transfer.qtyAssigned);
         input.spentQty += (transfer.qty - transfer.qtyAssigned);
         transfer.qtyAssigned += (transfer.qty - transfer.qtyAssigned);
			    if (transfer.assetId && (transfer.assetId != input.assetId))     //To be valid, same asset must be assigned from multiple inputs - Just a notification.
			    	console.error('Error at height: ' + cti.height + " txIndex:" + cti.txIndex + " txId:" + cti.tx.hash + ' already assigned transfer assetId partially different from input assetId\n    transfer: ' + transfer.assetId + ' != ' + input.assetId);
			  }
			  else                                                               //Input does not have enough
			  {
          transfer.qtyAssigned += input.unspentQty;                        //Assign it all
          input.spentQty += input.unspentQty;
          input.unspentQty = 0;
          if (transfer.assetId && (transfer.assetId != input.assetId))     //To be valid, same asset must be assigned from multiple inputs - Just a notification.
           console.error('Error (2) at height: ' + cti.height + " txIndex:" + cti.txIndex + " txId:" + cti.tx.hash + ' already assigned transfer assetId partially different from input assetId\n    transfer: ' + transfer.assetId + ' != ' + input.assetId);
        }
        
        if (transfer.qty > 0) {   //If transfer qty is 0, leave uncolored as per spec
          transfer.assetId = input.assetId;           
        }
      }
    }
  }   

  //Keep track of all the inputs that were spent.  These will modify the index to mark as spent
  for(var i=0;i < cti.inputs.length;i++) {
    if (cti.inputs[i].unspentQty > 0) {     //All inputs, whether we spent some or not are destroyed.
        self.log.info('Unspent qty: ' + cti.inputs[i].unspentQty + ' being destroyed in txId:' + cti.tx.hash + ' assetQty:' + JSON.stringify(cti.markerOutput.assetQuantities) + ' Marker:' + cti.outputMarkerIndex); 
        console.error('Unspent qty: ' + cti.inputs[i].unspentQty + ' being destroyed in txId:' + cti.tx.hash + ' assetQty:' + JSON.stringify(cti.markerOutput.assetQuantities) + ' Marker:' + cti.outputMarkerIndex); 
        cti.inputs[i].spentQty += cti.inputs[i].unspentQty;  //Spend the remainder into oblivion
        cti.inputs[i].unspentQty = 0;                       //See http://bitcoin.stackexchange.com/questions/38752/what-happens-to-unclaimed-open-assets-colored-coins-asset-quantity-units/38804      
    }
    if (cti.inputs[i].spentQty > 0) {
      let iclone = Object.assign({}, cti.inputs[i]);
      spentInputs.push(iclone);
    }
  }

  cti.spentInputs = spentInputs;


	//Sanity check - all valid outputs should be assigned value from inputs
	for(var k=0;k<cti.transfers.length;k++) {
    if (cti.transfers[k].qty && (cti.transfers[k].qty != cti.transfers[k].qtyAssigned)) {
      console.error("Bad transaction (insufficient inputs) at height: " + cti.height + " txIndex" + cti.txIndex + " txId:" + cti.tx.hash);
      console.log("Bad transaction (insufficient inputs) at height: " + cti.height + " txIndex" + cti.txIndex + " txId:" + cti.tx.hash);
    	console.error('Post - Assignment:' + JSON.stringify(cti));
    		//console.log('tx:' + JSON.stringify(cti.tx));
    		//Get the info from CoinPrism for each input in the list -- Some will be colored
    	cti.inputList.forEach(function(input) {
        //oa.getCoinprismTx(input.txId)    //Temporarily disabled
      });
    		
      self.log.info('cti for analysis: ' + JSON.stringify(cti));
      debugger; 
    }
  }
};

//Pushes new leveldb operations to op to be executed later
//Loop throught the TransactionOutputs.  Index Issuance and Transfers
OpenAssetService.prototype.indexAll = function (cti, callback) {
  this.indexAssetToTx(cti);
  this.indexTxToAsset(cti);
  this.indexAddressToTx(cti);
  this.indexAddressToBalance(cti, function(err, ops) {
    callback(null, ops);
  });
};



//On issuances, index the meta data (by asset id)
OpenAssetService.prototype.indexAssetToMeta = function (cti) {
  //console.log('tx in indexAssetToMeta', cti.tx);
  var self = this;
  var ops = [];

  //Index all issuances in the tx
  for(var i=0;i < cti.issuances.length;i++) {
    var issue = cti.issuances[i];
    if (issue.assetId && cti.outputMarkerIndex > 0) { //Issuance
      var key   = encoding.encodeAssetToMetaKey(issue.assetId);
      var value = encoding.encodeAssetToMetaValue(JSON.stringify(cti.metadata_acquired));

      ops.push({
        type: 'put',
        key: key,
        value: value
      });
    }
  }

  self.openassets_store.batch(ops, function(err) {
    if (err)
      console.error('Problem in indexAssetToMeta');
  });

}

//On issuances, index the assetIdd (by name)
OpenAssetService.prototype.indexNameToAsset = function (cti) {
  //console.log('tx in indexAssetToMeta', cti.tx);
  var self = this;
  var ops = [];

  //Index all issuances in the tx
  for(var i=0;i < cti.issuances.length;i++) {
    var issue = cti.issuances[i];
    //console.log('indexNameToAsset');
    var name_obj;
    if (cti.metadata_acquired) {
      //console.log(JSON.stringify(cti.metadata_acquired));
      //console.log('name_short: ' + cti.metadata_acquired.name_short);
    } else {
      //console.log('No metadata');
    }
    if (issue.assetId && cti.outputMarkerIndex > 0 && cti.metadata_acquired && cti.metadata_acquired.name_short && cti.metadata_acquired.name_short.length > 0) { //Issuance
      var name = cti.metadata_acquired.name_short || cti.metadata_acquired.name || cti.metadata_acquired || undefined;
      if (name) {
        //console.log('indexing in indexNameToAsset: ' + name);
        var key   = encoding.encodeAssetNameToAssetKey(name);
        var value = encoding.encodeAssetNameToAssetValue(issue.assetId, cti.tx.hash, issue.outputIdx);

        ops.push({
          type: 'put',
          key: key,
          value: value
        });
      }
    }
  }

  self.openassets_store.batch(ops, function(err) {
    if (err)
      console.error('Problem in indexNameToAsset');
  });

}



//If we find assetId, add to index
OpenAssetService.prototype.indexAssetToTx = function (cti) {

  //console.log('tx in indexAssetToTx:' + cti.tx);

  //Index all issuances in the tx
  for(var i=0;i < cti.issuances.length;i++) {
    var issue = cti.issuances[i];
    if (issue.assetId && cti.outputMarkerIndex > 0 && (issue.qty > 0)) { //Issuance
      var key   = encoding.encodeAssetToTxKey(issue.assetId, cti.height, cti.txIndex, issue.outputIdx);
      //console.log(JSON.stringify(cti.tx));
      var value = encoding.encodeAssetToTxValue(cti.tx.hash, issue.outputIdx);

      //console.log("op pushed");
      cti.db.ops.push({
        type: cti.db.action,
        key: key,
        value: value
      });
    }
  }

  //Index all transfers in the tx
  for(var i=0;cti.transfers && (i < cti.transfers.length);i++) {
    var transfer = cti.transfers[i];

    if (transfer.qty > 0 && (transfer.assetId)) {
      //console.log('encodeAssetToTx: qty: ' + transfer.qty + ' assetId:' + transfer.assetId );
      var key   = encoding.encodeAssetToTxKey(transfer.assetId, cti.height, cti.txIndex, transfer.outputIdx);;
      var value = encoding.encodeAssetToTxValue(cti.tx.hash, transfer.outputIdx);

      cti.db.ops.push({
        type: cti.db.action,
        key: key,
        value: value
      });
    }
  }
};

OpenAssetService.prototype.indexTxToAsset = function (cti) {

  //console.log('tx in indexTxToAsset:'+ cti.tx);

  //console.log('indexTxToAsset issuances:'+ cti.issuances);

  //console.log('decoded meta data:'+ cti.markerOutput.metadata.toString('utf-8'));


  //Index all issuances in the tx
  for(var i=0;i < cti.issuances.length;i++) {
    var issue = cti.issuances[i];
    //console.log('One issue:' + issue);
    if (issue.assetId && (cti.outputMarkerIndex > 0) && (issue.qty > 0)) { //Issuance

      //console.log(cti.tx.hash)
      //console.log('issue.outputIdx:'+ issue.outputIdx);
      var key   = encoding.encodeTxToAssetKey(cti.tx.hash, issue.outputIdx);
      var value = encoding.encodeTxToAssetValue(issue.assetId, issue.assetAddress, cti.height, cti.txIndex, issue.outputIdx, issue.qty, issue.qty);
	    //console.log("Issuance: " + '  Block#'+cti.height+ ' Tx#'+cti.txIndex + ' outputIdx:'+issue.outputIdx + ' txId:' + cti.tx.hash + ' qty:'+issue.qty);

      //console.log('indexTxToAsset indexing: '+ issue);

      cti.db.ops.push({
        type: cti.db.action,
        key: key,
        value: value
      });
    }
  }

  //Index all transfers in the tx
  for(var i=0;cti.transfers && (i < cti.transfers.length);i++) {
    var transfer = cti.transfers[i];

    //if (!transfer.assetId)
    //  console.log('Bad - non assignment of Asset');
    if (transfer.qty > 0 && (transfer.assetId)) {  
      var key   = encoding.encodeTxToAssetKey(cti.tx.hash, transfer.outputIdx);
      var value = encoding.encodeTxToAssetValue(transfer.assetId, transfer.toAssetAddress, cti.height, cti.txIndex, transfer.outputIdx, transfer.qty, transfer.qty);

	    console.log("Transfer: " + '  Block#'+cti.height+ ' Tx#'+cti.txIndex + ' outputIdx:'+transfer.outputIdx + ' txId:' + cti.tx.hash + ' qty:'+transfer.qty);

      cti.db.ops.push({
        type: cti.db.action,
        key: key,
        value: value
      });
    }
  }

  //If we have spent inputs, then record them - replaces previous index record - used to track spending of input
  for(var i=0;cti.spentInputs && (i<cti.spentInputs.length);i++) {
      var input = cti.spentInputs[i];
      var key   = encoding.encodeTxToAssetKey(input.txId, input.outputIdx);
      var value = encoding.encodeTxToAssetValue(input.assetId, input.assetAddress, input.height, input.txnum, input.outputIdx, input.qty, input.unspentQty);

      console.log("Spent: " + '  Block#'+cti.height+ ' Tx#'+cti.txIndex + ' outputIdx:'+input.outputIdx + ' txId:' + cti.tx.hash + ' qty:'+input.qty) + ' unspentQty' + input.unspentQty;

      cti.db.ops.push({
        type: cti.db.action,
        key: key,
        value: value
      });
  }
};

OpenAssetService.prototype.indexAddressToBalance = function (cti, callback) {
  var self = this;
  var bal_adj = {};
  

  //Calculate the balance adjustment for every issuance
  for(var i=0;i < cti.issuances.length;i++) {
    var issue = cti.issuances[i];
    if (issue.assetId && (cti.outputMarkerIndex > 0) && (issue.qty > 0)) { //Issuance
      assert(issue.assetAddress.length === 20, 'Issuance - bad address');
      assert(issue.assetId.length === 20, 'Issuance - bad assetId');
      var key   = encoding.encodeAddressToBalanceKey(issue.assetAddress, issue.assetId).toString('hex');
      //May have been issued before
      if (bal_adj[key] === undefined)
        bal_adj[key] = issue.qty;
      else 
        bal_adj[key] += issue.qty;

      console.log("Issuance:" + issue.qty);       
    }
  }

  //Calculate the balance adjustment for every transfer (TO only - positive adjustment)
  for(var i=0;cti.transfers && (i < cti.transfers.length);i++) {
    var transfer = cti.transfers[i];

    if (transfer.qty > 0 && (transfer.assetId)) {  
      var key   = encoding.encodeAddressToBalanceKey(transfer.toAssetAddress, transfer.assetId).toString('hex');;
      if (bal_adj[key] === undefined)
        bal_adj[key] = transfer.qty;
      else 
        bal_adj[key] += transfer.qty;

      console.log("Transfer:" + transfer.qty);       
    }
  }

  //Calculate the balance adjustment for every spent input (using the rule that excess not transfered is lost forever)
  for(var i=0;cti.spentInputs && (i<cti.spentInputs.length);i++) {
      var input = cti.spentInputs[i];
      var key   = encoding.encodeAddressToBalanceKey(input.assetAddress, input.assetId).toString('hex');;
      if (bal_adj[key] === undefined)
        bal_adj[key] = input.qty * -1;
      else 
        bal_adj[key] -= input.qty;      
      console.log("Spent inputs:" + input.qty);       
  }

  // console.log('bal_adj');
  // console.log(JSON.stringify(bal_adj));


  async.eachOfSeries(bal_adj, function(qty, key, cb) { 
      if (bal_adj.hasOwnProperty(key)) {
        if (qty != 0) {
          qty = parseInt(qty);
          async.series([                //This should be able to run in parallel, had a bad calculation - trying series.
            function (cb_a) {
              self.adjustAddressBalance(key, qty, cb_a);
            }, 
            function (cb_b) {
              self.adjustAssetBalance(key, qty, cb_b);
            }],
            function (err, results) {
              if (err) {
                cb(err);
              }
              else {
                console.log("Done adjusting balances" + JSON.stringify(results));
                cb();
              }
            } 
          );
        }
      }
  }, callback(null, cti.db.ops));
};


OpenAssetService.prototype.adjustAssetBalance = function (key, adjustment_qty, callback) {
  var self = this;
  var options = {sync: false, keyEncoding: 'binary', valueEncoding : 'binary' };

  assert(key.length===82, 'Key wrong length:' + key.length);
  var keyBuffer = new Buffer(key, 'hex');
  assert(keyBuffer.length===41, 'Keybuffer wrong length:' + keyBuffer.length);
  var balKey = encoding.decodeAddressToBalanceKey(keyBuffer);
  assert(balKey.assetId.length === 40, 'assetId is wrong length - ' + balKey.assetId.length);
  var assetId = self.toB58Asset(balKey.assetId, self.network);
  var address = self.toB58Address(balKey.assetAddress, self.network);

  var assetKeyBuffer = encoding.encodeAssetToBalanceKey(balKey.assetId, balKey.assetAddress);

  //Get the existing balance
  self.openassets_store.get(assetKeyBuffer, options, function(err, rslt) {
      var qty = 0;

      if (err) {
        if (!err.notFound)
          console.log('Error reading leveldb for address/asset');
      } else {
        qty = encoding.decodeAssetToBalanceValue(rslt).balance;
      }

      //Update the balance
      assert(assetKeyBuffer.length===41, 'assetKeyBuffer wrong length:' + assetKeyBuffer.length);

      self.openassets_store.put(assetKeyBuffer, encoding.encodeAssetToBalanceValue(qty + adjustment_qty), options, function (err) {
        callback(null, qty + adjustment_qty);
      });
  });
}



OpenAssetService.prototype.adjustAddressBalance = function (key, adjustment_qty, callback) {
  var self = this;
  var options = {sync: false, keyEncoding: 'binary', valueEncoding : 'binary' };

  assert(key.length===82, 'Key wrong length:' + key.length);
  var keyBuffer = new Buffer(key, 'hex');
  assert(keyBuffer.length===41, 'Keybuffer wrong length:' + keyBuffer.length);
  var balKey = encoding.decodeAddressToBalanceKey(keyBuffer);
  assert(balKey.assetId.length === 40, 'assetId is wrong length - ' + balKey.assetId.length);
  var assetId = self.toB58Asset(balKey.assetId, self.network);
  var address = self.toB58Address(balKey.assetAddress, self.network);


  console.log("Address/Asset Balance adjustment for Asset: " + assetId + ' in Address:' + address + ' qty:' + adjustment_qty);
  //Get the existing balance
  self.openassets_store.get(keyBuffer, options, function(err, rslt) {
      var qty = 0;

      if (err) {
        if (!err.notFound)
          console.log('Error reading leveldb for address/asset');
      } else {
        qty = encoding.decodeAddressToBalanceValue(rslt).balance;
        console.log("Balance for " + address + " found: " + qty);
      }

      //Update the balance
      if (qty + adjustment_qty < 0)
        console.error('Negative balance on Address:' + address + ' for Asset:' + assetId + ' qty:' + (qty + adjustment_qty));

      console.log("Updating Address " + address + " balance to: " + (qty + adjustment_qty));

      assert(keyBuffer.length===41, 'keyBuffer wrong length:' + keyBuffer.length);

      self.openassets_store.put(keyBuffer, encoding.encodeAddressToBalanceValue(qty + adjustment_qty), options, function (err) {
        console.log("Updated Address " + address + " balance to: " + (qty + adjustment_qty));
        callback(null, qty + adjustment_qty);
      });
  });
}

OpenAssetService.prototype.indexAddressToTx = function (cti) {

  //Index all issuances in the tx
  for (var issue in cti.issuances) {
    if (issue.assetId && (cti.outputMarkerIndex > 0)) { //Issuance
      var key   = encoding.encodeAddressToTxKey(issue.assetAddressBuffer, issue.assetId, cti.height, cti.txIndex, issue.outputIdx);
      var value = encoding.encodeAddressToTxValue(cti.tx.hash, issue.outputIdx);

      cti.db.ops.push({
        type: cti.db.action,
        key: key,
        value: value
      });
    }
  }


  //Index all transfers in the tx
  for(var i=0;cti.transfers && (i < cti.transfers.length);i++) {
    var transfer = cti.transfers[i];

    if (transfer.qty > 0 && (transfer.assetId)) {
      var key   = encoding.encodeAddressToTxKey(transfer.toAssetAddress, transfer.assetId, cti.height, cti.txIndex, transfer.outputIdx);
      var value = encoding.encodeAddressToTxValue(cti.tx.hash, transfer.outputIdx);

      cti.db.ops.push({
        type: cti.db.action,
        key: key,
        value: value
      });
    }
  }
};

OpenAssetService.prototype.indexAssetNameToAsset = function (cti) {
  
  db.ops.push({
    type: cti.db.action,
    key: key,
    value: value
  });
};


//Return array of all the issuances for a transaction.
OpenAssetService.prototype.getAssetIssuances = function(cti, cb) {
  var self = this;

  var issuances = [];
  var issuance = {};

  var issuance_count = 0;
  for(var i=0;i<cti.outputMarkerIndex;i++) {
    if (cti.tx.outputs[i]) {
      issuance.idx = issuance_count;
      issuance.txId = cti.tx.hash;
      issuance.outputIdx = i;
      issuance.qty = cti.markerOutput.assetQuantities[issuance_count];
      issuance.metadata = cti.markerOutput.metadata;
      issuance.assetAddress = this.getAssetAddress(cti.tx.outputs[i].script)   //cti.assetAddresses[issuance_count];
      if (issuance.assetAddress) {
        issuance.assetAddressBase58 = this.toB58Address(issuance.assetAddress, this.network);
        let iclone = Object.assign({}, issuance);
        issuances.push(iclone);
        issuance_count++;
      }
    }
  }

  cti.issuances = issuances;

  async.eachSeries(issuances, function(issuance, callback) {
  		self.getAssetId(cti.tx.inputs[0].prevTxId, cti.tx.inputs[0].outputIndex, function(err, assetId) {
  			if (err)
  				callback('Error in getAssetId');
  			else {
	  			issuance.assetId = assetId;
	  			issuance.assetIdBase58 = self.toB58Asset(assetId, self.network);
	  			//console.log('Issued assetId: ' + issuance.assetIdBase58);
	  			callback(null, assetId);
  			}
  		});
  	}, function (err) {
  		//console.log('issuances: ' + JSON.stringify(issuances));
  		return cb(null, cti);
  	});
};

OpenAssetService.prototype.getNetwork = function() {
  var self = this;
  console.log(JSON.stringify(self.network));
  return(self.network);
}

/**
 * Will get a the assetId for a Open Asset issuance
 * @param {String | Buffer} txId - The transaction hash from the first input of the issuing transaction (can also take txId as buffer)
 * @param {Numeric} outputIndex - The outputIndex from the first input of the issuing transaction
 * @param {Function} cb - Returns the assetId as a buffer (20 byte)
 */
OpenAssetService.prototype.getAssetId = function(txId, outputIndex, cb) {
	var self = this;
	if (Buffer.isBuffer(txId))
		txId = txId.toString('hex');

  //From spec: "RIPEMD-160 hash of the SHA-256 hash of the output script referenced by the first input of the transaction"
  self.node.getTransaction(txId, function(err, tx) {
		if (err) {
			cb('Could not getTransaction.');
		} else {
			var assetId = self.ce.hashScript(tx.outputs[outputIndex].script.toBuffer());
			cb(null, assetId);
		}
	});
} 


OpenAssetService.prototype.getAssetTransfers = function(cti, cb) {

	if (cti.markerOutput.assetQuantities.length <= cti.outputMarkerIndex) 
		return(cb(null, cti));  //No transfers if the number of assetQuantities is less than the outputMarkerIndex

  //console.log('Yes-Transfers');

  var self = this;

  var transfers = [];
  var transfer = {};

  var count=0;
  for(var i = cti.outputMarkerIndex+1; i < cti.tx.outputs.length; i++) {
    var output = cti.tx.outputs[i];

    var toAssetAddressBuffer = this.getAssetAddress(output.script); 
    if (toAssetAddressBuffer) {
      transfer.idx = count;
      transfer.outputIdx = i;  //Past the markerOutputIndex
      transfer.toAssetAddress = toAssetAddressBuffer;                    //Where the asset is sent
      transfer.toAssetAddressBase58 = this.toB58Address(toAssetAddressBuffer, this.network);
      if (cti.markerOutput.assetQuantities.length > (cti.outputMarkerIndex + count))
        transfer.qty = cti.markerOutput.assetQuantities[cti.outputMarkerIndex + count];
      else
        transfer.qty = 0;
      transfer.qtyAssigned = 0;
      let tclone = Object.assign({}, transfer);
      transfers.push(tclone);
      count++;
    }
  }

  cti.transfers = transfers;

	//Get a list of all the inputs
	cti.inputList = getInputsFromTx(cti.tx);
	//console.log('inputList:'+ cti.inputList);

	self.getAssetIdAndQtyFromIndex(cti, function (err, cti) {
	  self.assignInputsToTransfers(cti);
	  cb(null, cti);
	})

};

//Each tx output after the marker can have a different id
OpenAssetService.prototype.getTransferAssetIds = function(coloredTxInfo) {
  var self = this;

  var assetIds = [];

  var issuanceAssetId;
  if (coloredTxInfo.markerOutput.metadata)
    issuanceAssetId = this.ce.hashScript(coloredTxInfo.tx.inputs[0].script.toBuffer());

  return(issuanceAssetId);
};

OpenAssetService.getAugmentedColoredInfo = function (coloredInfo) {

};

//Temp placeholder
// OpenAssetService.isSpent = function(txId) {
//   return(false);
// };


OpenAssetService.prototype.getAssetAddresses = function(tx, txIndex, outputMarkerIndex) {
  //console.log('tx in getAssetAddresses:'+ JSON.stringify(tx));

  var addresses=[];
  for(var o=0;o<tx.outputs.length;o++) {
    //console.log(tx.outputs[o].script);
    var addressBuffer = this.getAssetAddress(tx.outputs[o].script);
    if (addressBuffer) {
      //console.log('getAssetAddress:'+ JSON.stringify(addressBuffer));
      addresses.push(addressBuffer);
      //console.log("Open Asset Address: " + this.toB58Address(addressBuffer, this.network));
    } else {
      console.log('getAssetAddress not found');
    }
  }

  return(addresses);
};


OpenAssetService.prototype.getAssetAddress = function(outputscript) {
  var addressInfo = encoding.extractAddressInfoFromScript(outputscript, this.network);
  if (addressInfo)
    return(addressInfo.hashBuffer);
};


/**
 * Convert a 20 byte address buffer into an OpenAsset address
 * @param  String addressBuffer  - A 20 bytes address buffer
 * @param  Network network  - A 20 bytes address buffer
 * @return String             The resulting OpenAsset address
**/
OpenAssetService.prototype.toB58Address = function (addressBuffer, network) {
  $.checkArgument((Buffer.isBuffer(addressBuffer) && addressBuffer.length===20) || addressBuffer.length===40, 'Argument is expected to be a 20 byte Buffer or hex string'); 
  $.checkArgument(network.name==='testnet' || network.name==='livenet', 'Network is expected to be testnet or livenet');

 
  if (!Buffer.isBuffer(addressBuffer))  //If not a buffer, then it is hex, so convert to a buffer.
  	addressBuffer = new Buffer(addressBuffer, 'hex');

  var network_first_byte = network.addressVersion || 0x00;

  var oaIdBuffer = new Buffer(1);
  oaIdBuffer.writeUInt8(19);
  var fbBuffer = new Buffer(1);
  fbBuffer.writeUInt8(network_first_byte);

  var addrBuff = new Buffer(Buffer.concat([
                oaIdBuffer,
                fbBuffer,
                addressBuffer]));

  var addrCheck = bitcore.crypto.Hash.sha256sha256(addrBuff);
  var addrBuffWithCheck = new Buffer(Buffer.concat([addrBuff, addrCheck.slice(0,4)]));

  return bitcore.encoding.Base58.encode(addrBuffWithCheck);
}



/**
 * Convert an asset Buffer to an assetAddress
 * @param  Buffer or Hex String (20 byte)
 * @return The resulting assetIdFriendly
**/
OpenAssetService.prototype.toB58Asset = function (assetIdBuffer, network) {
	$.checkArgument((Buffer.isBuffer(assetIdBuffer) && assetIdBuffer.length===20) || assetIdBuffer.length===40, 'Argument is expected to be a 20 byte Buffer or hex string');
  $.checkArgument(network.name==='testnet' || network.name==='livenet', 'Network is expected to be testnet or livenet');

  if (!Buffer.isBuffer(assetIdBuffer))  //If not a buffer, then it is hex, so convert to a buffer.
  	assetIdBuffer = new Buffer(assetIdBuffer, 'hex');

	const VERSION_BYTE_TESTNET = 115;
	const VERSION_BYTE_LIVENET = 23;

  	var oaIdBuffer = new Buffer(1);
  	if (this.network.name=='testnet') {
  		oaIdBuffer.writeUInt8(VERSION_BYTE_TESTNET);
  	} 
  	else {
  		oaIdBuffer.writeUInt8(VERSION_BYTE_LIVENET);
  	} 

  var assetBuff = new Buffer(Buffer.concat([
                oaIdBuffer,
                assetIdBuffer]));

  var assetChecksum = bitcore.crypto.Hash.sha256sha256(assetBuff);
  var assetBuffWithChecksum = new Buffer(Buffer.concat([assetBuff, assetChecksum.slice(0,4)]));

  return bitcore.encoding.Base58.encode(assetBuffWithChecksum);
}



/**
 * This function will attempt to rewind the chain to the common ancestor
 * between the current chain and a forked block.
 * @param {Block} block - The new tip that forks the current chain.
 * @param {Function} done - A callback function that is called when complete.
 */
OpenAssetService.prototype.disconnectTip = function(done) {
  var self = this;

  var tip = self.tip;

  var prevHash = BufferUtil.reverse(tip.header.prevHash).toString('hex');

  self.node.getBlock(prevHash, function(err, previTip) {
    if (err) {
      done(err);
    }

    // Undo the related indexes for this block
    self.disconnectBlock(tip, function(err) {
      if (err) {
        return done(err);
      }

      // Set the new tip
      previousTip.__height = self.tip.__height - 1;
      self.tip = previousTip;
      self.emit('removeblock', tip);
      done();
    });
  });
};

/**
 * This function will synchronize additional indexes for the chain based on
 * the current active chain in the bitcoin daemon. In the event that there is
 * a reorganization in the daemon, the chain will rewind to the last common
 * ancestor and then resume syncing.
 */
OpenAssetService.prototype.sync = function() {
  var self = this;

  if (self.bitcoindSyncing)
  	console.log('Bitcoind syncing....');

  if (self.bitcoindSyncing || self.node.stopping || !self.tip) {
    return;
  }

  self.bitcoindSyncing = true;

  var height;

  async.whilst(function() {
    if (self.node.stopping) {
      return false;
    }
    height = self.tip.__height;
    //console.log('self.tip.__height:'+ self.tip.__height);
    //console.log('self.node.services.bitcoind.height:'+self.node.services.bitcoind.height);
    return height < self.node.services.bitcoind.height;
  }, function(done) {
    self.node.getRawBlock(height + 1, function(err, blockBuffer) {
      if (err) {
        return done(err);
      }

      var block = Block.fromBuffer(blockBuffer);

      var prevHash = BufferUtil.reverse(block.header.prevHash).toString('hex');

      if (prevHash === self.tip.hash) {

        // This block appends to the current chain tip and we can
        // immediately add it to the chain and create indexes.

        // Populate height
        block.__height = self.tip.__height + 1;

        // Create indexes
        self.connectBlock(block, function(err) {
          if (err) {
            return done(err);
          }
          self.tip = block;
          self.log.debug('Chain added block to main chain');
          self.emit('addblock', block);
          done();
        });
      } else {
        // This block doesn't progress the current tip, so we'll attempt
        // to rewind the chain to the common ancestor of the block and
        // then we can resume syncing.
        self.log.warn('Reorg detected! Current tip: ' + self.tip.hash);
        self.disconnectTip(function(err) {
          if(err) {
            return done(err);
          }
          self.log.warn('Disconnected current tip. New tip is ' + self.tip.hash);
          done();
        });
      }
    });
  }, function(err) {
    if (err) {
      Error.captureStackTrace(err);
      return self.node.emit('error', err);
    }

    if(self.node.stopping) {
      self.bitcoindSyncing = false;
      return;
    }

    self.node.isSynced(function(err, synced) {
      if (err) {
        Error.captureStackTrace(err);
        return self.node.emit('error', err);
      }

      if (synced) {
        self.bitcoindSyncing = false;
        self.node.emit('synced');
      } else {
        self.bitcoindSyncing = false;
      }
    });

  });

};

OpenAssetService.prototype.getRoutePrefix = function() {
  return 'oa';
};

OpenAssetService.prototype.setupRoutes = function(app) {
  //app.get('/balance/:assetAddress', this.getAddressBalance.bind(this));
  app.get('/txs/:assetId', this.txByAsset.bind(this));
  //app.get('/plus/:assetId/txs', this.txsPlusAsset.bind(this));
  app.get('/tx/:txId', this.getOpenAssetTransaction.bind(this));
  app.get('/txo/:txId/:outputIdx', this.txoByTxIdAndOutputIdx.bind(this));
  app.get('/asset/meta/:assetId', this.getAssetMeta.bind(this));
  app.get('/asset/:name', this.getAssetByName.bind(this));
  app.get('/assets', this.getNamedAssetsPage.bind(this));
  app.get('/txs/address/:addressId', this.txByAddressREST.bind(this));
  app.get('/txs/:assetId/:addressId', this.txByAddressAsset.bind(this));
  app.get('/txos/:assetId/:addressId', this.txByAddressAssetTxosREST.bind(this));
  app.get('/utxos/:assetId/:addressId', this.txByAddressAssetUtxos.bind(this));
  app.get('/balance/:assetId/:addressId', this.balanceByAddressAsset.bind(this));
  app.get('/qbalance/:assetId/:addressId', this.qbalanceByAddressAsset.bind(this));
  app.get('/balance/:assetId', this.balanceByAsset.bind(this));
  //app.get('/balances/:address', this.balancesByAddress.bind(this));
  app.get('/balances', this.allBalances.bind(this));
  //app.get('/qbalance/:assetId', this.qbalanceByAsset.bind(this));  //Replaced by owners/:assetId
  app.get('/owners/:assetId', this.ownersByAssetREST.bind(this));
  app.get('/addresses/:assetId', this.addressesByAsset.bind(this));
  app.get('/txos', this.allTxos.bind(this));
  app.get('/height', this.getHeightREST.bind(this));
};




OpenAssetService.prototype.balanceByAssetAPI = function(assetId, callback) {
  var self = this;

  var assetIdBuffer = this.assetBufferFromAssetId(assetId);

  this.getMetaByAssetId(assetIdBuffer, function(err, rslt) {
    if (err) 
      return callback(err);
    callback(null, rslt)
  });   
}

OpenAssetService.prototype.getAssetTransactionAPI = function(assetId, callback) {
  var self = this;

  var assetIdBuffer = this.assetBufferFromAssetId(assetId);

  this.getOutputsByTxId(txIdBuffer, function(err, rslt) {
    if (err) 
      return callback(err);

    callback(null, rslt);
  });   
}


OpenAssetService.prototype.oaInfoAPI = function (callback) {
  var self = this;
  var info = {
    version : self.version,
    height : self.tip.__height,
    network : self.network.name,
    data: this.dataPath
  }
  callback(null, info);
}

OpenAssetService.prototype.ownersByAssetAPI = function(assetId, callback) {
  this.ownersByAsset(assetId, function(err, rslt) {
    if (err) 
      return callback(err);
    callback(null, rslt)
  });   
}


OpenAssetService.prototype.getAssetDefinitionAPI = function(assetId, callback) {
  var assetIdBuffer = this.assetBufferFromAssetId(assetId);

  this.getMetaByAssetId(assetIdBuffer, callback);
}


//Much faster, uses the new index
OpenAssetService.prototype.ownersByAssetAPI2 = function(assetId, callback) {
  var assetIdBuffer = this.assetBufferFromAssetId(assetId);

  this.getOwnersByAsset({assetIdBuffer : assetIdBuffer}, function(err, rslt) {
    if (err) 
      return callback(err);
    callback(null, rslt)
  });   
}

OpenAssetService.prototype.getAssetMeta = function(req, res, next) {
  var self = this;
  enableCors(res);

  var assetIdBuffer = this.assetBufferFromAssetId(req.params.assetId);

  this.log.info('request for metadata for:', assetId);  

  this.getMetaByAssetId(assetIdBuffer, function(err, rslt) {
    if (err) 
      return res.send(500, err);
    res.status(200).send(JSON.stringify(rslt));
  });   
}

OpenAssetService.prototype.allBalances = function(req, res, next) {
  var self = this;
  enableCors(res);

  this.getAllBalances(function(err, rslt) {
    if (err) 
      return res.send(500, err);

    res.status(200).send(JSON.stringify(rslt)); 
  });   
}


OpenAssetService.prototype.ownersByAssetREST = function(req, res, next) {
  var self = this;
  enableCors(res);

  this.ownersByAssetAPI2(req.params.assetId, function(err, rslt) {
    if (err) 
      return res.send(500, err);
      res.status(200).send(JSON.stringify(rslt));      
  });   
}


OpenAssetService.prototype.getAssetByName = function(req, res, next) {
  var self = this;
  enableCors(res);

  this.log.info('request for assetId for name::', req.params.name);  

  this.getAssetIdByName(req, function(err, assetInfo) {
    if (err) 
      return res.send(500, err);
    if (assetInfo && assetInfo.assetId) {
      var assetId = self.toB58Asset(assetInfo.assetId, self.network);
      res.status(200).send(JSON.stringify({assetId: assetId}));
    } else {
      res.status(200).send(JSON.stringify({}));      
    }
  });   
}

OpenAssetService.prototype.getNamedAssetsPage = function(req, res, next) {
  var self = this;
  enableCors(res);

  this.log.info('request for assets');  

  this.getNamedAssets(req, function(err, assets) {
    if (err) 
      return res.send(500, err);
    res.status(200).send(JSON.stringify(assets));      
  });   
}


OpenAssetService.prototype.getOpenAssetTransaction = function(req, res, next) {
  var self = this;
  enableCors(res);

  var txId = req.params.txId;
  try {
    var txIdBuffer = new Buffer(txId, 'hex');
  } catch(e) {
    res.send(500, "Make sure you send a valid txId");
  }

  this.log.info('request for info on txId:', txId);  

  this.getOutputsByTxId(txIdBuffer, function(err, rslt) {
    if (err) 
      return res.send(500, err);
    res.status(200).send(JSON.stringify(rslt));
  });   
}

OpenAssetService.prototype.txoByTxIdAndOutputIdx = function(req, res, next) {
  var self = this;
  enableCors(res);

  var txId = req.params.txId;
  var outputIdx = parseInt(req.params.outputIdx);

  this.getAssetByTxId(txId, outputIdx, function(err, rslt) {
    if (err) 
      return res.send(500, err);
    res.status(200).send(JSON.stringify(rslt));    
  });
}


OpenAssetService.prototype.txByAsset = function(req, res, next) {
  var self = this;
  enableCors(res);

  var assetIdBuffer = this.assetBufferFromAssetId(req.params.assetId);

  this.getTxByAsset({assetIdBuffer:assetIdBuffer, count: req.query.count, startBlock:req.query.startBlock, endBlock:req.query.endBlock}, function(err, rslt) {
    if (err) 
      return res.send(500, err);
    res.status(200).send(JSON.stringify(rslt));
  });   
}



OpenAssetService.prototype.txByAddressREST = function(req, res, next) {
  enableCors(res);
  var p = {};

  try {
    p.addressBuffer = this.ce.addressBufferFromAddress(req.params.addressId);
  } catch(e) {
    res.send(500, "Make sure you send a valid addressId");
  }

  p.count = req.params.count || undefined;

  this.getTxByAddressQuery(p, function(err, rslt) {
    if (err) 
      return res.send(500, err);
    res.status(200).send(JSON.stringify(rslt));
  });   
}


OpenAssetService.prototype.txByAddressAsset = function(req, res, next) {
  var self = this;
  enableCors(res);

  req.assetIdBuffer = this.assetBufferFromAssetId(req.params.assetId);
  try {
    req.addressBuffer = self.ce.addressBufferFromAddress(req.params.addressId);
  } catch(e) {
    res.send(500, "Make sure you send a valid addressId and a valid assetId");
  }

  this.getTxByAddressAsset(req, function(err, rslt) {
    if (err) 
      return res.send(500, err);
    res.status(200).send(JSON.stringify(rslt));
  });   
}


//
// Show all tx outputs (spent and unspent)
//
OpenAssetService.prototype.allTxos = function(req, res, next) {
  var self = this;
  enableCors(res);

  this.getAllTxos(req, function(err, rslt) {
    if (err) 
      return res.send(500, err);
    res.status(200).send(JSON.stringify(rslt));
  });   
}


OpenAssetService.prototype.txByAddressAssetTxosREST = function(req, res, next) {
  enableCors(res);
  try {
    req.addressBuffer = self.ce.addressBufferFromAddress(req.params.addressId);
    req.assetIdBuffer = self.ce.assetBufferFromAssetAddress(req.params.assetId);
  } catch(e) {
    res.send(500, "Make sure you send a valid addressId and a valid assetId");
  }

  this.txByAddressAssetTxos(req, function (err, rslt) {
    if (err) 
      return res.send(500, err);

    res.status(200).send(rslt);        
  });
}


OpenAssetService.prototype.txByAddressAssetTxos = function(params, callback) {
  var self = this;

  this.getTxByAddressAsset(params, function(err, arrOfTxAndOutputIdx) {
    if (err) 
      return callback(err);

    self.getTxos(arrOfTxAndOutputIdx, false, function (err, txos) {
      callback(err, txos);
    });

  });   
}



OpenAssetService.prototype.balanceByAddressAsset = function(req, res, next) {
  var self = this;
  var balance = 0;
  enableCors(res);

  try {
    req.addressBuffer = self.ce.addressBufferFromAddress(req.params.addressId);
    req.assetIdBuffer = self.ce.assetBufferFromAssetAddress(req.params.assetId);
  } catch(e) {
    res.send(500, "Make sure you send a valid addressId and a valid assetId");
  }

  this.getTxByAddressAsset(req, function(err, arrOfTxAndOutputIdx) {
    if (err) 
      return res.send(500, err);

    self.getTxos(arrOfTxAndOutputIdx, true, function (err, txos) {

      if (err)
        return res.send(500, err);
      txos.forEach(function(txo, i, txarr) {
        balance += txo.unspentQty;
      });

      res.status(200).send({balance : balance || 0});
    });

  }); 
}


OpenAssetService.prototype.getQuickBalanceByAddressAsset = function (params, callback) {
  var self = this;
  $.checkArgument(Buffer.isBuffer(params.assetIdBuffer) && params.assetIdBuffer.length===20, 'Requires an assetIdBuffer');
  $.checkArgument(Buffer.isBuffer(params.addressBuffer) && params.addressBuffer.length===20, 'Requires an addressBuffer');

  var key = encoding.encodeAddressToBalanceKey(params.addressBuffer, params.assetIdBuffer);

  this.openassets_store.get(key, {keyEncoding: 'binary', valueEncoding : 'binary' }, function(err, rslt) {
    if (err) {
      if(err instanceof levelup.errors.NotFoundError) {
        return(callback(null, {balance : 0}));
      }
      else
      {
        return callback(err);
      }
    }

    callback(null, { balance : encoding.decodeAddressToBalanceValue(rslt).balance });
  });
}

OpenAssetService.prototype.qbalanceByAddressAsset = function(req, res, next) {
  var self = this;
  enableCors(res);

  try {
    var addressBuffer = self.ce.addressBufferFromAddress(req.params.addressId);
    var assetIdBuffer = self.ce.assetBufferFromAssetAddress(req.params.assetId);
  } catch(e) {
    res.send(500, "Make sure you send a valid addressId and a valid assetId");
  }

  self.getQuickBalanceByAddressAsset({assetIdBuffer: assetIdBuffer, addressBuffer : addressBuffer}, function(err, rslt) {
    if (err) 
      return res.send(500, err);
    
    res.status(200).send(rslt);    
  });
}


OpenAssetService.prototype.balanceByAsset = function(req, res, next) {
  var self = this;
  var balance = 0;
  enableCors(res);

  var assetIdBuffer = this.assetBufferFromAssetId(req.params.assetId);

  this.getTxByAsset({assetIdBuffer:assetIdBuffer}, function(err, arrOfTxAndOutputIdx) {
    if (err) 
      return res.send(500, err);

    console.log('Found ' + arrOfTxAndOutputIdx.length + ' transactions.');
    self.getTxos(arrOfTxAndOutputIdx, true, function (err, txos) {
      var hodlers = {};
      var owners = [];

      console.log('Got all txos: ' + txos.length);
      if (err)
        return res.send(500, err);
      txos.forEach(function(txo, i, txarr) {
        console.log(txo.assetAddress + ' has ' + txo.unspentQty);
        if (hodlers[txo.assetAddress]) {
            hodlers[txo.assetAddress] += txo.unspentQty;
          }
        else {
          hodlers[txo.assetAddress] = txo.unspentQty;
        }
      });

      for (var address in hodlers) {
        if (!hodlers.hasOwnProperty(address)) continue;

        owners.push({address: address, qty : hodlers[address] })
      }

      var result = {
        assetId : req.params.assetId,
        owners: owners
      }

      //console.log('Hodlers: ' + JSON.stringify(hodlers));

      res.status(200).send(JSON.stringify(result));
    });
  }); 
}

OpenAssetService.prototype.ownersByAsset = function(assetId, callback) {
  var self = this;

  var assetIdBuffer = this.assetBufferFromAssetId(req.params.assetId);

  this.getTxByAsset({assetIdBuffer : assetIdBuffer}, function(err, arrOfTxAndOutputIdx) {
    if (err) 
      return callback(err);


    self.getTxos(arrOfTxAndOutputIdx, true, function (err, txos) {
      var hodlers = {};
      var owners = [];

      if (err)
        return callback(err);


      txos.forEach(function(txo, i, txarr) {
        if (hodlers[txo.assetAddress]) {
            hodlers[txo.assetAddress] += txo.unspentQty;
          }
        else {
          hodlers[txo.assetAddress] = txo.unspentQty;
        }
      });

      for (var address in hodlers) {
        if (!hodlers.hasOwnProperty(address)) continue;

        owners.push({address: address, qty : hodlers[address] })
      }

      var result = {
        assetId : assetId,
        owners: owners
      }

      callback(null, result);
    });
  }); 
}


//Return the same information as insight-api/addr/:addr, but for a single asset
OpenAssetService.prototype.getAddrInfo = function(params, callback) {
  var self = this;
  $.checkArgument(Buffer.isBuffer(params.assetIdBuffer) && params.assetIdBuffer.length===20, 'Requires an assetIdBuffer');
  $.checkArgument(Buffer.isBuffer(params.addressBuffer) && params.addressBuffer.length===20, 'Requires an addressBuffer');

  params.divisor = params.divisor || 1;
  //var transactions = [];

  var addr = {
    addrStr : self.toB58Address(params.addressBuffer, self.node.network),
    balance : 0,
    balanceSat : 0,
    totalReceived : 0,
    totalReceivedSat : 0,
    totalSent : 0,
    totalSentSat : 0,
    unconfirmedBalance: 0,          //TODO: Handle unconfirmed Txs
    unconfirmedBalanceSat: 0,       //TODO: Handle unconfirmed Txs
    unconfirmedTxApperances : 0,   //TODO: Handle unconfirmed Txs
  };

  console.log('Calling txByAddressAsseTxos');
  self.txByAddressAssetTxos(params, function (err, rslt) {
    addr.txApperances = rslt.length;     //Intentionally spelled wrong to match insight-ui
    addr.transactions = rslt.map(function(x) {return x.txId;});
    //addr.extra = rslt;
    addr.totalReceivedSat = rslt.reduce(function(b, x) {return b+x.qty;}, 0);
    addr.totalSentSat = rslt.reduce(function(b, x) {return b+(x.qty-x.unspentQty);}, 0);
    addr.balanceSat = addr.totalReceivedSat - addr.totalSentSat;

    addr.balance = addr.balanceSat / params.divisor;
    addr.totalReceived = addr.totalReceivedSat / params.divisor;
    addr.totalSent = addr.totalSentSat / params.divisor;

    self.getQuickBalanceByAddressAsset({assetIdBuffer : params.assetIdBuffer, addressBuffer : params.addressBuffer}, function (err, rslt) {
      addr.balance = rslt.balance;
      callback(null, addr);
    }); 
  });
}



OpenAssetService.prototype.addressesByAsset = function(req, res, next) {
  var self = this;
  var balance = 0;
  enableCors(res);

  var assetIdBuffer = this.assetBufferFromAssetId(req.params.assetId);

  this.getTxByAsset({assetIdBuffer:assetIdBuffer}, function(err, arrOfTxAndOutputIdx) {
    if (err) 
      return res.send(500, err);

    self.getTxos(arrOfTxAndOutputIdx, true, function (err, txos) {

      if (err)
        return res.send(500, err);
      txos.forEach(function(txo, i, txarr) {
        //TODO: Sum up balance PER Address
        if (txo.unspentQty > 0) {

          balance += txo.unspentQty;
        }
      });

      res.status(200).send(JSON.stringify({balance : balance}));
    });

  }); 
}






OpenAssetService.prototype.txByAddressAssetUtxos = function(req, res, next) {
  var self = this;
  enableCors(res);

  req.assetIdBuffer = this.assetBufferFromAssetId(req.params.assetId);

  try {
    req.addressBuffer = self.ce.addressBufferFromAddress(req.params.addressId);
  } catch(e) {
    res.send(500, "Make sure you send a valid addressId and a valid assetId");
  }

  this.getTxByAddressAsset(req, function(err, arrOfTxAndOutputIdx) {
    if (err) 
      return res.send(500, err);

    self.getTxos(arrOfTxAndOutputIdx, true, function (err, txos) {
      if (err)
        return res.send(500, err);

      res.status(200).send(JSON.stringify(txos));
    });

  });   
}


OpenAssetService.prototype.getHeightREST = function(req, res, next) {
  var self = this;
  enableCors(res);

  res.status(200).send({height: self.tip.__height});
}

// OpenAssetService.prototype.lookupHash = function(req, res, next) {
//   /*
//     This method is used to determine whether a file hash has
//     already been included in the blockchain. We are querying data
//     from level db that we previously stored into level db via the blockHanlder.
//   */
//   var self = this;
//   enableCors(res);

//   var hash = req.params.hash; // the hash of the uploaded file
//   this.log.info('request for hash:', hash);
//   var node = this.node;

//   // Search level db for instances of this file hash
//   // and put them in objArr
//   var stream = self.openassets_store.createReadStream({
//     gte: [OpenAssetService.PREFIX, hash].join('-'),
//     lt: [OpenAssetService.PREFIX, hash].join('-') + '~'
//   });

//   var objArr = [];

//   stream.on('data', function(data) {
//     // Parse data as matches are found and push it
//     // to the objArr
//     data.key = data.key.split('-');
//     var obj = {
//       hash: data.value,
//       height: data.key[2],
//       txid: data.key[3],
//       outputIndex: data.key[4]
//     };
//     objArr.push(obj);
//   });

//   var error;

//   stream.on('error', function(streamError) {
//     // Handle any errors during the search
//     if (streamError) {
//       error = streamError;
//     }
//   });

//   stream.on('close', function() {
//     if (error) {
//       return res.send(500, error.message);
//     } else if(!objArr.length) {
//       return res.sendStatus(404);
//     }

//     // For each transaction that included our file hash, get additional
//     // info from the blockchain about the transaction (such as the timestamp and source address).
//     async.each(objArr, function(obj, eachCallback) {
//       var txid = obj.txid;
//       var includeMempool = true;

//       node.log.info('getting details for txid:', txid);
//       node.getDetailedTransaction(txid, function(err, transaction) {
//         if (err){
//           return eachCallback(err);
//         }
//         var address = transaction.inputs[0].address;

//         obj.sourceAddress = address;
//         obj.timestamp = transaction.blockTimestamp;
//         return eachCallback();
//       });
//     }, function doneGrabbingTransactionData(err) {
//       if (err){
//         return res.send(500, err);
//       }

//       // Send back matches to the client
//       res.send(objArr);
//     });

//   });
// };



OpenAssetService.prototype.start = function(callback) {

  var self = this;
  if (!fs.existsSync(this.dataPath)) {
    mkdirp.sync(this.dataPath);
  }

  self.openassets_store = levelup(this.dataPath);

  //self.showIndexes();

  //console.log('After showIndexes');

  self.once('ready', function() {
    self.log.info('Bitcoin Database Ready');

    self.node.services.bitcoind.on('tip', function() {
      if(!self.node.stopping) {
        self.sync();
      }
    });
  });

  self.loadTip(function(err) {
    if (err) {
      return callback(err);
    }

    self.sync();
    self.emit('ready');
    callback();
  });

};

OpenAssetService.prototype.stop = function(callback) {
  var self = this;

  // Wait until syncing stops and all db operations are completed before closing leveldb
  async.whilst(function() {
    return self.bitcoindSyncing;
  }, function(next) {
    setTimeout(next, 10);
  }, function() {
    self.openassets_store.close();
  });
  callback();
};

OpenAssetService.prototype.getAPIMethods = function() {
  return [
    ['getOpenAssetBalance', this, this.getOpenAssetBalance, 2],
    ['getOpenAssetIssueHistory', this, this.getOpenAssetIssueHistory, 2],
    ['balanceByAsset', this, this.balanceByAssetAPI, 1],
    ['ownersByAssetCalculated', this, this.ownersByAssetAPI, 1],
    ['ownersByAsset', this, this.ownersByAssetAPI2, 1],
    ['getAssetDefinition', this, this.getAssetDefinitionAPI, 1],
    ['getAssetTransaction', this, this.getAssetTransactionAPI, 1],
    ['oa_txs', this, this.oa_txs, 3],
    ['oa_tx', this, this.oa_tx, 3],
    ['oa_blocks', this, this.oa_blocks, 3],
    ['oa_block', this, this.oa_block, 3],
    ['oa_addr', this, this.oa_addr, 3],
    ['proxyAPI', this, this.proxyAPI, 3],
    ['oaInfo', this, this.oaInfoAPI, 0]
  ];
};



//Gets the /insight/api/txs, then adds asset information to 
//asset passed in req.params.assetId
// OpenAssetService.prototype.oa_txs = function(req, res, next) {
//   var self = this;
//   enableCors(res);
//   var removal_count = 0;

//   try {
//     var assetIdBuffer = this.ce.assetBufferFromAssetAddress(req.params.assetId);
//   } catch(e) {
//     res.send(500, "Make sure you send a valid assetId");
//   }

//   var uri = 'http://' + req.headers.host + '/' + this.node.services['insight-api'].routePrefix + req.url;




//   console.log(uri);
//   request(uri, function(err, response, body) {
//     if (err)
//       res.status(500).send(err);
//     else {
//       //console.log(body);
//       var rslt = JSON.parse(body);

//       async.eachOfSeries(rslt.txs, function(tx, index, transformed) {
//         //console.log(tx);
        // async.mapSeries(tx.vout, function(an_output, trans) {
        //   //console.log(an_output);
        //   self.getTxo({txId: tx.txid, outputIdx: an_output.n}, function(err, output) {
        //       if (output && output.assetId == assetIdBuffer) {
        //         output.assetId = self.toB58Asset(output.assetId, self.network);
        //         output.assetAddress = self.toB58Address(output.assetAddress, self.node.network);
        //         if (output == 0)
        //           output.assetValueOut = output.unspentQty;
        //         else
        //           output.assetValueOut += output.unspentQty;
        //       }
        //     trans();
        //   }); 
//         });
//         transformed();
//       }, function(err, none) {
//         console.log('end result:' + JSON.stringify(rslt));
//         res.status(200).send(rslt);
//       });
//     }
//   });
// }

OpenAssetService.prototype.oa_txs = function(req, res, next) {
  var self = this;
  enableCors(res);

  var assetIdBuffer = this.assetBufferFromAssetId(req.params.assetId);

  if (req.query.block) {
    this.oa_txs_block({assetIdBuffer: assetIdBuffer, block:req.query.block, limit:req.query.limit || 1000}, function(err, rslt) {
      res.status(200).send({txs : rslt});
    });
  } else if (req.query.address) {
    this.oa_txs_address({assetIdBuffer: assetIdBuffer, address:req.query.address, limit:req.query.limit || 100}, function(err, rslt) {
      res.status(200).send({txs : rslt});
    });
  } else {
    //Call txs_block with no block
    this.oa_txs_block({assetIdBuffer: assetIdBuffer, limit:req.query.limit || 1000}, function(err, rslt) {
      res.status(200).send({txs : rslt});
    });
  }
}

OpenAssetService.prototype.oa_txs_block = function(params, callback) {
  var self = this;

  this.node.services.bitcoind.getBlockHeader(params.block, function(err, blk) {

    if (blk.height) {
      params.startBlock = blk.height;
      params.endBlock = blk.height;
    }

   // console.log('params:' + JSON.stringify(params));

    self.getTxByAsset(params, function (err, rslt) {
      //console.log('rslt:' + JSON.stringify(rslt));

      async.eachOfSeries(rslt, function(tx, index, transformed) {
        //console.log(tx);
        self.getTxo(tx, function(err, output) {
          console.log('output:' + JSON.stringify(output));
          tx.vout = [];
          var out = {};
          out.assetId = self.toB58Asset(output.assetId, self.network);
          out.assetAddress = self.toB58Address(output.assetAddress, self.node.network);
          if (out.assetValueOut === undefined)
            out.assetValueOut = output.qty;
          else
            out.assetValueOut += output.qty;

          out.isSpent = (output.unspentQty === 0);

          tx.vout.push(out);
          transformed();
        });
      }, function(err, none) {
        callback(err, rslt);
        //console.log('end result:' + JSON.stringify({txs : rslt}));
        //res.status(200).send({txs : rslt});
      });
    });
  });
}


OpenAssetService.prototype.oa_txs_address = function(params, callback) {
  var self = this;

  params.addressBuffer = this.ce.addressBufferFromAddress(params.address);

  // console.log('params:' + JSON.stringify(params));
  self.getTxByAddressAsset(params, function (err, rslt) {

    async.eachOfSeries(rslt, function(tx, index, transformed) {
      //console.log(tx);
      self.getTxo(tx, function(err, output) {
        console.log('output:' + JSON.stringify(output));
        tx.vout = [];
        var out = {};
        out.assetId = self.toB58Asset(output.assetId, self.network);
        out.assetAddress = self.toB58Address(output.assetAddress, self.node.network);
        if (out.assetValueOut === undefined)
          out.assetValueOut = output.qty;
        else
          out.assetValueOut += output.qty;

        out.isSpent = (output.unspentQty === 0);

        tx.vout.push(out);
        transformed();
      });
    }, function(err, none) {
      callback(err, rslt);
    });
  });
}


OpenAssetService.prototype.assetBufferFromAssetId = function(assetId) {
  return this.ce.assetBufferFromAssetAddress(assetId);
}

//Display only the blocks that contain the asset in question
// OpenAssetService.prototype.oa_blocks = function(req, res, next) {
//   var self = this;
//   enableCors(res);

//   try {
//     var assetIdBuffer = this.ce.assetBufferFromAssetAddress(req.params.assetId);
//   } catch(e) {
//     res.send(500, "Make sure you send a valid assetId");
//   }


//   this.getBlocksByAsset(assetIdBuffer, req.query.limit || 100, function(err, rslt) {

//     async.mapSeries(rslt.blocks, function(block, transformed) {
//       //console.log('Checking block:' + block.height)
//       self.node.services.bitcoind.getBlockOverview(block.height, function(err, info) {
//         //console.log(info);
//         block = info;
//         block.txlength = info.txids.length;
//         transformed(null, block);
//       })
//     },
//       function(err, rslt) {
//         //console.log(JSON.stringify(rslt));
//         res.status(200).send({blocks: rslt});
//       });
//   });
// }


//Show all blocks, but change the txlength to show number of tx with the asset in question
OpenAssetService.prototype.oa_blocks = function(req, res, next) {
  var self = this;
  enableCors(res);

  console.log('oa_blocks');

  var assetIdBuffer = this.assetBufferFromAssetId(req.params.assetId)

  var uri = 'http://' + req.headers.host + '/' + this.node.services['insight-api'].routePrefix + req.url;

  request(uri, function(err, response, body) {
    if (err)
      res.status(500).send(err);
    else {
      if (response.statusCode === 200) {
        var blocks_obj = JSON.parse(body);
        async.eachOfSeries(blocks_obj.blocks, function(block, itr, done) {
          self.getTxCountForBlock(block.height, assetIdBuffer, function (err, count) {
            blocks_obj.blocks[itr].txlength = count;
            done();
          });
        }, function(err) {
          res.status(response.statusCode).send(blocks_obj);
        });
      }
      else{
        res.status(response.statusCode).send(body);
      }
    }
  });
}

OpenAssetService.prototype.oa_block = function(req, res, next) {
  var self = this;
  enableCors(res);

  var assetIdBuffer = this.assetBufferFromAssetId(req.params.assetId)

  console.log('blockHash' + req.params.blockHash);
  this.node.services.bitcoind.getBlockHeader(req.params.blockHash, function(err, blk) {
    if (err)
      return res.status(404).send('Not found.');

    var p = {
      assetIdBuffer: assetIdBuffer
    }

    p.startBlock = blk.height;
    p.endBlock = blk.height;

    if (req.query.limit)
      p.limit = req.query.limit;

    console.log(p);
    console.log(blk);

    self.getTxByAsset(p, function (err, rslt) {
      var txs = rslt.map(function(obj){return(obj.txId);});
      blk.tx = txs;
      blk.isMainChain = true;
      blk.reward = 0;

      res.status(200).send(blk);
    });
  });
}


OpenAssetService.prototype.oa_tx = function(req, res, next) {
  var self = this;
  enableCors(res);

  var assetIdBuffer = this.assetBufferFromAssetId(req.params.assetId)

  var uri = 'http://' + req.headers.host + '/' + this.node.services['insight-api'].routePrefix + req.url;

  console.log('oa_tx:' + uri);

  try {
    request(uri, function(err, response, body) {
      if (err) {
        console.log('oa_tx err: '+ response.statusCode);
        return res.status(404).send(err);
      }
      else {
        console.log('oa_tx: '+ response.statusCode);
        if (response.statusCode == 200) {
          console.log(body);
          var rslt = JSON.parse(body);
          var tx  = rslt;

          res.status(200).send(rslt);
        } else {
          res.status(400).send(body);
        }
      }
    });
  } catch(e) {
    console.log('oa_tx catch: '+ response.statusCode);

    res.status(404).send(e);
  }
}


OpenAssetService.prototype.oa_addr = function(req, res, next) {
  console.log('oa_address');
  var self = this;
  enableCors(res);

  try {
    var assetIdBuffer = this.assetBufferFromAssetId(req.params.assetId)
    var addressBuffer = this.ce.addressBufferFromAddress(req.params.addr);
  } catch(e) {
    res.send(500, "Make sure you send a valid assetId and address");
  }

  var uri = 'http://' + req.headers.host + '/' + this.node.services['insight-api'].routePrefix + req.url;

  console.log('oa_address:' + uri);

  self.getAddrInfo({assetIdBuffer : assetIdBuffer, addressBuffer : addressBuffer}, function (err, rslt) {
    if (err) {
      res.status(500).send(err);
    } else {
      console.log('oa_address rslt:' + rslt);
      res.status(200).send(rslt);
    }
  });
}


OpenAssetService.prototype.proxyAPI = function(req, res, next) {
  var self = this;
  enableCors(res);

  var uri = 'http://' + req.headers.host + '/' + this.node.services['insight-api'].routePrefix + req.url;

  console.log('Proxy:' + uri);

  request(uri, function(err, response, body) {
    if (err)
      res.status(500).send(err);
    else {
      //console.log(JSON.stringify(body));
      res.status(200).send(body);
    }
  });
}


OpenAssetService.prototype.getPublishEvents = function() {
  //TB - Not working yet - too address centric - needs to be modified for openasset addresses
  //     and for openasset transactions.
  return [
    // {
    //   name: 'openasset/transaction',
    //   scope: this,
    //   subscribe: this.subscribe.bind(this, 'openasset/transaction'),
    //   unsubscribe: this.unsubscribe.bind(this, 'openasset/transaction')
    // },
    // {
    //   name: 'openasset/balance',
    //   scope: this,
    //   subscribe: this.subscribe.bind(this, 'openasset/balance'),
    //   unsubscribe: this.unsubscribe.bind(this, 'openasset/balance')
    // }
  ];
};

/**
 * The Bus will use this function to subscribe to the available
 * events for this service. For information about the available events
 * please see `getPublishEvents`.
 * @param {String} name - The name of the event
 * @param {EventEmitter} emitter - An event emitter instance
 * @param {Array} addresses - An array of addresses to subscribe
 */
OpenAssetService.prototype.subscribe = function(name, emitter, addresses) {
  $.checkArgument(emitter instanceof EventEmitter, 'First argument is expected to be an EventEmitter');
  $.checkArgument(Array.isArray(addresses), 'Second argument is expected to be an Array of addresses');

  for(var i = 0; i < addresses.length; i++) {
    var hashHex = bitcore.Address(addresses[i]).hashBuffer.toString('hex');
    if(!this.subscriptions[name][hashHex]) {
      this.subscriptions[name][hashHex] = [];
    }
    this.subscriptions[name][hashHex].push(emitter);
  }
};

/**
 * The Bus will use this function to unsubscribe to the available
 * events for this service.
 * @param {String} name - The name of the event
 * @param {EventEmitter} emitter - An event emitter instance
 * @param {Array} addresses - An array of addresses to subscribe
 */
OpenAssetService.prototype.unsubscribe = function(name, emitter, addresses) {
  $.checkArgument(emitter instanceof EventEmitter, 'First argument is expected to be an EventEmitter');
  $.checkArgument(Array.isArray(addresses) || _.isUndefined(addresses), 'Second argument is expected to be an Array of addresses or undefined');

  if(!addresses) {
    return this.unsubscribeAll(name, emitter);
  }

  for(var i = 0; i < addresses.length; i++) {
    var hashHex = bitcore.Address(addresses[i]).hashBuffer.toString('hex');
    if(this.subscriptions[name][hashHex]) {
      var emitters = this.subscriptions[name][hashHex];
      var index = emitters.indexOf(emitter);
      if(index > -1) {
        emitters.splice(index, 1);
      }
    }
  }
};

/**
 * A helper function for the `unsubscribe` method to unsubscribe from all addresses.
 * @param {String} name - The name of the event
 * @param {EventEmitter} emitter - An instance of an event emitter
 */
OpenAssetService.prototype.unsubscribeAll = function(name, emitter) {
  $.checkArgument(emitter instanceof EventEmitter, 'First argument is expected to be an EventEmitter');

  for(var hashHex in this.subscriptions[name]) {
    var emitters = this.subscriptions[name][hashHex];
    var index = emitters.indexOf(emitter);
    if(index > -1) {
      emitters.splice(index, 1);
    }
  }
};



///////////////////////////
//   Database - Access   //
///////////////////////////

var MIN_32BE = new Buffer('00000000', 'hex');
var MAX_32BE = new Buffer('FFFFFFFF', 'hex');
var MIN_INT = 0;
var MAX_INT = 0xFFFFFFFF;

/****************************************
v.assetIdBuffer - Buffer of assetId (required)
v.startBlock - Starting block (optional)
v.endBlock - Ending block (optional)
v.count - Number of records to return
*****************************************/
OpenAssetService.prototype.getTxByAsset = function(v, callback) {
  $.checkArgument(Buffer.isBuffer(v.assetIdBuffer) && v.assetIdBuffer.length===20, 'Requires an assetId');


  var startBlock = v.startBlock || MIN_INT;
  var endBlock = v.endBlock || MAX_INT;
  var reverse = v.count ? true : false;
  var limit = parseInt(v.count) || MAX_INT;
  var txAndOutput;

  var startBlockBuffer = new Buffer(4);
  startBlockBuffer.writeUInt32BE(startBlock);

  var endBlockBuffer = new Buffer(4);
  endBlockBuffer.writeUInt32BE(endBlock);

  var error;
  var transactions = [];

  //Read data from leveldb which is between our startDate and endDate
  var stream = this.openassets_store.createReadStream({
    limit: limit,
    reverse: reverse,
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETTOTX,
      v.assetIdBuffer,
      startBlockBuffer,
      MIN_32BE,  //Min txnum
      MIN_32BE   //Min outputnum
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETTOTX,
      v.assetIdBuffer,
      endBlockBuffer,
      MAX_32BE,  //Max txnum
      MAX_32BE   //Max outputnum
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    txAndOutput = encoding.decodeAssetToTxValue(data.value);
    transactions.push(txAndOutput);
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, transactions);
  });
};


/****************************************
v.assetIdBuffer - Buffer of assetId (required)
*****************************************/
OpenAssetService.prototype.getOwnersByAsset = function(v, callback) {
  var self = this;
  var error;
  var owners = [];

  //Read data from leveldb which is between our startDate and endDate
  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETTOBALANCE,
      v.assetIdBuffer,
      MIN_32BE,  //Min address
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETTOBALANCE,
      v.assetIdBuffer,
      MAX_32BE   //Max address
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    var key = encoding.decodeAssetToBalanceKey(data.key);
    var val = encoding.decodeAssetToBalanceValue(data.value);
    owners.push({assetId : self.toB58Asset(key.assetId, self.network), address : self.toB58Address(key.assetAddress, self.network), balance: val.balance});
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, owners);
  });
};

/********************************************************************
  p.assetIdBuffer - Buffer of address (required)
  p.count         - Number of records to return (most recent first)
*********************************************************************/
OpenAssetService.prototype.getTxByAddressQuery = function(p, callback) {
  $.checkArgument(p.addressBuffer && Buffer.isBuffer(p.addressBuffer) && p.addressBuffer.length===20, 'Request must include 20 byte address.');

  var self = this;
  var reverse = p.count ? true : false;
  var limit = parseInt(p.count) || MAX_INT;
  var key;

  var error;
  var transactions = [];

  console.log(JSON.stringify(p)); 

  //Read data from leveldb which is between our startDate and endDate
  var stream = this.openassets_store.createReadStream({
    limit: limit,
    reverse: reverse,
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ADDRESSTOTX,
      p.addressBuffer,
      MIN_32BE   //Min
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ADDRESSTOTX,
      p.addressBuffer,
      MAX_32BE   //Max
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    key = encoding.decodeAddressToTxKey(data.key);

    transactions.push({assetId: self.toB58Asset(key.assetId, self.network), tx: encoding.decodeAddressToTxValue(data.value)});
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, transactions);
  });
};



OpenAssetService.prototype.getTxByAddressAsset = function(params, callback) {
  $.checkArgument(params.addressBuffer && Buffer.isBuffer(params.addressBuffer) && params.addressBuffer.length===20, 'Request must include 20 byte addressBuffer');
  $.checkArgument(params.assetIdBuffer && Buffer.isBuffer(params.assetIdBuffer) && params.assetIdBuffer.length===20, 'Request must include 20 byte assetBuffer');

  var self = this;
  if (params.query && params.query.count) {
    params.limit = params.query.count;
    params.reverse = true;
  }
  //var reverse = params.query && params.query.count ? true : false;
  //var limit = params.query && parseInt(params.query.count) || MAX_INT;
  var key;

  var error;
  var transactions = [];

  //Read data from leveldb which is between our startDate and endDate
  var stream = this.openassets_store.createReadStream({
    limit: params.limit || 1000,
    reverse: params.reverse || false,
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ADDRESSTOTX,
      params.addressBuffer,
      params.assetIdBuffer,
      MIN_32BE   //Min
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ADDRESSTOTX,
      params.addressBuffer,
      params.assetIdBuffer,
      MAX_32BE   //Max
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    key = encoding.decodeAddressToTxKey(data.key);

    transactions.push(encoding.decodeAddressToTxValue(data.value));
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, transactions);
  });
};



OpenAssetService.prototype.getBalanceByAddressAsset = function(req, callback) {
  $.checkArgument(req.addressBuffer && Buffer.isBuffer(req.addressBuffer) && req.addressBuffer.length===20, 'Request must include 20 byte addressBuffer');
  $.checkArgument(req.assetBuffer && Buffer.isBuffer(req.assetBuffer) && req.assetBuffer.length===20, 'Request must include 20 byte assetBuffer');

  var self = this;
  var error;
  var balance = 0;

  //Read data from leveldb which is between our startDate and endDate
  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ADDRESSTOTX,
      req.addressBuffer,
      req.assetBuffer,
      MIN_32BE   //Min
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ADDRESSTOTX,
      req.addressBuffer,
      req.assetBuffer,
      MAX_32BE   //Max
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {

    var value = encoding.decodeAddressToTxValue(data.value)
    console.log('decoded data.value: ' + JSON.stringify(value));
    self.getTxo(value, function(err, output) {
        if (output.assetId) {
            console.log('output.unspentQty:' + output.unspentQty);
            if (output.unspentQty > 0) {
              balance += output.unspentQty;
          }
        }
      });

  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, {balance : balance});
  });
};




OpenAssetService.prototype.getAllTxos = function(req, callback) {
  var self = this;
  var counts = {
    utxo: 0,
    spent: 0,
    partial: 0
  }

  var utxo_count = 0;
  var spent_count = 0;
  var partially_spent_count = 0;
  var reverse = req.query.count ? true : false;
  var limit = parseInt(req.query.count) || MAX_INT;

  var error;
  var txos = [];

  //Read data from leveldb which is between our startDate and endDate
  var stream = this.openassets_store.createReadStream({
    limit: limit,
    reverse: reverse,
    gte: Buffer.concat([
      constants.ASSETPREFIXES.TXTOASSET,
      MIN_32BE   //Min
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.TXTOASSET,
      MAX_32BE   //Max
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    var key =   encoding.decodeTxToAssetKey(data.key);
    var value = encoding.decodeTxToAssetValue(data.value);

    //Convert to base58 
    value.assetAddress = self.toB58Address(value.assetAddress, self.network);
    value.assetId = self.toB58Asset(value.assetId, self.network);
    if (value.unspentQty > 0)
      counts.utxo++;

    if (value.unspentQty < value.qty && value.unspentQty !== 0)
      counts.partial++;

    if (value.unspentQty === 0)
      counts.spent++;

    //if (value.qty != value.unspentQty) {
      txos.push({key : key, value: value});
    //}
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    txos.push(counts);
    callback(null, txos);
  });
};



OpenAssetService.prototype.getAllTxByAsset = function(callback) {

  var error;
  var transactions = [];

  //Read data from leveldb which is between our startDate and endDate
  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETTOTX,
      MIN_32BE
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETTOTX,
      MAX_32BE   //Max outputnum
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    transactions.push({key:data.key.toString('hex'), value: data.value.toString('hex')});
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, transactions);
  });
};




OpenAssetService.prototype.getAssetByTxId = function(txIdBuffer, outputIdx, callback) {
  $.checkArgument((Buffer.isBuffer(txIdBuffer) && txIdBuffer.length===32) || txIdBuffer.length===64, 'First argument is expected to be a 32 byte txId.'); 
  $.checkArgument(isNumeric(outputIdx), 'Second argument is expected to be a numeric outputIdx');  

  if(!Buffer.isBuffer(txIdBuffer))
    txIdBuffer = new Buffer(txIdBuffer, 'hex');

  var outputIdxBuffer = new Buffer(4);
  outputIdxBuffer.writeUInt32BE(outputIdx);  

  var error;
  var asset = {};

  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.TXTOASSET,
      txIdBuffer,
      outputIdxBuffer
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.TXTOASSET,
      txIdBuffer,
      outputIdxBuffer
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    //There can only be one
    asset = encoding.decodeTxToAssetValue(data.value);
    //asset.txIdBuffer = txIdBuffer;
    asset.txId = txIdBuffer.toString('hex');
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, asset);
  });
};

OpenAssetService.prototype.getAllAssetByTxId = function(callback) {

  var error;
  var assets = [];

  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.TXTOASSET,
      MIN_32BE
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.TXTOASSET,
      MAX_32BE
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    var asset = encoding.decodeTxToAssetValue(data.value);
    //if (asset.qty > 0)
    assets.push(asset);
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, assets);
  });
};


OpenAssetService.prototype.getBlocksByAsset = function(assetIdBuffer, limit, callback) {
  $.checkArgument(assetIdBuffer && Buffer.isBuffer(assetIdBuffer) && assetIdBuffer.length===20, 'Request must include 20 byte assetBuffer');

  var result = {};
  result.blocks = [];
  var block_count = 0;
  var error;
  var blk = { last_height : 0};  

  limit = limit || 100;

  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETTOTX,
      assetIdBuffer,
      MIN_32BE   //Min
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETTOTX,
      assetIdBuffer,
      MAX_32BE   //Max
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary',
    reverse: true,
  });

  stream.on('data', function(data) {
    var key = encoding.decodeAssetToTxKey(data.key);
    //console.log(key);
    if (key.height != blk.last_height) {
      blk.last_height = key.height;
      //let iclone = Object.assign({}, key);
      result.blocks.push({height: key.height});
      block_count++;
    }
    if (block_count >= limit) {
      stream.destroy();
      //return callback(null, result);
    }
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, result);
  });

}

//Returns all the asset outputs recorded for a give txId (hex or buffer)
OpenAssetService.prototype.getOutputsByTxId = function(txIdBuffer, callback) {
  $.checkArgument(Buffer.isBuffer(txIdBuffer) || txIdBuffer.length===32, 'Argument is expected to be a 32 byte txId Buffer'); 

  if(!Buffer.isBuffer(txIdBuffer))
    txIdBuffer = new Buffer(txIdBuffer, 'hex');

  var error;
  var asset = {};
  var assets = [];

  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.TXTOASSET,
      txIdBuffer,
      MIN_32BE
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.TXTOASSET,
      txIdBuffer,
      MAX_32BE
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    asset = encoding.decodeTxToAssetValue(data.value);
    //asset.txIdBuffer = txIdBuffer;
    asset.txId = txIdBuffer.toString('hex');
    assets.push(asset);
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, assets);
  });
};






OpenAssetService.prototype.getAllTxByAddress = function(callback) {
  var self = this;
  var error;
  var txs_by_address = [];

  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ADDRESSTOTX,
      MIN_32BE
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ADDRESSTOTX,
      MAX_32BE
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    var a_tx = encoding.decodeAddressToTxValue(data.value);
    var a_tx_key = encoding.decodeAddressToTxKey(data.key);
    a_tx.assetAddress = self.toB58Address(new Buffer(a_tx_key.assetAddress, 'hex'), self.node.network);
    txs_by_address.push(a_tx);
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, txs_by_address);
  });
};

OpenAssetService.prototype.getTxCountForBlock = function(height, assetIdBuffer, callback) {
  this.getTxByAsset({assetIdBuffer: assetIdBuffer, startBlock : height,endBlock: height}, function (err, rslt) {
    callback(err, rslt.length);
  });
};

OpenAssetService.prototype.getAllTxByAddress = function(callback) {
  var self = this;
  var error;
  var txs_by_address = [];

  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ADDRESSTOTX,
      MIN_32BE
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ADDRESSTOTX,
      MAX_32BE
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    var a_tx = encoding.decodeAddressToTxValue(data.value);
    var a_tx_key = encoding.decodeAddressToTxKey(data.key);
    a_tx.assetAddress = self.toB58Address(new Buffer(a_tx_key.assetAddress, 'hex'), self.node.network);
    txs_by_address.push(a_tx);
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, txs_by_address);
  });
};

OpenAssetService.prototype.getAssetIdByName = function(req, callback) {
  var self = this;
  var assetInfo;
  var error;

  var nameBuffer = new Buffer(req.params.name, 'utf-8');

  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETNAMETOASSET,
      nameBuffer
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETNAMETOASSET,
      nameBuffer
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    assetInfo = encoding.decodeAssetNameToAssetValue(data.value);
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, assetInfo);
  });
};

OpenAssetService.prototype.getNamedAssets = function(req, callback) {
  var self = this;
  var assetIdName;
  var assetIdBuffer;
  var assets=[];
  var error;

  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETNAMETOASSET,
      MIN_32BE
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETNAMETOASSET,
      MAX_32BE
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    var assetName = encoding.decodeAssetNameToAssetKey(data.key);
    var aResult = encoding.decodeAssetNameToAssetValue(data.value);
    var assetIdBase58 = self.toB58Asset(aResult.assetId, self.network);
    assets.push({assetName: assetName, assetId: assetIdBase58});
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, assets);
  });
};


OpenAssetService.prototype.getAllMetaByAssetId = function(callback) {
  var self = this;
  var error;
  var meta_by_asset = [];

  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETTOMETA,
      MIN_32BE
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETTOMETA,
      MAX_32BE
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    var a_meta = encoding.decodeAssetToMetaValue(data.value);
    var a_meta_key = encoding.decodeAssetToMetaKey(data.key);
    meta_by_asset.push({key:a_meta_key, value:a_meta});
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, meta_by_asset);
  });
};


OpenAssetService.prototype.getAllBalances = function(callback) {
  var self = this;
  var error;
  var balances = [];

  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ADDRESSTOBALANCE,
      MIN_32BE
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ADDRESSTOBALANCE,
      MAX_32BE
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    var k = encoding.decodeAddressToBalanceKey(data.key);
    var v = encoding.decodeAddressToBalanceValue(data.value);
    var assetId = self.toB58Asset(k.assetId, self.network);
    var address = self.toB58Address(k.assetAddress, self.network);
    balances.push({assetId: assetId, address: address, balance:v.balance});
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, balances);
  });
};


OpenAssetService.prototype.getMetaByAssetId = function(assetIdBuffer, callback) {
  var self = this;
  var error;
  var meta_by_asset = [];

  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETTOMETA,
      assetIdBuffer
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.ASSETTOMETA,
      assetIdBuffer
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    var a_meta = encoding.decodeAssetToMetaValue(data.value);
    var a_meta_key = encoding.decodeAssetToMetaKey(data.key);
    meta_by_asset.push({key:a_meta_key, value:a_meta});
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, meta_by_asset);
  });
};



//Loops through an array of Tx and OutputIdx and gets the Tx Outputs from the index
//To get the unspent (UTXO), set the onlyUnspent to true
OpenAssetService.prototype.getTxos = function(arrOfTxAndOutputIdx, onlyUnspent, callback) {
  var self = this;
  var txOutputs = [];

  async.eachSeries(arrOfTxAndOutputIdx, function(txAndOi, next) {
    self.getTxo(txAndOi, function(err, output) {
        if (output.assetId) {
            if ((onlyUnspent === false) || (output.unspentQty > 0)) {
              output.assetId = self.toB58Asset(output.assetId, self.network);  //Convert to base58
              output.assetAddress = self.toB58Address(output.assetAddress, self.node.network);
              output.txId = txAndOi.txId;
              txOutputs.push(output);
          }
        }
        next();
      });
    },function done() {
        callback(null, txOutputs);
  });
}


OpenAssetService.prototype.getTxo = function(txAndOutputIdx, callback) {

  var keyBuffer = encoding.encodeTxToAssetKey(new Buffer(txAndOutputIdx.txId, 'hex'), txAndOutputIdx.outputIdx);

  var options = {
    keyEncoding: 'binary',
    valueEncoding: 'binary'
  };

  this.openassets_store.get(keyBuffer, options, function(err, data) {
    if (err) {
      if(err instanceof levelup.errors.NotFoundError)
        return callback(null, undefined);
      else 
        return callback(err);
    }
    else {
      var output = encoding.decodeTxToAssetValue(data);
      callback(null, output);
    }
  });
}

/*
OpenAssetService.prototype.getTxo = function(txAndOutputIdx, callback) {
  var self = this;
  var error;
  var output = {};

  var txIdBuffer = new Buffer(txAndOutputIdx.txId, 'hex');
  var outputIdxBuffer = new Buffer(4);
  outputIdxBuffer.writeUInt32BE(txAndOutputIdx.outputIdx);

  var stream = this.openassets_store.createReadStream({
    gte: Buffer.concat([
      constants.ASSETPREFIXES.TXTOASSET,
      txIdBuffer,
      outputIdxBuffer
    ]),
    lte: Buffer.concat([
      constants.ASSETPREFIXES.TXTOASSET,
      txIdBuffer,
      outputIdxBuffer
    ]),
    valueEncoding: 'binary',
    keyEncoding: 'binary'
  });

  stream.on('data', function(data) {
    output = encoding.decodeTxToAssetValue(data.value);
  });

  stream.on('error', function(streamError) {
    if (streamError) {
      error = streamError;
    }
  });

  stream.on('close', function() {
    if (error) {
      return callback(error);
    }
    callback(null, output);
  });
};
*/


//Just for displaying the index data - a debugging aid
OpenAssetService.prototype.showIndexes = function(){
    console.log('In Start - test - lookup asset');

  this.getAllTxByAsset(function(err, dta) {
      console.log('Showing all tx for asset');
      console.log(dta);
  });

  this.getAllAssetByTxId(function(err, dta) {
      console.log('Showing all asset (tx)');
      console.log(dta);
  });


  this.getAllTxByAddress(function(err, dta) {
      console.log('Showing all tx (for addresses)');
      console.log(dta);
  });

  this.getAllMetaByAssetId(function(err, dta) {
      console.log('Showing all metadata (for assets)');
      console.log(dta);
  });

  this.getAllBalances(function(err, dta) {
      console.log('Showing all balances');
      console.log(dta);
  });
}

module.exports = OpenAssetService;
