'use strict';

var assert = require('chai').assert;
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
var async = require('async');
var request = require("request");


chai.use(chaiHttp);
var server = 'http://localhost:3001';
var coinprism = 'https://api.coinprism.com/v1';


describe('Updated oa-core', function() {
    it('should return block height above 420,000 when fully synched.', function(done) {
      chai.request(server)
        .get('/oa/height')
        .end(function (err, res) {
          res.should.have.status(200);
          res.body.should.have.property('height');
          res.body.height.should.be.above(420000);
          done();
        });    
    });  
});


//Get all balances for every asset/address into an array
//Loop through the array and check each against a balance
//function that sums the UTXOs
describe('Internal oa-core Index Consistency', function() {
    this.timeout(60000);
    var balances;

    get_all_balances(function(err, rslt) {
      balances = rslt;
      console.log('balances length: ' + balances.length);
      //console.log(JSON.stringify(balances));
      for(var itr=0;itr<1/*balances.length*/;itr++) {
        checkEachBalance(balances, itr);
      };
    });
});


//Requires this closure to work properly
function checkEachBalance(balances, itr) {
  describe('Test a balance for asset: '+ balances[itr].assetId + ' and address: ' + balances[itr].address + ' cnt(' + itr + ')', function() {
    it('should equal ' + balances[itr].balance, function(done) {
      checkOaCoreUtxoBalance(balances[itr].assetId, balances[itr].address, balances[itr].balance, done);
    });
  });
}

function checkOaCoreUtxoBalance(asset, address, balance, done) {
  chai.request(server)
    .get('/oa/balance/' + asset + '/' + address)
      .end(function(err, res) {
        console.log('body' + JSON.stringify(res.body));
        console.log('res balance' + JSON.stringify(res.body.balance));
        console.log('balance' + balance);

        assert(res.body.balance == balance, 'Array balance is ' + balance + ' but UTXO balance is ' + res.body.balance);
        done();
      });
}


//Get all balances for every asset/address into an array
//Loop through the array and check each against a balance
//function that sums the UTXOs
describe('oa-core check balances against coinprism', function() {
    this.timeout(60000);
    var balances;

    get_all_balances(function(err, rslt) {
      balances = rslt;
      console.log('balances length: ' + balances.length);
      //console.log(JSON.stringify(balances));
      for(var itr=0;itr<10 /*balances.length*/;itr++) {
        checkEachBalanceAgainstCoinprism(balances, itr);
      };
    });
});

//Requires this closure to work properly
function checkEachBalanceAgainstCoinprism(balances, itr) {
  describe('Test coinprism balance for asset: '+ balances[itr].assetId + ' and address: ' + balances[itr].address + ' cnt(' + itr + ')', function() {
    this.timeout(10000);
    it('should equal ' + balances[itr].balance, function(done) {
      checkCoinprismBalance(balances[itr].assetId, balances[itr].address, balances[itr].balance, done);
    });
  });
}


function checkCoinprismBalance(asset, address, balance, done) {

  chai.request(coinprism)
    .get('/addresses/' + address)
    .end(function (err, res) {
      if (res.body.assets) {
        for(var i=0;i<res.body.assets.length;i++) {
          if (res.body.assets[i].id == asset) {
            assert(res.body.assets[i].balance == balance, 'Array balance is ' + balance + ' but Coinprism balance is ' + res.body.assets[i].balance);
            done();
          }
        }
      }
      else
      {
        done();
      }
    });
}


describe('Balance API', function() {
    this.timeout(16000);
    var asset = 'AGXSTkTjYAZRh9Q3484vqxus355VzjYF5v';
    var address = 'akDVEPcXFivMsNiRP5ZpdEBZYj5t7PwmrZD';
    var cpm_balance = undefined;
    var cpm_owners = undefined;

    before(function(done) {
      get_coinprism_balance(asset, address, function(err, balance) {
        cpm_balance = balance;
        done();
      });
    });

    before(function(done) {
      get_coinprism_asset_owners(asset, function(err, owners) {
        cpm_owners = owners;
        done();
      });
    });


    it('should return the same balance for an address as coinprism', function(done) {
       get_oa_balance(asset, address, function(err, oa_balance) {
          if (err)
            console.log(err);
          assert(oa_balance == cpm_balance, "Balance should match.  oa:" + oa_balance + ' coinprism:' + cpm_balance);
          done();
       });
    });
});

function get_coinprism_balance(asset, address, callback) {

  chai.request(coinprism)
    .get('/addresses/' + address)
    .end(function (err, res) {
      if (res.body.assets) {
        for(var i=0;i<res.body.assets.length;i++) {
          if (res.body.assets[i].id == asset) {
            var balance = res.body.assets[i].balance;          
          }
        }
      }

      callback(null, balance);
    });
}

function get_coinprism_asset_owners(asset, callback) {
  var owners;

  chai.request(coinprism)
    .get('/assets/' + asset+ '/owners')
    .end(function (err, res) {
      if (err)
        callback(err);
      else
        callback(null, res.body);
    });
}

function get_all_balances(callback) {
  chai.request(server)
    .get('/oa/balances')
    .end(function (err, res) {
        //console.log(JSON.stringify(res));
        callback(err, res.body);
    });
}

  // get_item(our_api + '/balances', null, function (err, lst) {
  //   console.log('Count balances:' + lst.length);
  //   console.log(lst);
  //   console.log(lst[0]);
  //   console.log(lst[0].assetId);
  //   //lst = JSON.parse(lst);
  //   async.forEachOfSeries(lst, 
  //     function(one, i, cb) {
  //       console.log("Get:" + i + ' ' + our_api + '/balance/' + one.assetId+'/'+one.address);
  //       get_item(our_api + '/balance/'+one.assetId+'/'+one.address, null, function (err, lst2) {
  //         if (err) {
  //           console.log(err);
  //           cb(err);
  //         }
  //         else {
  //           if(lst2.balance === one.balance)
  //             console.log("Passed: " + ' Expected:' + one.balance);
  //           else
  //             console.log('Failed on assetId: ' + JSON.stringify(lst2) + ' Expected:' + one.balance);
  //           cb();
  //         }
  //       });

  //   },
  //     function(err) {
  //       console.log("Finished Checking");
  //     }

  //   );
  // });

function get_oa_balance(asset, address, callback) {
  var balance;

  chai.request(server)
    .get('/oa/balance/'+asset+'/'+address)
    .end(function (err, res) {
        callback(err, res.body.balance);
    });
}


// var async = require('async');
// var request = require("request");
// var coloringengine = require("../coloringengine");
// var ce = new coloringengine.ColoringEngine(function(h,c) {});

// var oa_api = 'http://localhost:3001/oa';
// var cpm_api = 'https://api.coinprism.com/v1';

// function test_address_balance_against_coinprism(asset, address, done) {
//   a_equals_b(oa_api+'/balance/'+asset+'/'+address, 'balance', cpm_api + '/addresses/'+address, 'balance', done);
// }

// function test_oa_against_coinprism(hash, done) {
//   a_equals_b(oa_api+'/tx/'+hash, 'qty', cpm_api + '/transactions/'+hash, 'asset_qty', done);
// };


// function a_equals_b(url_a, item_a, url_b, item_b, callback) {
//   async.parallel({
//       a: function(callback_a) { get_item(url_a, item_b, function(err, rslt_a) {
//           callback_a(null, rslt_a);
//         })
//       },
//       b: function(callback_b) { get_item(url_b, item_b, function(err, rslt_b) {
//           callback_b(null, rslt_b);
//         })
//       },


//     }, function(err, results) {
//       if (err)
//         throw err;

//       if (results.a === undefined) {
//         throw('Result from first url is undefined: ' + url_a);
//       }

//       if (results.b === undefined) {
//         throw('Result from second url is undefined: ' + url_b);
//       }

//       console.log(results);
//       if (results[0] === results[1]) {
//         console.log(results[0] + ' is equal to ' + results[1]);
//         callback(null, true);
//       }

//     }
//   );
// }



// /* Pass in a URL and a top-level item to return from the JSON
//  * Or leave item as undefined get the all the data back      */
// function get_item(url, item, callback) {
//   var data;
//   if (url) {
//     request(url, function(error, response, body) {
//       if (error) {
//           console.log("Error getting info from " + url + error);
//           return callback(error);
//       }
//       if (response.statusCode == 200) {
//         data = JSON.parse(body);
//         if (item)
//           callback(null, data[item]);
//         else 
//           callback(null, data);
//       }
//       else
//       {
//         console.error('url call failed: ' + url);
//         callback('URL call failed:');
//       }
//     });
//   }
//   else
//   {
//     //If not a url, just return json name
//     callback('Invalid URL:' + url); 
//   } 
// }
