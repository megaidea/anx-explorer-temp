var inherits = require('util').inherits;
var EventEmitter = require('events').EventEmitter;


function WEC_Service(options) {
  EventEmitter.call(this);
  this.node = options.node;
  this.assetId = "AGXSTkTjYAZRh9Q3484vqxus355VzjYF5v";

  //Remove divisibility once we have it in oa-core
  this.divisibility = 5;
  this.divisor = Math.pow(10, this.divisibility);
}
inherits(WEC_Service, EventEmitter);

WEC_Service.dependencies = ['bitcoind', 'oa-core'];

WEC_Service.prototype.start = function(callback) {
  setImmediate(callback);
};

WEC_Service.prototype.stop = function(callback) {
  setImmediate(callback);
};

WEC_Service.prototype.getRoutePrefix = function() {
  return 'WEC';
};

WEC_Service.prototype.setupRoutes = function(app) {
  app.get('/balance', this.balance.bind(this));
  app.get('/available', this.available.bind(this));
  app.post('/balance', this.balance.bind(this));
  app.post('/available', this.available.bind(this));
  app.get('/owners', this.owners.bind(this));
  app.get('/txs', this.txs.bind(this));  
  app.get('/tx/:txId', this.tx.bind(this));  
  app.get('/status', this.proxyAPI.bind(this));  
  app.get('/currency', this.proxyAPI.bind(this));  
  app.get('/sync', this.proxyAPI.bind(this));  
  app.get('/peer', this.proxyAPI.bind(this));
  app.get('/address', this.address.bind(this));  
  app.get('/blocks', this.blocks.bind(this));  
  app.get('/block/:blockHash', this.block.bind(this));  
  app.get('/version', this.proxyAPI.bind(this));  
};

WEC_Service.prototype.getAPIMethods = function() {
  return [];
};

WEC_Service.prototype.getPublishEvents = function() {
  return [];
};

WEC_Service.prototype.getPublishEvents = function() {
  return [];
};

WEC_Service.prototype.balance = function(req, res, next) {
    var self = this;

    this.node.ownersByAsset(this.assetId, function (err, rslt) {
        if (err) {
            console.log(err);
            res.status(500).send(err);
        } else {
            var sum = 0;
            for(var i=0;rslt && i<rslt.length;i++) {
                console.log(rslt[i].balance);
                sum += rslt[i].balance;
            }
            res.status(200).send(JSON.stringify({balance : sum / self.divisor }));
        }
    });
}

WEC_Service.prototype.available = function(req, res, next) {
    var self = this;

    this.node.ownersByAsset(this.assetId, function (err, rslt) {
        if (err) {
            console.log(err);
            res.status(500).send(err);
        } else {
            var sum = 0;
            for(var i=0;rslt && i<rslt.length;i++) {
                //Do not count these owner addresses for available
                if ( (rslt[i].address != "akYFNxN7jZAWhK9t4CduJXCxPioPEmPwvvb") && 
                     (rslt[i].address != "anYwPsucB74q5QKaVKjZtwqKKsKfxJNrDar") &&   //Same as akYFNxN7jZAWhK9t4CduJXCxPioPEmPwvvb, but encoded for multisig
                     (rslt[i].address != "akRNa83e6Cp7aswkuvbY8J69MwjTfQeLwBq") && 
                     (rslt[i].address != "akDVEPcXFivMsNiRP5ZpdEBZYj5t7PwmrZD") && 
                     (rslt[i].address != "akWKFi1qVYrZFg4oEgixcmPPXTuTeFnp2Sg") 
                    ) {
                        sum += rslt[i].balance; 
                }
            }
            res.status(200).send(JSON.stringify({available : sum / self.divisor}));
        }
    });
}



WEC_Service.prototype.txs = function(req, res, next) {
  //console.log('going through WEC');
  req.params.assetId = this.assetId;
  this.node.oa_txs(req,res,next);
}

WEC_Service.prototype.tx = function(req, res, next) {
  req.params.assetId = this.assetId;
  this.node.oa_tx(req,res,next);
}

WEC_Service.prototype.blocks = function(req, res, next) {
  req.params.assetId = this.assetId;
  //console.log(req);
  this.node.oa_blocks(req,res,next);
}

WEC_Service.prototype.block = function(req, res, next) {
  req.params.assetId = this.assetId;
  this.node.oa_block(req,res,next);
}

WEC_Service.prototype.address = function(req, res, next) {
  req.params.assetId = this.assetId;
  console.log('Calling oa_address');
  this.node.oa_address(req,res,next);
}

WEC_Service.prototype.proxyAPI = function(req, res, next) {
  this.node.proxyAPI(req,res,next);
}


WEC_Service.prototype.owners = function(req, res, next) {
    var self = this;

    this.node.ownersByAsset(this.assetId, function (err, rslt) {
        if (err) {
            console.log(err);
            res.status(500).send(err);
        } else {
            for(var i=0;i<rslt.length;i++) {
              rslt[i].balance = rslt[i].balance / self.divisor;
            }
            res.status(200).send(JSON.stringify(rslt));
        }
    });
}

module.exports = WEC_Service;