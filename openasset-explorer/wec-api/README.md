##Usage
[http://localhost:3011/WEC/balance](http://localhost:3011/WEC/balance) -- Shows total issued.  
[http://localhost:3011/WEC/available](http://localhost:3011/WEC/available) -- Shows total issued minus addresses owned by creators and private investors.  
[http://localhost:3011/WEC/owners](http://localhost:3011/WEC/owners) -- Shows balance in each address.
Custom or passthrough proxy apis added to make wec-api compatible with insight-api    


##Installation
wec-api depends on bitcoind, and oa-core.  Follow installation instructions for oa-core.

Then in the node_modules folder in the bitcore installation:

From the `node_modules` folder where you see bitcore-node folder:

`ln -s <your path>/anx-explorer/wec-api`

From the `wec-api` folder, run `npm install` to install the dependencies.

Add `"wec-api"` after `"oa-core"` in the `bitcore-node.json` config file.

wec-api now passes through API calls, so it can act as the primary api to insight-ui instead of insight-api.
To configure (on bitcore 4.0) - Add this section under "servicesConfig":
    "insight-ui": {
      "apiPrefix": "WEC",
      "routePrefix": ""
    },

Setting routePrefix to "" will eliminate need for /insight
Setting apiPrefix will cause insight-ui to use /WEC (using wec-api) instead of /api or /insight-api




