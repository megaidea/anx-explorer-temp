# Please change the multichain path to your folder
multichainPath='/usr/share/nginx/html/anx-explorer/'

echo "\nEntering File..."

cd $multichainPath
pwd
echo "\n"
echo "Please enter the multichain file name: "
read input_variable
echo "\n"

FILE=$multichainPath$input_variable
if [ ! -e "$FILE" ]
  then
    echo "The new file name is: $input_variable"
    cp -R ./multichain-explorer/ ./$input_variable/
    cd ./$input_variable/node_modules
    rm -rf insight-ui insight-api bitcore-node bitcore-lib
    ln -s ../../multichain-explorer/node_modules/bitcore-lib/
    ln -s ../../multichain-explorer/node_modules/bitcore-node/
    ln -s ../../multichain-explorer/node_modules/insight-api/
    ln -s ../../multichain-explorer/node_modules/insight-ui/
    echo "Multichain is created, please change the multichain config of bitcore-node.js in "$input_variable" file"
    exit
  else
    echo "Sorry file exist, please run the script again\n"
    exit
fi
