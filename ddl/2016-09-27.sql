-- WOW-197
-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', '', 'home.available_supply.value', '2', NULL);
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "", "8,404,396.80,775", '6', Null, (select `id` from `config_translation_key` where `category` = "explorer" and `key` = "home.available_supply.value" and `platform_id` = 2));
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "", "8,404,396.80,775", '7', Null, (select `id` from `config_translation_key` where `category` = "explorer" and `key` = "home.available_supply.value" and `platform_id` = 2));
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "", "8,404,396.80,775", '8', Null, (select `id` from `config_translation_key` where `category` = "explorer" and `key` = "home.available_supply.value" and `platform_id` = 2));


INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', '', 'home.total_supply.value', '2', NULL);
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "", "100,000,000", '6', Null, (select `id` from `config_translation_key` where `category` = "explorer" and `key` = "home.total_supply.value" and `platform_id` = 2));
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "", "100,000,000", '7', Null, (select `id` from `config_translation_key` where `category` = "explorer" and `key` = "home.total_supply.value" and `platform_id` = 2));
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "", "100,000,000", '8', Null, (select `id` from `config_translation_key` where `category` = "explorer" and `key` = "home.total_supply.value" and `platform_id` = 2));
-- WOW-197 END

-- Fix External Verification
-- End
