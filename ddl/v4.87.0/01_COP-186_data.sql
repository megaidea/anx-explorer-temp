START TRANSACTION;

SET @platform_id = (SELECT `id` FROM `config_platform` WHERE `name` = 'explorer');

-- INSERT NEW i18n KEY & VALUE

SET @pt_BR = (SELECT `id` FROM `config_language` WHERE `iso` = 'pt_BR');
SET @es_MX = (SELECT `id` FROM `config_language` WHERE `iso` = 'es_MX');
SET @en_US = (SELECT `id` FROM `config_language` WHERE iso = 'en_US');
SET @zh_HK = (SELECT `id` FROM `config_language` WHERE iso = 'zh_HK');
SET @zh_CN = (SELECT `id` FROM `config_language` WHERE iso = 'zh_CN');
SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.latestblocks.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'LOS ÚLTIMOS BLOQUES', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Blocos mais recentes', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.height.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Altura', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Altura', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.age.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Antigüedad', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Idade', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.transactions.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Actas', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'transações', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.size.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Tamaño', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Tamanho', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.mindedby.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Minado por', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Minado por', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.seeallblocks.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Ver todos los bloques', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Ver todos os blocos', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.hash.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Picadillo', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Jogo da velha', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.about.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Acerca de', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Sobre', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.about.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Acerca de', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Sobre', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();


SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.aboutmc.text');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'ANX Explorer es un explorador de recursos de código abierto con API REST completa y websocket para sus activos. Visite http://anxintl.com/abs para crear su propio activo y comience su proyecto de crypto-asset con ANX.', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'O ANX Explorer é um explorador de recursos de código aberto com APIs REST e websocket completas para seus ativos. Visite http://anxintl.com/abs para criar seu próprio recurso e começar seu projeto de criptografia com ANX.', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.poweredby.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Energizado por', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Distribuído por', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.blocks.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Bloques', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Blocos', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.bydate.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'por fecha.', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'por data.', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.timestamp.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Marca de tiempo', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Timestamp', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.today.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Hoy', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Hoje', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.applicationstatus.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Estado de la aplicación', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Status do aplicativo', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.syncstatus.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Estado de sincronización', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Status da sincronização', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();


SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.syncprogress.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Progreso de sincronización', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Progresso da sincronização', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();



SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.currentsyncstatus.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Estado de sincronización actual', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Status da sincronização atual', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.startdate.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Fecha de inicio', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Data de início', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.initialblockchainheight.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Altura inicial de la cadena del bloque', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Altura da Cadeia do Bloco Inicial', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.synctype.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Tipo de sincronización', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Tipo de sincronização', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();


SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.lastblock.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Último bloque', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Último Bloco', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.lastblockhash.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Último bloque de hash (Bitcoind)', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Último Bloquear Hash (Bitcoind)', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();


SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.currentblockchaintip.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Extremidad actual de la cadena del bloque (visión)', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Corrente Blockchain Dica (discernimento)', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.currentblockchaintip.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Extremidad actual de la cadena del bloque (visión)', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Corrente Blockchain Dica (discernimento)', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.bitcoinnodeinformation.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Información del nodo Bitcoin', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Informações do nó Bitcoin', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.version.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Versión', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Versão', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.version.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Versión', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Versão', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.connectionstoothernodes.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Conexiones a otros nodos', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Conexões com outros nós', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();


SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.miningdifficulty.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Dificultad Minera', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Dificuldade em Mineração', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();



SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.network.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Red', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Rede', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.address.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Dirección', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Endereço', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();


SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.mined.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'minado', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Minado', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.confirmations.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'CONFIRMACIONES', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'CONFIRMAÇÕES', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.summary.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Resumen', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Resumo', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.receivedtime.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Tiempo recibido', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Tempo recebido', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();


SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.minedtime.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Tiempo Minado', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Tempo Minado', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.details.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Detalles', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Detalhes', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();


SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.transaction.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Transacción', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Transação', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();


INSERT INTO `config_translation_key`
        (`category`, `created_at`, `key`, `platform_id`)
        VALUES
                ("explorer", NOW(), 'home.includedinblock.label', @platform_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();
SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.includedinblock.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Incluido en el bloque', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Incluído no Bloco', @pt_BR, NULL, @key_id),
                (NULL, NOW(), 'Included in Block', @en_US, NULL, @key_id),
                (NULL, NOW(), '於區塊', @zh_HK, NULL, @key_id),
                (NULL, NOW(), '于区块', @zh_CN, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'header.search.placeholder');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Búsqueda de bloque, transacción o dirección', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Procurar por bloco, transação ou endereço', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.status.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Estado', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Estado', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();

SET @key_id = (SELECT `id` FROM `config_translation_key` WHERE `key` = 'home.assets.label');

INSERT INTO `config_translation_value`
        (`environment_id`, `created_at`, `value`, `language_id`, `site_id`, `key_id`)
        VALUES
                (NULL, NOW(), 'Bienes', @es_MX, NULL, @key_id),
                (NULL, NOW(), 'Ativos', @pt_BR, NULL, @key_id)
        ON DUPLICATE KEY UPDATE
                `updated_at` = NOW();



COMMIT;
