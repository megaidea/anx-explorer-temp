-- Update Default Css Key for explorer

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#D02128' WHERE `key` = 'btn-primary-bg' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#7A1617' WHERE `key` = 'btn-primary-border-bg' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#D02128' WHERE `key` = 'progress-bar-bg' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#2FA4D7' WHERE `key` = 'txvalues-success-bg' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#D02128' WHERE `key` = 'txvalues-primary-bg' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#AC0015' WHERE `key` = 'txvalues-danger-bg' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#373D42' WHERE `key` = 'progress-bar-info-bg' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#D02128' WHERE `key` = 'header-bg' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#7A1617' WHERE `key` = 'header-search-bg' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#fff' WHERE `key` = 'header-search-placeholder-color' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#373D42' WHERE `key` = 'header-status-bg' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#373D42' WHERE `key` = 'footer-bg' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#fff' WHERE `key` = 'footer-link-color' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#fff' WHERE `key` = 'color-primary' AND `platform_id` = 2 AND site_id IS NULL;

UPDATE `config_css_key` a inner join `config_css_value` b on a.id = b.key_id SET `value` = '#fff' WHERE `key` = 'link-text-color' AND `platform_id` = 2 AND site_id IS NULL;

-- Add i18n key
-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Brand Name', 'home.brand.label', '2', '181');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "ANX Explorer", "ANX Explorer", '6', Null, '181');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "ANX Explorer", "ANX Explorer", '7', Null, '181');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "ANX Explorer", "ANX Explorer", '8', Null, '181');
