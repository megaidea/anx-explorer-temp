UPDATE `config_translation_key` SET `key` = 'home.404.text' WHERE `key` = '404 Page not found :(' AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.about.label' WHERE `key` = 'About' AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.address.label' WHERE `key` = 'Address' AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.age.label' WHERE `key` = 'Age' AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.applicationstatus.label' WHERE `key` = 'Application Status' AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.bestblock.label' WHERE `key` = 'Best Block' AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.bitcoinnodeinformation.label' WHERE `key` = 'Bitcoin node information' AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.block.label' WHERE `key` = 'Block' AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.blockreward.label' WHERE `key` = 'Block Reward' AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.blocks.label' WHERE `key` = 'Blocks' AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.bytesserialized.label' WHERE `key` = 'Bytes Serialized' AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.cantconnecttobitcoind.text' WHERE `key` = "Can't connect to bitcoind to get live updates from the p2p network. (Tried connecting to bitcoind at {{host}}:{{port}} and failed.)" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.cantconnecttoinsightserver.text' WHERE `key` = "Can't connect to insight server. Attempting to reconnect..." AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.cantconnecttointernet.text' WHERE `key` = "Can't connect to internet. Please, check your connection." AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.complete.label' WHERE `key` = "Complete" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.confirmations.label' WHERE `key` = "Confirmations" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.Conn.label' WHERE `key` = "Conn" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.connectionstoothernodes.label' WHERE `key` = "Connections to other nodes" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.currentblockchaintip.label' WHERE `key` = "Current Blockchain Tip (insight)" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.currentsyncstatus.label' WHERE `key` = "Current Sync Status" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.details.label' WHERE `key` = "Details" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.difficulty.label' WHERE `key` = "Difficulty" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.doublespentattemptdetected.text' WHERE `key` = "Double spent attempt detected. From tx:" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.error.text' WHERE `key` = "Error!" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.finalbalance.label' WHERE `key` = "Final Balance" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.finishdate.label' WHERE `key` = "Finish Date" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.gotohome.text' WHERE `key` = "Go to home" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.hashserialized.label' WHERE `key` = "Hash Serialized" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.height.label' WHERE `key` = "Height" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.incoherenceinleveldbdetected.label' WHERE `key` = "Incoherence in levelDB detected:" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.infoerrors.label' WHERE `key` = "Info Errors" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.initialblockchainheight.label' WHERE `key` = "Initial Block Chain Height" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.input.label' WHERE `key` = "Input" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.lastblock.label' WHERE `key` = "Last Block" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.lastblockhash.label' WHERE `key` = "Last Block Hash (Bitcoind)" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.latestblocks.label' WHERE `key` = "Latest Blocks" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.latesttransactions.label' WHERE `key` = "Latest Transactions" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.loadingaddressinformation.label' WHERE `key` = "Loading Address Information" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.loadingblockinformation.label' WHERE `key` = "Loading Block Information" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.loadingselecteddate.label' WHERE `key` = "Loading Selected Date..." AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.loadingtransactiondetails.label' WHERE `key` = "Loading Transaction Details" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.loadingtransactions.label' WHERE `key` = "Loading Transactions..." AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.loading.text' WHERE `key` = "Loading..." AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.minedtime.label' WHERE `key` = "Mined Time" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.mindedby.label' WHERE `key` = "Mined by" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.miningdifficulty.label' WHERE `key` = "Mining Difficulty" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.nextblock.label' WHERE `key` = "Next Block" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.noinputsnewlygeneratedcoins.text' WHERE `key` = "No Inputs (Newly Generated Coins)" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.noblocks.text' WHERE `key` = "No blocks yet." AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.nomatchingrecordsfound.label' WHERE `key` = "No matching records found!" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.numtransactions.label' WHERE `key` = "No. Transactions" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.numberoftransactions.label' WHERE `key` = "Number Of Transactions" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.output.label' WHERE `key` = "Output" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.poweredby.label' WHERE `key` = "Powered by" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.previousblock.label' WHERE `key` = "Previous Block" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.protocolversion.label' WHERE `key` = "Protocol version" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.proxysetting.label' WHERE `key` = "Proxy setting" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.receivedtime.label' WHERE `key` = "Received Time" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.redirecting.label' WHERE `key` = "Redirecting..." AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'header.search.placeholder' WHERE `key` = "Search for block, transaction or address" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.seeallblocks.label' WHERE `key` = "See all blocks" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.showtransactionoutputdata.label' WHERE `key` = "Show Transaction Output data" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.showall.label' WHERE `key` = "Show all" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.showinput.label' WHERE `key` = "Show input" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.showless.label' WHERE `key` = "Show less" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.showmore.label' WHERE `key` = "Show more" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.size.label' WHERE `key` = "Size" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.sizeinbytes.label' WHERE `key` = "Size (bytes)" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.skippedblockspreviouslysynced.label' WHERE `key` = "Skipped Blocks (previously synced)" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.startdate.label' WHERE `key` = "Start Date" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.status.label' WHERE `key` = "Status" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.summary.label' WHERE `key` = "Summary" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.summaryconfirmed.text' WHERE `key` = "Summary <small>confirmed</small>" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.syncprogress.label' WHERE `key` = "Sync Progress" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.syncstatus.label' WHERE `key` = "Sync Status" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.synctype.label' WHERE `key` = "Sync Type" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.syncedblocks.label' WHERE `key` = "Synced Blocks" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.testnet.label' WHERE `key` = "Testnet" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.therearenotransactionsinvolvingthisaddress.text' WHERE `key` = "There are no transactions involving this address" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.timeoffset.label' WHERE `key` = "Time Offset" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.timestamp.label' WHERE `key` = "Timestamp" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.today.label' WHERE `key` = "Today" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.totalamount.label' WHERE `key` = "Total Amount" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.totalreceived.label' WHERE `key` = "Total Received" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.totalsent.label' WHERE `key` = "Total Sent" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.transaction.label' WHERE `key` = "Transaction" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.transactionoutputsetinformation.label' WHERE `key` = "Transaction Output Set Information" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.transactionoutputs.label' WHERE `key` = "Transaction Outputs" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.transactions.label' WHERE `key` = "Transactions" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.type.label' WHERE `key` = "Type" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.unconfirmed.label' WHERE `key` = "Unconfirmed" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.unconfirmedtransaction.label' WHERE `key` = "Unconfirmed Transaction!" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.unconfirmedtxsbalance.label' WHERE `key` = "Unconfirmed Txs Balance" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.valueout.label' WHERE `key` = "Value out" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.version.label' WHERE `key` = "Version" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.waitingforblocks.label' WHERE `key` = "Waiting for blocks..." AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.waitingfortransactions.label' WHERE `key` = "Waiting for transactions..." AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.bydate.label' WHERE `key` = "by date." AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.firstseenat.label' WHERE `key` = "first seen at" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.mined.label' WHERE `key` = "mined" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.minedon.label' WHERE `key` = "mined on:" AND `platform_id` = 2;

UPDATE `config_translation_key` SET `key` = 'home.waitingforblocks.label' WHERE `key` = "Waiting for blocks" AND `platform_id` = 2;


-- insert new key

-- Inserting new keys

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'About Text', 'home.about.text', '2', '158');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "WEC Coins rides on Open Assets Protocol which is a simple and powerful protocol built on top of the Bitcoin Blockchain. The Open Assets Protocol is an evolution of the concept of colored coins.", "WEC Coins rides on Open Assets Protocol which is a simple and powerful protocol built on top of the Bitcoin Blockchain. The Open Assets Protocol is an evolution of the concept of colored coins.", '6', Null, '158');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "WEC硬幣騎在開放式資產協議是建立在比特幣Blockchain的頂部有一個簡單而功能強大的協議。開放式資產議定書是彩色金幣的概念的演變。", "WEC硬幣騎在開放式資產協議是建立在比特幣Blockchain的頂部有一個簡單而功能強大的協議。開放式資產議定書是彩色金幣的概念的演變。", '7', Null, '158');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "WEC硬币骑在开放式资产协议是建立在比特币Blockchain的顶部有一个简单而功能强大的协议。开放式资产议定书是彩色金币的概念的演变。", "WEC硬币骑在开放式资产协议是建立在比特币Blockchain的顶部有一个简单而功能强大的协议。开放式资产议定书是彩色金币的概念的演变。", '8', Null, '158');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Hash', 'home.hash.label', '2', '159');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Hash", "Hash", '6', Null, '159');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "哈希", "哈希", '7', Null, '159');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "哈希", "哈希", '8', Null, '159');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Verify', 'home.verify.label', '2', '160');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Verify", "Verify", '6', Null, '160');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "校驗", "校驗", '7', Null, '160');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "校验", "校验", '8', Null, '160');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Verify signed message', 'home.verifysignedmessage.label', '2', '161');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Verify signed message", "Verify signed message", '6', Null, '161');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "驗證簽名郵件", "驗證簽名郵件", '7', Null, '161');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "验证签名邮件", "验证签名邮件", '8', Null, '161');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Signature', 'home.signature.label', '2', '162');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Signature", "Signature", '6', Null, '162');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "簽名", "簽名", '7', Null, '162');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "签名", "签名", '8', Null, '162');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Message', 'home.message.label', '2', '163');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Message", "Message", '6', Null, '163');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "信息", "信息", '7', Null, '163');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "信息", "信息", '8', Null, '163');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Network', 'home.network.label', '2', '164');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Network", "Network", '6', Null, '164');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "網絡", "網絡", '7', Null, '164');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "网络", "网络", '8', Null, '164');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Fee Rate', 'home.feerate.label', '2', '165');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Fee Rate", "Fee Rate", '6', Null, '165');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "費率", "費率", '7', Null, '165');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "费率", "费率", '8', Null, '165');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Unsupported Transaction.', 'home.unsupportedtransaction.label', '2', '166');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Unsupported Transaction.", "Unsupported Transaction.", '6', Null, '166');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "不支持事務處理。", "不支持事務處理。", '7', Null, '166');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "不支持事务处理。", "不支持事务处理。", '8', Null, '166');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Transaction succesfully broadcast.', 'home.transactionsuccesfullybroadcast.label', '2', '167');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Transaction succesfully broadcast.", "Transaction succesfully broadcast.", '6', Null, '167');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "交易成功播出。", "交易成功播出。", '7', Null, '167');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "交易成功播出。", "交易成功播出。", '8', Null, '167');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'An error occured', 'home.anerroroccured.label', '2', '168');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "An error occured", "An error occured", '6', Null, '168');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "發生錯誤", "發生錯誤", '7', Null, '168');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "发生错误", "发生错误", '8', Null, '168');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Send transaction', 'home.sendtransaction.label', '2', '169');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Send transaction", "Send transaction", '6', Null, '169');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "發送交易", "發送交易", '7', Null, '169');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "发送交易", "发送交易", '8', Null, '169');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Raw transaction data', 'home.rawtransactiondata.label', '2', '170');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Raw transaction data", "Raw transaction data", '6', Null, '170');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "原始的交易數據", "原始的交易數據", '7', Null, '170');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "原始的交易数据", "原始的交易数据", '8', Null, '170');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Broadcast Raw Transaction', 'home.broadcastrawtransaction.label', '2', '171');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Broadcast Raw Transaction", "Broadcast Raw Transaction", '6', Null, '171');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "廣播事務原料", "廣播事務原料", '7', Null, '171');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "广播事务原料", "广播事务原料", '8', Null, '171');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Input unconfirmed', 'home.inputunconfirmed.label', '2', '172');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Input unconfirmed", "Input unconfirmed", '6', Null, '172');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "未經證實的輸入", "未經證實的輸入", '7', Null, '172');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "未经证实的输入", "未经证实的输入", '8', Null, '172');


-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Home value', 'home.value.label', '2', '173');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Value", "Value", '6', Null, '173');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Value", "Value", '7', Null, '173');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "Value", "Value", '8', Null, '173');

-- insert a new key
INSERT INTO `config_translation_key` (`category`, `description`, `key`, `platform_id`, `id`) VALUES ('explorer', 'Home about text', 'home.aboutmc.text', '2', '174');
-- insert default value for en_US
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "<strong>ANX Explorer</strong>   is an open-source asset explorer with complete REST and websocket APIs for your assets.   Visit [Gotham dashboard entry point] to create your own asset and begin your crypto-asset project with ANX.", "<strong>ANX Explorer</strong>   is an open-source asset explorer with complete REST and websocket APIs for your assets.   Visit [Gotham dashboard entry point] to create your own asset and begin your crypto-asset project with ANX.", '6', Null, '174');
-- insert default value for zh_HK
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "<strong>ANX Explorer</strong>   is an open-source asset explorer with complete REST and websocket APIs for your assets.   Visit [Gotham dashboard entry point] to create your own asset and begin your crypto-asset project with ANX.", "<strong>ANX Explorer</strong>   is an open-source asset explorer with complete REST and websocket APIs for your assets.   Visit [Gotham dashboard entry point] to create your own asset and begin your crypto-asset project with ANX.", '7', Null, '174');
-- insert default value for zh_CN
INSERT INTO `config_translation_value` (`environment_id`, `description`, `value`, `language_id`, `site_id`, `key_id`) VALUES (NULL, "<strong>ANX Explorer</strong>   is an open-source asset explorer with complete REST and websocket APIs for your assets.   Visit [Gotham dashboard entry point] to create your own asset and begin your crypto-asset project with ANX.", "<strong>ANX Explorer</strong>   is an open-source asset explorer with complete REST and websocket APIs for your assets.   Visit [Gotham dashboard entry point] to create your own asset and begin your crypto-asset project with ANX.", '8', Null, '174');
