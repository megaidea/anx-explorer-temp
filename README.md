# anx-explorer

Anx-explorer is an insight-based explorer that supports multi-curency and multi coloured coin protocols.

# Requirements

1. NodeJS v4.2.6
2. ZeroMQ (sudo apt-get install libzmq3-dev)
3. pm2
4. GIT
5. bower (installed globally)
6. grunt (installed globally)
7. bitcored-node
8. bitcore

# Multichain Explorer

1. Install multichain instance

```
cd /tmp
wget http://www.multichain.com/download/multichain-1.0-alpha-21.tar.gz
tar -xvzf multichain-1.0-alpha-21.tar.gz
cd multichain-1.0-alpha-21
sudo mv multichaind multichain-cli multichain-util /usr/local/bin
vi ~/.bash_profile
	alias mcu=/usr/local/bin/multichain-util
	alias mcc=/usr/local/bin/multichain-cli
source ~/.bash_profile
```

2. Start up multichain instance
3. In project root, run `sh create-multichain.sh`
4. In your new multichain explorer directory (using anx-explorer as example), edit the bitcore-node.js

```
{
  "multichain_chain": "anx",
  "multichain_native_currency": "ANX",
  "multichain_port": 7206,
  "multichain_pass": "7JR21LJKvzBysryy8ocMM2gSS9Yw3hANYDDS5pqjLx3Z",
  "multichain_params": {"address_pubkeyhash_version" : "3886b9d6",
                        "address_scripthash_version" : "105f50ee",
                        "private_key_version" : "8081f9c1",
                        "address_checksum_value" : "e68f95bb"},

  "datadir": "/data/multichain/anx/data",
}
``` 
5. run `sh project.sh`

# Openassets Explorer

1. run `bitcore-node create oa-node` in Project root
2. cd oa-node
3. run `bitcore-node install insight-api`
4. run the following commands
	a. `cp ../openassets/bitcored ./` 
	b. `cp ../openassets/bitcore-node.js ./`
	c. `cp ../openassets/project.sh ./`
	d. `mkdir data`
5. Edit port in bitcore-node.js
6. run `cd node_modues`
7. run the following commands
	a. ln -s ../../openassets/oa-core/
	b. ln -s ../../openassets/wec-api/
	c. ln -s ../../openassets/custom-api/
	d. ln -s ../../multichain-explorer/node_modules/insight-ui/
8. run `sh project.sh` in oa-node directory

