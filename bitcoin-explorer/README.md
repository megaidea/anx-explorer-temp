# Bitcoin-explorer

Bitcoin-explorer using ANX UI

We use nodeJS v5.11.1

Installation:

  - install bitcore-node by running 'npm install -g bitcore-node'
  - Edit bitcore-node.json, use either 'testnet' or 'mainnet'
  - run npm install
  - at node_modules, symbolic link 'insight-ui -> ../../multichain-explorer/node_modules/insight-ui'
  - Edit appropriate nginx settings

Starting

  - run 'bitcore-node start' OR
  - 'pm2 start pm2.run.json' (Remember to change the bitcore-node location of pm2.run.json, you can find it by running $ which bitcore-node)

