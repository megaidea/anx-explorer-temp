/**************************************& 
 * Copyright 2016 ANX International    *
 * Author: Tron Black  & Franco Ng     *
 * Purpose: Add Multichain to bitcore  *
 ***************************************/ 
//TODO: rename the callback() and next()

"use strict";

var util = require("util");
var EventEmitter = require("events").EventEmitter;
var fs = require("fs");
var assert = require("assert");
var async = require("async");
var sync = require("synchronize");
var levelup = require("levelup");
var leveldown = require("leveldown");
var mkdirp = require("mkdirp");
delete global._bitcore;
var bitcore = require("bitcore-lib");
var BufferUtil = bitcore.util.buffer;
var Networks = bitcore.Networks;
var Block = bitcore.Block;
var $ = bitcore.util.preconditions;
var request = require('request');
var config = require(global.insight_config.mccore_config_path);

// TODO: Place all key here
var ALL_LATEST_TX_KEY = 'ALL_LATEST_TX';
var ALL_LATEST_VALUE_TX_KEY = 'ALL_LATEST_VALUE_TX';
var NATIVE_CURRENCY = '_NATIVE'; //Asset name for native currency
var LATEST_TXS_LIMIT = -25;
var CACHE_LIMIT = 1000;
var KEY_SITE_INFO_PREFIX = "SITE_INFO_";


function isEmptyObject(obj) {
  return !Object.keys(obj).length;
}

function MultichainExplorerService(options) {

  if (!(this instanceof MultichainExplorerService)) {
    return new MultichainExplorerService(options);
  }
  if (!options) {
    options = {};
  }
  this.node = options.node;
  this.network = this.node.network;

  this.log = this.node.log;

  this.routePrefix = 'mc';
  //console.log('options',options);

  EventEmitter.call(this);
  this.setDataPath();

  this.node.services.db.emit('mcCoreReady');

  //this.node.services.bitcoind.on('tip', function(){console.log('haha')});
  //this.node.services.bitcoind.on('tip', this.multichainBlockHandler.bind(this));
  this.node.services.db.on('addblock',this.multichainTipHandler.bind(this));
  //console.log('')

}
MultichainExplorerService.dependencies = ['multichaind','web'];

util.inherits(MultichainExplorerService, EventEmitter);

function enableCors(response) {
  // A convenience function to ensure
  // the response object supports cross-origin requests for APIs
  response.setHeader('Content-Type', 'application/json');
  response.set('Access-Control-Allow-Origin','*');
  response.set('Access-Control-Allow-Methods','POST, GET, OPTIONS, PUT');
  response.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

}

function txFormat(data,txid){
  return {
    'timestamp':data.time,
    'txid':txid
  };
}

function checkAndPushUniqueElementToArray(insertArray,checkElement,insertElement,criteria){
  if (!(checkElement in insertArray)){
    insertArray[checkElement] = []
  }
  if (insertArray[checkElement].findIndex(criteria) == -1){
    insertArray[checkElement].push(insertElement);
  }
  return insertArray;
}

function checkAndAddBalanceToAddressMap(insertMap,address,asset,amount){
  if (!(address in insertMap)){
    insertMap[address] = {}
  }
  if (!(asset in insertMap[address])){
    insertMap[address][asset] = 0.00;
  }
  insertMap[address][asset] += amount;
  return insertMap;
}

MultichainExplorerService.prototype.multichainTipHandler = function(tip,callback){
  this.multichainBlockHandler(tip.__height);
};

var q = async.queue(function(input, callback) {
  input.that.updateTotalSentAndTotalReceived(input.address, input.amount, function(){
    callback();
  });
});

MultichainExplorerService.prototype.updateTotalSentAndTotalReceived = function(address, amount, callback) {
  var self = this;
  var options = {sync: true, keyEncoding: 'binary', valueEncoding : 'utf8'};

  var key_totalSent = address+"_AddrInfo_totalSent";
  var key_totalReceived = address+"_AddrInfo_totalReceived";
  var key_totalBalance = address+"_AddrInfo_totalBalance";
  
  if(amount < 0) {
    self.mc_store.get(key_totalSent,options,function(err,result){
      if(!result){
        result = 0.00;
      }
      var totalSent = (parseFloat(result) - parseFloat(amount)).toFixed(8); //TODO: use BigNumber, dont't use magic number
      //console.log("MultichainExplorerService.prototype.updateTotalSentAndTotalReceived totalSent - address: "+address+" - "+result+" - "+ amount +" -> "+totalSent)
      self.mc_store.put(key_totalSent, totalSent, options, function (err) {
        callback(err);
      });
    });
  } else{
    self.mc_store.get(key_totalReceived,options,function(err,result){
      if(!result){
        result = 0.00;
      }
      var totalReceived = (parseFloat(result) + parseFloat(amount)).toFixed(8);
      //console.log("MultichainExplorerService.prototype.updateTotalSentAndTotalReceived totalReceived - address: "+address+" - "+result+" + "+ amount +" -> "+totalReceived)
      self.mc_store.put(key_totalReceived, totalReceived, options, function (err) {
        callback(err);
      });
    });
  }
}

MultichainExplorerService.prototype.multichainBlockHandler = function(block, callback){
  var self = this;
  var options = {sync: true, keyEncoding: 'binary', valueEncoding : 'utf8'};

  this.node.services.bitcoind.getBlockDetails(block,function(err,data){
    if (err){
      console.log('error',err);
    }

    var txs = data.tx;
    var assetMap = {};
    var txsArray = [];
    var txsAddressMap = {};
    var receivedMap = {};
    var sentMap = {};
    var amountMap = {}; //amountMap[address] = amount
    var txsAmountMap = []; //txsAmountMap = [{txid: , amount: , assetName:}, ...]
    var numOfTxsMap = {};

    var native = global.insight_config.multichain_native_currency;

    async.eachSeries(txs,function(txid,nextTx){
      var txAmountMap = {}; //txAmountMap[assetName][address] = amount
      self.node.services.bitcoind.getTransactionVerbose(txid,true,function(err,data){
        if (err){
          console.log('Error!',err);
        }
        if(typeof data == "undefined"){
          // TODO: add fake tx for block 0
          nextTx();
        } else {
          txsArray.push(txFormat(data,txid));
          self.processVinTransactions(data, amountMap, function(txVinAmountMap) {
            txAmountMap = txVinAmountMap;
            async.eachSeries(data.vout,function(vout,nextVout){
              self.isValueTransaction(data, function(isValueTransaction){
                if (vout.scriptPubKey.addresses){
                  var address = vout.scriptPubKey.addresses[0]
                  var key = address
                  txsAddressMap = checkAndPushUniqueElementToArray(txsAddressMap, key, txid, function(tx){
                    return tx == txid;
                  })
                  if(isValueTransaction){
                    if (typeof numOfTxsMap[address] == 'undefined'){
                      numOfTxsMap[address] = 1;
                    } else {
                      numOfTxsMap[address] += 1;
                    }
                    key = key + '_' + 'value_transaction_only';
                    txsAddressMap = checkAndPushUniqueElementToArray(txsAddressMap, key, txid, function(tx){
                      return tx == txid;
                    });
                  }
                  if (vout.value > 0) {
                    assetMap = checkAndPushUniqueElementToArray(assetMap, native, txFormat(data, txid), function(tx){
                      return tx.txid == txid
                    });
                    var key = address + '_' + native;
                    txsAddressMap = checkAndPushUniqueElementToArray(txsAddressMap, key, txid, function(tx){
                      return tx.txid == txid
                    });

                    if (typeof amountMap[address] == 'undefined') {
                      amountMap[address] = vout.value;
                    } else {
                      amountMap[address] += vout.value;
                    }
                    if (typeof txAmountMap[NATIVE_CURRENCY] == 'undefined') {
                      txAmountMap[NATIVE_CURRENCY] = {};
                    }
                    if(typeof txAmountMap[NATIVE_CURRENCY][address] == 'undefined'){
                      txAmountMap[NATIVE_CURRENCY][address] = vout.value;
                    }else {
                      txAmountMap[NATIVE_CURRENCY][address] += vout.value;
                    }

                    if(isValueTransaction){
                      key = key+'_'+'value_transaction_only';
                      txsAddressMap = checkAndPushUniqueElementToArray(txsAddressMap, key, txid, function(tx){
                        return tx.txid == txid;
                      });
                    }

                    //todo
                    //receivedMap = checkAndAddBalanceToAddressMap(receivedMap,vout.scriptPubKey.addresses[0],native,vout.value);
                  }
                  async.eachSeries(vout.assets, function(asset, nextAsset){
                    var assetName = asset.name;
                    var assetAmount = asset.qty;

                    assetMap = checkAndPushUniqueElementToArray(assetMap, assetName, txFormat(data, txid), function(tx){
                      return tx.txid == txid
                    });
                    var key = address + '_' + assetName;
                    txsAddressMap = checkAndPushUniqueElementToArray(txsAddressMap, key, txid, function(tx){
                      return tx.txid == txid
                    });
                    if(isValueTransaction){
                      key = key + '_' + 'value_transaction_only';
                      if(typeof txAmountMap[assetName] == 'undefined'){
                        txAmountMap[assetName] = {};
                      }
                      if(typeof txAmountMap[assetName][address] == 'undefined'){
                        txAmountMap[assetName][address] = assetAmount;
                      } else {
                        txAmountMap[assetName][address] += assetAmount;
                      }
                      txsAddressMap = checkAndPushUniqueElementToArray(txsAddressMap, key, txid, function(tx){
                        return tx.txid == txid;
                      });
                    }
                    //todo
                    //receivedMap = checkAndAddBalanceToAddressMap(receivedMap,vout.scriptPubKey.addresses[0],assetName,asset.qty);
                    nextAsset();
                  },function done(){
                    nextVout();
                  });
                } else {
                  nextVout();
                }
              });
            },function done(){
              async.forEachOf(txAmountMap, function (amountArray, assetName, nextAsset) {
                var formattedTxAmountMap = {};
                var totalAmount = 0;
                async.forEachOf(amountArray, function (amount, address, nextaddress) {
                  if(amount > 0) {
                    totalAmount += amount;
                  }
                  nextaddress();
                },function done(){
                  if(totalAmount > 0){
                    var formattedTxAmountMap = {txid: txid, amount: totalAmount, assetName: assetName};
                    txsAmountMap = txsAmountMap.concat(formattedTxAmountMap).slice(LATEST_TXS_LIMIT);
                    nextAsset();
                  }else{
                    nextAsset();
                  }

                });
              },function done(){
                nextTx();
              });
            });
          });
        }
      });
    },function done(){
      self.mc_store.get(ALL_LATEST_TX_KEY, options,function(err,result) {
        var txList = [];
        if (err) {
          console.log('tx empty');
        } else {
          txList = JSON.parse(result);
        }
        txList = txList.concat(txsArray).slice(LATEST_TXS_LIMIT);
        self.mc_store.put(ALL_LATEST_TX_KEY, JSON.stringify(txList), options, function (err) {
          //console.log('Updated latestList');
        });
      });

      self.mc_store.get(ALL_LATEST_VALUE_TX_KEY, options,function(err,result) {
        var txList = [];
        if (err) {
          console.log('tx empty');
        } else {
          txList = JSON.parse(result);
        }
        txList = txList.concat(txsAmountMap).slice(LATEST_TXS_LIMIT);
        self.mc_store.put(ALL_LATEST_VALUE_TX_KEY, JSON.stringify(txList), options, function (err) {
        });
      });

      async.forEachOf(txsAddressMap, function (txIdArray, addressAsset, next){
        self.mc_store.get(addressAsset, options, function(err, result) {
          var txList = [];
          if (err){
            console.log('This addressAsset has no record');
          }else {
            txList = JSON.parse(result);
          }
          txList = txList.concat(txIdArray).slice(-CACHE_LIMIT);
          self.mc_store.put(addressAsset, JSON.stringify(txList), options, function (err){
            next();
          })
        });
      });

      async.forEachOf(assetMap, function(assetArray, assetName, next){
        self.mc_store.get(assetName, options, function(err, result){
          var txList = [];
          if (err){
            console.log('This asset has no record');
          }else{
            txList = JSON.parse(result);
          }
          txList = txList.concat(assetArray).slice(-CACHE_LIMIT);
          self.mc_store.put(assetName, JSON.stringify(txList), options, function(err){
            next();
          })
        });
      });
      //self.processBalance(receivedMap,sentMap);

      async.forEachOf(numOfTxsMap, function(numOfTxs, address, next){
        if (numOfTxs !== 0){
          var key_numOfTxs = address + "_AddrInfo_numOfTxs"; // TODO: should not use magic key
          self.mc_store.get(key_numOfTxs, options, function (err, result) {
            if (!result) {
              result = 0;
            }
            numOfTxs = parseInt(result) + parseInt(numOfTxs); // TODO: should not change the numOfTxs
            self.mc_store.put(key_numOfTxs, numOfTxs, options, function (err) {
              if(err){
                console.log(err);
              }
            });
          });
        }
      });
      async.forEachOf(amountMap, function(amount, address, next){
        if(amount!==0){
          q.push({amount: amount, address: address, that: self});
        }
      });

  });

  });
  //callback();
};
MultichainExplorerService.prototype.calculateSent = function(data,callback){
    var sentMap = {};

    async.eachSeries(data.vin,function(vin,next){
      if (vin.txid && vin.vout){
        self.getTxOutput(vin.txid,vin.vout,function(err,vout){
          //console.log('vout',vout);
          if (err){

          }
          if (vout.value >0){
            //console.log('sent asset native amount ' + vout.value + ' to ' + vout.scriptPubKey.addresses[0]);
            sentMap = checkAndAddBalanceToAddressMap(sentMap,vout.scriptPubKey.addresses[0],native,vout.value);
          }
          //console.log('vout', vout);
          //console.log('vout.assets.length' + vout.assets.length);
          async.eachSeries(vout.assets,function(asset,next){
            var assetName = asset.name;
            console.log('sent asset ' + assetName + ' amount ' + asset.qty + ' to ' + vout.scriptPubKey.addresses[0]);
            sentMap = checkAndAddBalanceToAddressMap(sentMap,vout.scriptPubKey.addresses[0],assetName,asset.qty);
            console.log('sentMap',sentMap);
            next();
          },function done(){
            next();
          });
        })
      }else {
        next();
      }
    },function done(){
      callback(null,sentMap);
    });
};

MultichainExplorerService.prototype.processVinTransactions = function(data, amountMap, callback){
  var self = this;
  var txAmountMap = {}; //txAmountMap[ccy] = amount
  
  async.forEach(data.vin,function(vin,nextVin){

    if ((typeof vin.txid !== 'undefined') && (typeof vin.vout !== 'undefined')){
      self.getTxOutput(vin.txid,vin.vout,function(err,result){
        var address = result.scriptPubKey.addresses.toString(); //TODO: valid address, check the .toString() function of addresses
        var value = result.value;
        if(value > 0){
          if (!(NATIVE_CURRENCY in txAmountMap)){
            txAmountMap[NATIVE_CURRENCY] = {}
          }
          if (typeof amountMap[address] == 'undefined'){
            txAmountMap[NATIVE_CURRENCY][address] = -value;
            amountMap[address] = -value;
          } else {
            txAmountMap[NATIVE_CURRENCY][address] -= value;
            amountMap[address] -= value;
          }
        }
        async.eachSeries(result.assets,function(asset,nextAsset){
          var assetName = asset.name;
          var assetAmount = asset.qty;
          if(typeof txAmountMap[assetName] == 'undefined'){
            txAmountMap[assetName] = {};
          }
          if(assetAmount > 0){
            if (typeof txAmountMap[assetName][address] == 'undefined'){
              txAmountMap[assetName][address] = -assetAmount;
            } else {
              txAmountMap[assetName][address] -= assetAmount;
            }
          }
          nextAsset();
        },function done(){
          nextVin();
        });
      });
    }else{
      nextVin();
    }
  },function done(){
    callback(txAmountMap);
  });
};

MultichainExplorerService.prototype.isValueTransaction = function(data,callback) {
  var self = this;
  var grantTransaction = false;

  //check coinbase transaction;
  if(typeof data.vin[0].coinbase !== 'undefined'){
    callback(false);
    return;
  }
  
  //check grant transaction;
  data.vout.forEach(function (vout) {
    if(typeof vout.permissions !== 'undefined' && !isEmptyObject(vout.permissions)) {
      grantTransaction = true;
    }
    
  })
  
  if(grantTransaction){
    callback(false);
    return;
  }else{
    callback(true);
    return;
  }
  
  
};

MultichainExplorerService.prototype.processBalance = function(receivedMap,sentMap){
  var netValueMap = {};
  //console.log(receivedMap['13g8TqcGMsnC7Scehc8yaAAGSagcWNAA9X']);
  async.waterfall([
    function(callback){
      async.forEachOf(receivedMap,function(valueMap,address,next){
        async.forEachOf(valueMap,function(value,asset,next){
          valueMap = checkAndAddBalanceToAddressMap(netValueMap,address,asset,value);
          next();
        });
        next();
      },function done(){
        callback();
      })},
    function(callback){
      async.forEachOf(sentMap,function(valueMap,address,next){
        async.forEachOf(valueMap,function(value,asset,next){
          valueMap = checkAndAddBalanceToAddressMap(netValueMap,address,asset,-value);
          next();
        });
        next();
      },function done(){
        callback();
      })
    }
  ],function(err,res){
    if (err){
      console.log('err', err);
    }
    async.forEachOf(netValueMap,function(valueMap,address,next){
      var balanceList = {};
      self.mc_store.get(address+'_BALANCE',options,function(err,result){
        if (err){
          console.log('This address has no balance record');
        }else {
          balanceList = JSON.parse(result);
        }
        async.forEachOf(valueMap,function(value,asset,next){
          if (!(asset in balanceList)){
            balanceList[asset] = 0.00;
          }
          balanceList[asset]+= value;
          next();
        },function done(){
          self.mc_store.put(address+'_BALANCE',JSON.stringify(balanceList),options,function(err){
            //console.log('balance of ' + address, balanceList);
            //console.log(address,balanceList);
            next();
          })
        })
      });
    });
  });
};

MultichainExplorerService.prototype.getTxOutput = function(txid,index,callback){
  var self = this;
  self.node.services.bitcoind.getTransactionVerbose(txid,true,function(err,data){
    if (err){
      console.log('Error :', err);
    }
    if (index >= data.vout.length){
      console.log('Index length than data.vout');
    }
    callback(null,data.vout[index]);
  });
};

MultichainExplorerService.prototype.setDataPath = function() {
  var chain = global.insight_config.multichain_chain;
  var dbName = 'multichain-'+ chain +'-db';
  $.checkState(this.node.datadir, 'bitcoind is expected to have a "spawn.datadir" property');
  // console.log(this);
  // console.log(this.network);
  // console.log(Networks.livenet);
  var datadir = this.node.datadir;
  if (this.network.name === Networks.livenet.name) {
    this.dataPath = datadir + '/' + dbName;
  } else if (this.network.name === Networks.testnet.name) {
    if (this.network.regtestEnabled) {
      this.dataPath = datadir + '/regtest/' + dbName;
    } else {
      this.dataPath = datadir + '/testnet3/' + dbName;
    }
  } else {
    throw new Error('Unknown network: ' + this.network);
  }
};

MultichainExplorerService.prototype.start = function(callback) {
  var self = this;
  if (!fs.existsSync(this.dataPath)) {
    mkdirp.sync(this.dataPath);
  }
  var options = {sync: false, keyEncoding: 'binary', valueEncoding : 'binary' };

  self.mc_store = levelup(this.dataPath);

  //self.showIndexes();

  //console.log('After showIndexes');

  // put siteInfo into leveldb, each site have a key, e.g. SITE_INFO_ATM
  var configSiteInfo =  config.siteInfo
  Object.keys(configSiteInfo).forEach(function(site) {
    var siteInfo = configSiteInfo[site];
    self.log.info('mc-core start - Saving config to level db - site: ',site,', siteInfo: ', siteInfo);
    self.putConfigToDB(site, siteInfo);
  });
  
  self.once('ready', function() {
    self.log.info('mc-core ready!');

    self.node.services.bitcoind.on('tip', function() {
      console.log('tip! index.js:108');
      //if(!self.node.stopping) {
      //  self.sync();
      //}
    });
    
  });

  //self.loadTip(function(err) {
  //  if (err) {
  //    return callback(err);
  //  }
  //
  //  self.sync();
  //  self.emit('ready');
  //  callback();
  //});

  callback();
};

MultichainExplorerService.prototype.getAPIMethods = function() {
  //console.log('getAPIMethod');
  return [
  ];
};

MultichainExplorerService.prototype.getPublishEvents = function() {
  //console.log('getPublishEventes');
  return [
  ];
};

MultichainExplorerService.prototype.setupRoutes = function(app) {

  //Enable CORS
  app.use(function(req, res, next) {
    res.header('Accept', 'application/json');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
  });

  app.get('/latestTx', this.getLatestTx.bind(this));
  app.get('/latestValueTx', this.getLatestValueTx.bind(this));
  app.get('/latestTx/:asset',this.getLatestTxByAssetName.bind(this) );

  app.get('/txs',this.getTxsByAddress.bind(this));
  //app.get('/txs/:address/:asset',this.getTxsByAddressAndAsset.bind(this));

  app.get('/addrInfo/:address',this.getAddressInfo.bind(this));

  app.get('/siteInfo/:site', this.getSiteInfo.bind(this));
  
  app.get('/', function(req,res,next){
    res.status(200).send(JSON.stringify('{status: "okay"}'));
  });
};

MultichainExplorerService.prototype.getRoutePrefix = function() {
  return this.routePrefix;
};

MultichainExplorerService.prototype.getAddressInfo = function(req,res,next){
  enableCors(res);
  var self = this;
  var address = req.params.address;
  this.getAddressInfoHandler(address,function(err,result){

    if (err){
      res.jsonp({
        error: 'error.no.adddress.info' //change me add error i18n??
      });
    }else {
      res.jsonp(result);
    }
  });

};

MultichainExplorerService.prototype.getAddressInfoHandler = function(address,callback){
  var self = this;
  var options = {sync: true, keyEncoding: 'binary', valueEncoding : 'utf8'};
  
  var addressInfo = {
    addrStr : address,
    balance : 0,
    totalReceived : 0,
    totalSent : 0,
    numOfTxs: 0
  };
  
  //TODO: don't use magic key 
  var key_totalSent = address+"_AddrInfo_totalSent";
  var key_totalReceived = address+"_AddrInfo_totalReceived";
  var key_totalBalance = address+"_AddrInfo_totalBalance";
  var key_numOfTxs = address+"_AddrInfo_numOfTxs";
  async.series([
    function(callback2) {
      self.mc_store.get(key_totalSent,options,function(err,result){
        if(result){
          addressInfo.totalSent = result;
        }
        callback2();
      });
    },
    function(callback3) {
      self.mc_store.get(key_totalReceived,options,function(err,result){
        if(result) {
          addressInfo.totalReceived = result;
          addressInfo.balance = result - addressInfo.totalSent;
        }
        callback3();
      });
    },
    function(callback5) {
      self.mc_store.get(key_numOfTxs,options,function(err,result){
        if(result) {
          addressInfo.numOfTxs = result;
        }
        callback5();
      });
    }
  ],
  function() {
    callback(null, addressInfo)
  });

};

MultichainExplorerService.prototype.getTxsByAddress = function(req,res,next){
  enableCors(res);

  var self = this;
  var address = req.query.address;
  var page = parseInt(req.query.pageNum) || 0;
  var asset = req.query.asset;
  var valueTransactionOnly = false;
  if(req.query.valueTransactionOnly==="true"){
    valueTransactionOnly = true;
  }

  this.getTxsByAddressHandler(address,page,asset,valueTransactionOnly,function(err,result){

    if (err){
      res.jsonp({
        error: 'error.no.transaction'
      })
    }else {
      res.jsonp(result);
    }
  });

};

MultichainExplorerService.prototype.getTxsByAddressHandler = function(address,page,asset,valueTransactionOnly,callback){
  var self = this;
  var options = {sync: true, keyEncoding: 'binary', valueEncoding : 'utf8'};
  var pageLength = 10;
  var pageTotal = 1;
  var key;

  if (address && asset){
    key = address + '_' + asset;
  }else{
    key = address;
  }

  if(valueTransactionOnly){
    key = address+'_'+'value_transaction_only';
  }
  self.mc_store.get(key,options,function(err,result){
    if (err){
      console.log('error', err);
      callback(err);
    }else {
      result = JSON.parse(result);
      pageTotal = Math.ceil(result.length / pageLength);
      var pageOffset = pageLength * (page+1);
      if (pageOffset > result.length) pageLength -= (pageOffset- result.length);
      result = result.splice(-pageOffset,pageLength).reverse();
      async.mapSeries(result,function(tx,callback){+
        self.node.services.bitcoind.getTransactionVerbose(tx,true,function(err,res){
          async.forEach(res.vin,function(vin,next){
            var index = res.vin.indexOf(vin);
            if ((typeof vin.txid !== 'undefined') && (typeof vin.vout !== 'undefined')){
              self.getTxOutput(vin.txid,vin.vout,function(err,result){
                res.vin[index].addr = result.scriptPubKey.addresses.toString();
                res.vin[index].value = result.value;
                if (result.assets){
                  res.vin[index].assets = result.assets
                }
                next();
              })
            }else{
              next();
            }
          },function done(){
            callback(null,res);
          });
        })
      },function(err,result){
        //console.log(result);
        callback(null,{
          pageTotal: pageTotal,
          txs: result
        });
      });
    }

  });
};

MultichainExplorerService.prototype.getLatestValueTx = function(req,res,next){
  enableCors(res);

  this.getLatestValueTxHandler(function(err,result){
    if (err){
      res.jsonp({
        error: 'error.no.transaction'
      })
    }else {
      res.jsonp({
        data: result
      });
    }
  });
};

MultichainExplorerService.prototype.getLatestValueTxHandler = function(callback){
  var self = this;
  var options = {sync: true, keyEncoding: 'binary', valueEncoding : 'utf8'};

  self.mc_store.get(ALL_LATEST_VALUE_TX_KEY,options,function(err,result){

    if (err){
      console.log('error', err);
      callback(err);
    }else {
      result = JSON.parse(result).reverse();
      callback(null,result);
    }

  });
};

MultichainExplorerService.prototype.getLatestTx = function(req,res,next){
  enableCors(res);

  this.getLatestTxHandler(function(err,result){
    if (err){
      res.jsonp({
        error: 'error.no.transaction'
      })
    }else {
      res.jsonp({
        data: result
      });
    }
  });
};

MultichainExplorerService.prototype.getLatestTxHandler = function(callback){
  var self = this;
  var options = {sync: true, keyEncoding: 'binary', valueEncoding : 'utf8'};

  self.mc_store.get(ALL_LATEST_TX_KEY,options,function(err,result){

    if (err){
      console.log('error', err);
      callback(err);
    }else {
      result = JSON.parse(result).reverse();
      callback(null,result);
    }

  });
};

MultichainExplorerService.prototype.getLatestTxByAssetName = function(req,res,next){
  enableCors(res);

  var self = this;
  var asset = req.params.asset;

  this.getLatestTxByAssetNameHandler(asset,function(err,result){

    if (err){
      res.jsonp({
        error: 'error.no.transaction'
      })
    }else {
      res.jsonp({
        data: result
      });
    }
  });

};

MultichainExplorerService.prototype.getLatestTxByAssetNameHandler = function(assetName,callback){
  var self = this;
  var options = {sync: true, keyEncoding: 'binary', valueEncoding : 'utf8'};

  self.mc_store.get(assetName,options,function(err,result){
    if (err){
      console.log('error', err);
      callback(err);
    }else {
      result = JSON.parse(result).splice(LATEST_TXS_LIMIT).reverse();
      callback(null,result);
    }

  });
};

MultichainExplorerService.prototype.stop = function(callback) {
  var self = this;

  // Wait until syncing stops and all db operations are completed before closing leveldb
  async.whilst(function() {
    return self.bitcoindSyncing;
  }, function(next) {
    setTimeout(next, 10);
  }, function() {
    self.mc_store.close();
  });
  callback();
};

MultichainExplorerService.prototype.getSiteInfo = function (req, res, next) {
  enableCors(res);
  var self = this;
  var site = req.params.site;
  site = site.toUpperCase();

  this.getSiteInfoHandler(site, function (err, result) {
    if (err) {
      self.log.info("getSiteInfo err - site:"+site)
      res.jsonp({
        error: 'error.no.site.info' //change me add error i18n??
      });
    } else {
      res.jsonp(result);
    }
  });
};

MultichainExplorerService.prototype.getSiteInfoHandler = function (site, callback) {
  if(typeof site == 'undefined'){
    callback("Invalid request");
  }
  var self = this;
  var options = { sync: true, keyEncoding: 'binary', valueEncoding: 'utf8' };

  self.mc_store.get(KEY_SITE_INFO_PREFIX + site, options, function (err, result) {
    if (err) {
      console.error('mc-core getSiteInfoHandler - Fail to get config from config');
      callback(err);
    } else if (result) {
      callback(null, JSON.parse(result));
    } else{
      callback("mc-core getSiteInfoHandler - Fail to get config from config, err and result are null");
    }
  });
};

MultichainExplorerService.prototype.putConfigToDB = function (site, siteInfo, callback) {
  var self = this;
  var options = { sync: true, keyEncoding: 'binary', valueEncoding: 'utf8' };

  self.mc_store.put(KEY_SITE_INFO_PREFIX + site, JSON.stringify(siteInfo), options, function (err) {
    if (err) {
      console.error("mc-core putConfigToDB - err: ",err);
      callback(err);
    }
  });
}

module.exports = MultichainExplorerService;