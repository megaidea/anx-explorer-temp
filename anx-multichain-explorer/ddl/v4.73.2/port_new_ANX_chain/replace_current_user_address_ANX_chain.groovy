import altcoinx.*
import com.anxintl.siteadmin.*
import com.anxintl.siteconfig.core.*
import org.joda.time.DateTime

//Change it to true to confirm the replacement process
boolean startReplace = false
int replaceNumberOfAddress = 100

List<String> invalidCharList = ["1"]

List<DigitalCcySettings> digitalCcySettingsList = DigitalCcySettings.findAllByBlockchainInstance("ANX")
List<CcyEnum> ccyEnumList = digitalCcySettingsList.collect{new CcyEnum(it.ccy.toString())}

List<Account> accountList = Account.findAllByCcyInList(ccyEnumList)

List<SubAccount> subAccountList = SubAccount.findAllByAccountInList(accountList)

List<CoinAddressAllocation> coinAddressAllocationList = CoinAddressAllocation.findAllBySubAccountInList(subAccountList)

Map<SubAccount, CoinAddressAllocation> latestCoinAddressAllocationList = [:]

coinAddressAllocationList.each{ coinAddressAllocation ->
	if (!latestCoinAddressAllocationList[coinAddressAllocation.subAccount])
		latestCoinAddressAllocationList[coinAddressAllocation.subAccount] = coinAddressAllocation
	else if (coinAddressAllocation.dateCreated > latestCoinAddressAllocationList[coinAddressAllocation.subAccount].dateCreated)
		latestCoinAddressAllocationList[coinAddressAllocation.subAccount] = coinAddressAllocation
}

Map<SubAccount, CoinAddressAllocation> invalidCoinAddressAllocationList = [:]

latestCoinAddressAllocationList.each{ subAccount, coinAddressAllocation ->
	if (coinAddressAllocation.coinAddress[0] in invalidCharList)
  		invalidCoinAddressAllocationList[subAccount] = coinAddressAllocation
}

invalidCoinAddressAllocationList = invalidCoinAddressAllocationList.take(replaceNumberOfAddress)

invalidCoinAddressAllocationList.each{ subAccount, coinAddressAllocation ->
	println "[${coinAddressAllocation.subAccount.account.ccy}]${coinAddressAllocation.subAccount.account.owner.username} : ${coinAddressAllocation.coinAddress}"
}

if (startReplace){
  println "Replacing new addresses"
  invalidCoinAddressAllocationList.each{ subAccount, coinAddressAllocation ->
  	String legacyAddress = coinAddressAllocation.coinAddress
    CcyEnum ccy = subAccount.account.ccy
    CoinAddress coinAddress = CoinAddress.findByCcyAndAssigned(ccy,false)
	if (coinAddress){
        CoinAddressAllocation newCoinAddressAllocation = new CoinAddressAllocation()
        newCoinAddressAllocation.coinAddress = coinAddress.coinAddress
      	newCoinAddressAllocation.subAccount = coinAddressAllocation.subAccount

		coinAddress.assigned =true
		coinAddress.dateTimeAssigned = DateTime.now()

		newCoinAddressAllocation.save(flush:true)
		coinAddress.save(flush:true)
		println legacyAddress + " -> " + coinAddress.coinAddress
	}else {
		println legacyAddress + " : OUT OF NEW ADDRESS"
	}

  }
}
false
