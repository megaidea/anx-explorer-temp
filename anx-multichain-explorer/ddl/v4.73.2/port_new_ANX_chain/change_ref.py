from subprocess import check_output
import json

chain_name = "anx_zero"
multichain_table = "`multichain_table`"
asset_ref = "asset_ref"
ccy = "ccy"
file_destination = "change_ref.sql"

file_reissue = open(file_destination,"wb")

out = check_output(["multichain-cli", chain_name,"listassets"])
out = out.split("\n",2)[2]; #Remove the input entry, making the output a whole json format

asset_list = json.loads(out)
for asset in asset_list:
    change_line = "update %s set %s = '%s' where %s = '%s';" % (multichain_table, asset_ref, asset['assetref'], ccy, asset['name'])
    #issue_line = "multichain-cli %s issue %s %s %s %s" % (new_chain, new_chain_address, asset['name'] ,asset['issueqty'] ,asset['units'])
    print change_line
    file_reissue.write(change_line+"\n")


file_reissue.close()
