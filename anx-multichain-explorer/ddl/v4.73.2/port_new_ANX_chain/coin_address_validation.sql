-- COIN ADDRESS validation QUERY
SELECT * FROM coin_address_validation
where digital_ccy_type = "MULTICHAIN"
and ccy is null;

-- COIN ADDRESS validation UPDATE
UPDATE coin_address_validation
set pattern = "^[abAB][a-km-zA-HJ-NP-Z1-9]{25,40}$"
where digital_ccy_type = "MULTICHAIN"
and ccy is null;
