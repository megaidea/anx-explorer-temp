from subprocess import check_output
import json

old_chain = "anx_zero"
new_chain = "anx_new1"
new_chain_address = "1HduPV8nsRVzsbiUTQgDhBXeoQZRVPjq5gHN7"
bash_shebang = "#!/bin/sh"
file_destination = "reissue.sh"

file_reissue = open(file_destination,"wb")
file_reissue.write(bash_shebang + "\n")

out = check_output(["multichain-cli", old_chain,"listassets"])
out = out.split("\n",2)[2]; #Remove the input entry, making the output a whole json format

asset_list = json.loads(out)
for asset in asset_list:
    issue_line = "multichain-cli %s issue %s %s %s %s" % (new_chain, new_chain_address, asset['name'] ,asset['issueqty'] ,asset['units'])
    print issue_line
    file_reissue.write(issue_line+"\n")


file_reissue.close()
