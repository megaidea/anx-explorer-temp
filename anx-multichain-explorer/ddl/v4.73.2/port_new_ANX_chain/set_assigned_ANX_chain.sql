-- QUERY
SELECT *
FROM coin_address
where
ccy in (SELECT ccy from digital_ccy_settings where blockchain_instance = "ANX")
and assigned = 0
and left(coin_address,1) = "1"
OR ccy = "ANX";

-- UPDATE
UPDATE coin_address 
SET assigned = 1
where
ccy in (SELECT ccy from digital_ccy_settings where blockchain_instance = "ANX")
and assigned = 0
and left(coin_address,1) = "1"
OR ccy = "ANX";
